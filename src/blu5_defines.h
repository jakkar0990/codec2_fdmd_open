
#ifndef BLU5_DEFINES

#define BLU5_DEFINES
/*--------------------------------------------------------------------

					        Jak Defines Header

 --------------------------------------------------------------------*/

#define BLU5_DATA_MODE   // This is the General Switch for adding the data funct.
                         // code.

#define JAK_FUNCTIONS		 // This enables the comm function definitions in
                         // freedv_api.c
												 
#define JAK_MODS         // This just alters the the original codec2 code
                         // for sections that we wanted to detract or not use.
						/*  Sections not in use:
							- switch_back section - for the preferences section of the code */

#define REPEAT_TRANSMIT // Repeat transmit activated

#define __CRC_16__      // Activates code for use of CRC16 error detection

//#define ARQ_MODE

//----------------------------------------------------------------------
// CODE ENABLE ?
//----------------------------------------------------------------------
#define PREFS 1							// This is used as a decision to allow 
                                        // enabling and disabling of the Prefs section
                                        // of the sm1000 src code

#define CODEC2_ALT						// This defn was used for a problematic section
                                        // of src code in c2enc.c 
                                        // We commented it out so as to prevent issues.

#define ARQ_ENABLED						// Controls whether ARQ code is compiled

#define UART_TX_CONTROL                 // Removes Original SM1000 code to allow UART PTT control

// #define KEIL_TOOLCHAIN               // To make source code work for Keil Programming

#define I2C_SPL							// Enable code that transfers using SPL code

//#define SPI_ENABLE

//#define I2C_REGISTER          // Enable i2c code to send via register transfer value

#ifndef CORTEX_M4
//======================
// FREEDV IMP
//======================

#define DEBUG_DEFS_FREEDV     // Extra definitions in fdmdv2.cpp of data functions

#else
//======================
// SM1000 IMP
//======================

//#define EMBEDDED					// Controls whether code for the embedded project // is compiled or not						
//#define SM1000						// Enables code specifically for SM1000

//======================
// BlU5 HF IMP
//======================

#define EMBEDDED				// Controls whether code for the embedded project // is compiled or not						
#define BLU5_BOARD						// Enables code specifically for Blu5 Board


#endif



/*------------------------------------------------------------------

					OP_MODE OPTIONS

--------------------------------------------------------------------*/


//--------------------------------------------------------------------
// Definitions We Included Here so definitions visible in FreeDV_API
//--------------------------------------------------------------------


#ifdef BLU5_DATA_MODE

#define STATE_RX        0x00    /*!< Receive state: normal operation */
#define STATE_TX        0x10    /*!< Transmit state: normal operation */
#define STATE_RX_TOT    0x01    /*!< Receive state: after time-out */
#define STATE_MENU      0x20    /*!< Menu state: normal operation */

#endif


#ifdef BLU5_DATA_MODE
								//-------------------------------------------
#define MAX_MODES  3			// THESE ARE THE MODES THAT CAN BE CYCLED THROUGH
#define ANALOG     0			// These are designed to be PTT operated
#define DV         1			//------------------------------------------
#define TONE       2

// Special Case Modes			// These are modes are more modem oriented
#define DATA_UART  4			// DATA_UART - Tx Sends Data in Phases from UART
#define DV_TEST    5			// DV_TEST   - Continuous Voice mode controlled via Key Press from UART
#define DATA_TEST  6			// DATA_TEST - Continuous Data mode controlled via Key Press from UART
#define DATA_ARQ   7			// DATA_ARQ - To send error-free transmissions via ARQ protocol

#endif

/*--------------------------------------------------------------------
                        GENERAL NUMERIC DEFS
--------------------------------------------------------------------*/

#define MAX_UART_BUFF_SIZE 700
#define ARQ_MAX_FRAMES 130
#define UART_BYTES_PER_FRAME 6
#define ARQ_BYTES_PER_FRAME 5
#define DEBUG_NUM  70

#define ACK_MATRIX_SIZE 3


												 
/*====================================================================
                         STATE DEFINITIONS
======================================================================*/


/*--------------------------------------------------------------------
					Data_Tx/Rx Stages Definitions
 --------------------------------------------------------------------*/							 

// DATA_UART STAGES ------------------

#define NO_TXING 0
#define SYNCING 2
#define TRANSMIT_DATA 3
#define END_OF_TXING 4

// SPECIAL ARQ TRANSMIT STATES
#define TRANSMIT_ACK 5
#define TX_ERROR     6


/*--------------------------------------------------------------------
                             ARQ STATES
--------------------------------------------------------------------*/

// RX STATES ----------------------
#define ARQ_RX_DATA				 0
#define ARQ_RX_ACK		 		 1

// TX STATES ----------------------
#define ARQ_TX_DATA		 		 10
#define ARQ_TX_ACK				 11

// IDLE STATES --------------------
#define ARQ_WAIT				 20

// TX/RX COMPLETE STATE -----------
#define ARQ_TX_RX_COMPLETE		 30 // Output data to User screen
#define ARQ_ERROR				 31


/*====================================================================
                         TIME DEFINITIONS
======================================================================*/


/*--------------------------------------------------------------------
                         Sync/EOT Time Duration
 --------------------------------------------------------------------*/	

#define SYNC_TIME_ARQ  5000
#define SYNC_TIME_UART 5000

#define EOT_TIME_UART  2000
#define EOT_TIME_ARQ   2000

//---------------------------------
// ARQ TIME LIMITS
//---------------------------------

#define ARQ_RX_DATA_TIMEOUT_PERIOD 20000   // Time needed to enter error state after no signal rxed
#define ARQ_RX_ACK_TIMEOUT_PERIOD  10000    // Time to wait whilst receiving ack frames

#define ARQ_WAIT_PERIOD		       2000    // Time to wait after receiving data

#define ARQ_TX_ACK_PERIOD          5000

/*--------------------------------------------------------------------

						End of Jak Defines Header

 --------------------------------------------------------------------*/
 
 
 
 
#endif



#include "blu5_defines.h"
#include <stdint.h>

#ifndef EMBEDDED

#include <sys/time.h>

#endif //EMBEDDED

//=======================
// To use Timer 
//=======================

#ifndef _BLU5_STRUCT_DEFINES

#define _BLU5_STRUCT_DEFINES


#ifdef __cplusplus
  extern "C" {
#endif

/**********************************************************


				DATA BUFF STRUCT


**********************************************************/
		
struct data_struct {
// Buffers are intended to store six bytes for any frame

unsigned char data_text_sync[10];  // Last character is Null Chracter. Make it easier to fill in the Frame index byte
unsigned char data_text_eot[10];   // Same as above
unsigned char data_text_ack[10];   // Ack Frame
unsigned char data_tx_rx_frame[10]; // Tx/Rx Frame

//---------------------------------------------
// TEST DATA SNIPPET - USED FOR DATA_TEST MODE
//---------------------------------------------
 
unsigned char data_text_0[100];     // Test Data Snippet

//---------------------------------------------
// UART DATA BUFFER - 
//---------------------------------------------

volatile unsigned char data_from_uart[MAX_UART_BUFF_SIZE] ;   // Data From UART to Transmit over HF
volatile unsigned char data_to_uart[MAX_UART_BUFF_SIZE] ;     // Data from HF to UART

//---------------------------------------------
// DATA CONTROL BUFFERS - 
//---------------------------------------------

unsigned char  data_text_rx[ARQ_MAX_FRAMES][ARQ_BYTES_PER_FRAME + 1];		// Data Snippets received via HF (error-free)

///---------------------------------------------
// Debug Buffers
//---------------------------------------------

unsigned char debug[DEBUG_NUM][ARQ_BYTES_PER_FRAME + 1];

unsigned short ack_received[ACK_MATRIX_SIZE][2];

} ;

/**********************************************************


		         	 TIMER STRUCT


**********************************************************/

/**********************************************************
 N.B.:

 The RX_DATA_TIMEOUT timers are reset whenever a sync signal
 is detected.

**********************************************************/

struct timers {

	//-----------------------------------
	// DATA TIMER START/STOP
	//-----------------------------------

	uint8_t start_sync;
	uint8_t start_eot;

	//-----------------------------------
	// ARQ TIMER START/STOP
	//-----------------------------------

	uint8_t start_tx_ack;
	uint8_t start_tx_err;

	uint8_t start_wait;

	uint8_t start_rx_ack;
	uint8_t start_rx_data;

	#ifdef EMBEDDED

	//------------------------------------
	// DATA TIMERS
	//------------------------------------

	uint16_t sync_time_elapsed ; 		// in milliseconds
	uint16_t end_of_tx_time_elapsed ;	// in milliseconds

	uint16_t sync_time;
	uint16_t eot_time;
	//------------------------------------
	// ARQ TIMERS
	//------------------------------------

	// Rx Timers
	uint16_t rx_data_time_elapsed;	    // ...
	uint16_t rx_ack_time_elapsed;	    // ...

	// Tx Timer
	uint16_t tx_ack_time_elapsed;	    // ...

	// Wait Timer
	uint16_t wait_time_elapsed;         // ...

	#else

	//------------------------------------
	// DATA TIMERS
	//------------------------------------

	double sync_time_elapsed; 		   // in milliseconds
	double end_of_tx_time_elapsed;	   // in milliseconds

	double sync_time;
	double eot_time;

	//------------------------------------
	// ARQ TIMERS
	//------------------------------------

	// Rx Timers
	double rx_data_time_elapsed;	   // ...
	double rx_ack_time_elapsed;	       // ...

	// Tx Timer
	double tx_ack_time_elapsed;		   // ...

	// Wait Timer
	double wait_time_elapsed;          // ...

	double now_ms ;
	double then_ms;
	double diff_ms;
	
	struct timeval now ;
	struct timeval then;

	#endif //EMBEDDED

};




/**********************************************************


					DATA TX/RX STRUCT


**********************************************************/


typedef unsigned short  crc;

struct data_tx_rx {

	//================================================
	// A. GENERAL - SM1000 OR FREEDV
	//================================================

	//===================================
	// A.1 DATA
	//===================================

	//------------------------------
	// MODEM STATES
	//------------------------------

	uint8_t * op_mode;
	uint8_t data_tx_stage;  // NO_TXING

	//------------------------------
	// PTT SPECIFIC 
	//------------------------------
	uint8_t * core_state;
	uint8_t not_rxing_txing;

	//------------------------------
	//  MODULATION FLAGS / VARs
	//------------------------------
	uint8_t rx_demod_sync;				  // Flag exported from FDMDV modem to indicate presence of Sync Signal
	unsigned short bit_err_det;           // Variable indicates bit errors detected from frame

	long no_bits_detected;

	//------------------------------
	//  Checksum
	//------------------------------
	crc  crcTable[256];
	unsigned short crc_code;

	unsigned short crc_var_ext;
	unsigned short crc_var_calc;

	//===================================
	// A.2 TX SETTINGS
	//===================================
	
	uint8_t data_transmission;
	uint8_t begin_tx;

	uint8_t is_data_test_mode_flag;

	uint8_t menu_flag;

	uint16_t frames_sent;

	unsigned char * uchar_ptr;
	unsigned char * modulator_input;

	short data_offset;
	short init_offset;

	uint8_t init_rem;
	unsigned short char_num;

	//===================================
	// A.3 RX SETTINGS
	//===================================
	uint8_t rx_data_accept;   // RX FLAG
	// Final Data tx/rx complete function
	int(*output_data_f_ptr)(struct data_struct * data_buffers, struct arq_machine * arq_mod);
	// The debug output function
	void(*debug_data_f_ptr)(struct data_struct * data_buffers, struct arq_machine * arq_mod);

	uint8_t output_data;
	int data_outputted;

	uint8_t ack_incr_occured;


	//================================================
	// B. EITHER SM1000 OR FREEDV
	//================================================
	
	//===================================
	// B.1 FREEDV
	//===================================
	
	int * g_tx;

	//===================================
	// B.2 SM1000
	//===================================

	//============================
	// UART VARIABLES
	//============================

	//--------------------------
	// UART COUNTERS
	//--------------------------

	uint16_t tx_uart_data_index;
	uint16_t rx_uart_data_index;

	uint8_t	usart_perr_counter;
	uint8_t	usart_err_counter;

	//--------------------------
	// UART FLAGS
	//--------------------------
	volatile uint8_t is_idle;

	volatile uint8_t uart_start_stop;
	uint8_t uart_buffer_sent;

	//========================================================

};


/**********************************************************


				ARQ STATE MACHINE STRUCT


**********************************************************/



struct arq_machine {

	//-----------------------------------
	// ARQ STATE	
	//-----------------------------------
	unsigned char arq_state ;   // Current State
	unsigned char prv_state ;   // Previous State
	//-----------------------------------


	//-----------------------------------
	// ARQ COUNTERS	
	//-----------------------------------
	short transmissions;

	//------------------------------------
	// INDEXERS
	//------------------------------------
	short arq_total_session_frames ;
	short arq_tx_frame_indexer;
	short arq_rx_frame_indexer;
	short arq_ack_frame_indexer;

	unsigned short crc_code_ext_calc[130][2];

	//-------------------------------------
	// 
	//-------------------------------------

	short rxed_frames_counter;

	//------------------------------------
	// Ratio_Complete
	//------------------------------------
	float ratio_complete;
	float prv_ratio_complete;

	//---------------------------------------
	// Debug Value
	//---------------------------------------
	short prv_arq_ack_frame_indexer;

	short debug_counter;
	long debug_counter_1;
	long debug_counter_2;

	//---------------------------------------
	// ERROR VARs
	//---------------------------------------
	uint8_t arq_error_code;

	unsigned char test_rx_frame[10]; // Tx/Rx Frame

	//------------------------------------------


	/*====================================================================

								FLAGS

	====================================================================*/


	//---------------------------------------
	// ARQ FLAGS
	//---------------------------------------
	uint8_t data_tx_completed ;
	uint8_t data_session ;
	uint8_t data_transfer;

	uint8_t tx_err_sent ;
	uint8_t tx_error;
	uint8_t sent_first_data_packet;

	uint8_t rx_ack_received;

	uint8_t error_occurred ;
	uint8_t start_frame_received ;  // Indicates if the start frame was received

	struct data_tx_rx * data_modem;

	struct data_struct * data_buffers;
	
};


/**********************************************************************
 *
 * Filename:    crc.h
 *
 * Description: A header file describing the various CRC standards.
 *
 * Notes:
 *
 *
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 **********************************************************************/

 /*
  * Select the CRC standard from the list that follows.
  */

//#define JAK_CRC_DEFS

#define JAK_CRC_CCITT

typedef unsigned short  crc;

#if defined(JAK_CRC_CCITT)

#define CRC_NAME			"CRC-CCITT"
#define POLYNOMIAL			0x1021
#define INITIAL_REMAINDER	0xFFFF
#define FINAL_XOR_VALUE		0x0000
#define REFLECT_DATA		0x0000
#define REFLECT_REMAINDER	0x0000
#define CHECK_VALUE			0x29B1

#elif defined(JAK_CRC16)

#define CRC_NAME			"CRC-16"
#define POLYNOMIAL			0x8005
#define INITIAL_REMAINDER	0x0000
#define FINAL_XOR_VALUE		0x0000
#define REFLECT_DATA		1
#define REFLECT_REMAINDER	1
#define CHECK_VALUE			0xBB3D

#elif defined(JAK_CRC32)

typedef unsigned long  crc;

#define CRC_NAME			"CRC-32"
#define POLYNOMIAL			0x04C11DB7
#define INITIAL_REMAINDER	0xFFFFFFFF
#define FINAL_XOR_VALUE		0xFFFFFFFF
#define REFLECT_DATA		1
#define REFLECT_REMAINDER	1
#define CHECK_VALUE			0xCBF43926

#else

#error "One of CRC_CCITT, CRC16, or CRC32 must be #define'd."

#endif


void crcInit(struct data_tx_rx * data_tx_rx_mod);
crc crcSlow(unsigned char const message[], int nBytes);
crc crcFast(unsigned char const message[], int nBytes, struct data_tx_rx * data_tx_rx_mod);


#endif //_BLU5_STRUCT_DEFINES


#ifdef __cplusplus
}
#endif

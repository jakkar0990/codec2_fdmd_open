/*---------------------------------------------------------------------------*\

  FILE........: freedv_api.c
  AUTHOR......: David Rowe
  DATE CREATED: August 2014

  Library of API functions that implement FreeDV "modes", useful for
  embedding FreeDV in other programs.

\*---------------------------------------------------------------------------*/

/*
  Copyright (C) 2014 David Rowe 

  All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2.1, as
  published by the Free Software Foundation.  This program is
  distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif /* __APPLE__ */

#include "fsk.h"
#include "fmfsk.h"
#include "codec2.h"
#include "codec2_fdmdv.h"
#include "fdmdv_internal.h"
#include "golay23.h"
#include "varicode.h"
#include "freedv_api.h"
#include "freedv_api_internal.h"
#include "freedv_vhf_framing.h"
#include "comp_prim.h"
#include "codec2_ofdm.h"
#include "ofdm_internal.h"
#include "mpdecode_core.h"
#include "gp_interleaver.h"
#include "interldpc.h"


/*----------------------------------------------------------------------
	Jak Blu5 Header File:
	
	Unfortunately currently don't know how to get working with this header
	- ideally seperate source file and header and make part of compilation
	- NOT CRITICAL!
-----------------------------------------------------------------------*/


/* Please include the blu5_defines.h file for now!*/

//BLU5_HF_RADIO_MODS ----------
#include "blu5_defines.h"
#include "blu5_struct_defines.h"


#define VERSION     11    /* The API version number.  The first version
                           is 10.  Increment if the API changes in a
                           way that would require changes by the API
                           user. */
/*
 * Version 10   Initial version August 2, 2015.
 * Version 11   September 2015
 *              Added: freedv_zero_total_bit_errors(), freedv_get_sync()
 *              Changed all input and output sample rates to 8000 sps.  Rates for FREEDV_MODE_700 and 700B were 7500.
 */

/* experimentally derived fudge factors to normalise power across modes */

#define NORM_PWR_COHPSK  1.74   
#define NORM_PWR_FSK     0.193 
#define NORM_PWR_OFDM    1.00


/* OFDM payload data test frame for 700D */

extern int payload_data_bits[];

/**********************************************************************************

                            Start of Jak Functions Defs

***********************************************************************************/

//---------------------------
// CRC Forward Declerations
//---------------------------



/**********************************************************************************

     Jak Function: Tx/Rx Function with FDMDV Modulation

***********************************************************************************/


// Test Function ---------------------------------------------------

void jak_counter(){
	
	int i;
	i++;
	
}


//----------------------------------------------------------------
// Transmit function for modified data transmit
// Orig Function: freedv_comptx_fdmdv_1600  
//----------------------------------------------------------------
void jak_data_fdmdv_mod_1600(struct freedv *f, short mod_out1[], unsigned char * mod_in, struct arq_machine * arq_mod, uint8_t * op_mode) {

	int    bit, byte, i, j;
	int    bits_per_codec_frame, bits_per_modem_frame;
	int    data, codeword1, data_flag_index;
	COMP   tx_fdm[f->n_nat_modem_samples];
	// Jak Edit
	COMP	 mod_out[f->n_nat_modem_samples];
	// Jak Edit
	bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
	bits_per_modem_frame = fdmdv_bits_per_frame(f->fdmdv);

	// Rollover and retransmit string

	// memcpy(f->packed_codec_bits, &(mod_in[(*off)]), 6);	

	// Offset value
	if (*op_mode == DATA_ARQ) {
		memcpy(f->packed_codec_bits, mod_in, ARQ_BYTES_PER_FRAME);
		f->packed_codec_bits[5] = arq_mod->arq_tx_frame_indexer;
		//memcpy(f->packed_codec_bits + 5,( (const char *) (arq_mod->arq_tx_frame_indexer + 1) ),1); // Frame Index value being the delimiter is accounted for
	}
	else {
		memcpy(f->packed_codec_bits, mod_in, UART_BYTES_PER_FRAME);
	}

#ifdef __CRC_16__

	/*------------------------------------------
	// Jak Edit: 20190118 - 0813
	//------------------------------------------
	// Here the bytes are put into individual bits
	// for easier maintaining.
	//----------------------------------------*/
	if (*op_mode == DATA_ARQ) {
		if (arq_mod->crc_code_ext_calc[arq_mod->arq_tx_frame_indexer][1] == 0) {
			arq_mod->data_modem->crc_code = crcFast(f->packed_codec_bits, 6, arq_mod->data_modem);
			arq_mod->crc_code_ext_calc[arq_mod->arq_tx_frame_indexer][1] = arq_mod->data_modem->crc_code;
		}
		arq_mod->data_modem->crc_code = arq_mod->crc_code_ext_calc[arq_mod->arq_tx_frame_indexer][1];
	}
	else {
		arq_mod->data_modem->crc_code = crcFast(f->packed_codec_bits, 6, arq_mod->data_modem);
	}
	/* unpack bits, MSB first */

	bit = 7; byte = 0;
	for (i = 0; i < 8 * 6; i++) {
		f->tx_bits[i] = (f->packed_codec_bits[byte] >> bit) & 0x1;
		bit--;
		if (bit < 0) {
			bit = 7;
			byte++;
		}
	}

	for (bit = 15; i < 8*6 + 16; i++) {
		f->tx_bits[i] = (arq_mod->data_modem->crc_code >> bit) & 0x1;
		bit--;
	}


#else

	/******************************************************************
	Jak Edit: 190118 - 0716:
	------------------------
	So here is where the Golay Code is calculated for the DV protocol.

	For now have been using this FEC to detect any errors in the frames
	Unfortunately the bit errors detected were only for those bits
	protected by the FEC.
	*******************************************************************/

	/**************************************************************************
	Jak Edit: 20180930 - 0832
	FEC Coder: Here redundant bits are added to the data to provide ability to
						 detect and correct errors in the data.
	***************************************************************************/
	//------------------------------------------
	//  FEC Coder
	//------------------------------------------

	/* Protect first 12 out of first 16 excitation bits with (23,12) Golay Code:

	0,1,2,3: v[0]..v[1]
	4,5,6,7: MSB of pitch
	11,12,13,14: MSB of energy

	*/

	data = 0;
	for (i = 0; i<8; i++) {
		data <<= 1;
		data |= f->codec_bits[i];
	}
	for (i = 11; i<15; i++) {
		data <<= 1;
		data |= f->codec_bits[i];
	}
	codeword1 = golay23_encode(data);

	/* now pack output frame with parity bits at end to make them
	as far apart as possible from the data they protect.  Parity
	bits are LSB of the Golay codeword */

	for (i = 0; i<bits_per_codec_frame; i++)
		f->tx_bits[i] = f->codec_bits[i];

	for (j = 0; i<bits_per_codec_frame + 11; i++, j++) {
		f->tx_bits[i] = (codeword1 >> (10 - j)) & 0x1;
	}
	f->tx_bits[i] = 0; /* spare bit */

#endif //CRC16
	
	//------------------------------------------
	// Jak Edit
	// End of FEC Coder
	//------------------------------------------
	
	/* optionally overwrite with test frames */

	if (f->test_frames) {
		fdmdv_get_test_bits(f->fdmdv, f->tx_bits);
		fdmdv_get_test_bits(f->fdmdv, &f->tx_bits[bits_per_modem_frame]);
		//fprintf(stderr, "test frames on tx\n");
	}

	/* modulate even and odd frames */

	fdmdv_mod(f->fdmdv, tx_fdm, f->tx_bits, &f->tx_sync_bit);
	assert(f->tx_sync_bit == 1);

	fdmdv_mod(f->fdmdv, &tx_fdm[FDMDV_NOM_SAMPLES_PER_FRAME], &f->tx_bits[bits_per_modem_frame], &f->tx_sync_bit);
	assert(f->tx_sync_bit == 0);

	assert(2 * FDMDV_NOM_SAMPLES_PER_FRAME == f->n_nom_modem_samples);

	for (i = 0; i<f->n_nom_modem_samples; i++)
		mod_out[i] = fcmult(FDMDV_SCALE, tx_fdm[i]);

	for (i = 0; i<f->n_nom_modem_samples; i++)
		mod_out1[i] = mod_out[i].real;
}





//----------------------------------------------------------------
// Same as above but outputs complex values
// 
//----------------------------------------------------------------

#ifndef EMBEDDED

void jak_data_fdmdv_mod_1600_COMP(struct freedv *f, COMP mod_out1[], unsigned char * mod_in,struct arq_machine * arq_mod, uint8_t * op_mode) {

	int    bit, byte, i, j;
	int    bits_per_codec_frame, bits_per_modem_frame;
	int    data, codeword1, data_flag_index;
	COMP   tx_fdm[f->n_nat_modem_samples];
	// Jak Edit
	COMP	 mod_out[f->n_nat_modem_samples];
	// Jak Edit
	bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
	bits_per_modem_frame = fdmdv_bits_per_frame(f->fdmdv);

	// Rollover and retransmit string

	// memcpy(f->packed_codec_bits, &(mod_in[(*off)]), 6);	

	// Offset value
	if (*op_mode == DATA_ARQ) {
		memcpy(f->packed_codec_bits, mod_in, ARQ_BYTES_PER_FRAME);
		f->packed_codec_bits[5] = arq_mod->arq_tx_frame_indexer;
		//memcpy(f->packed_codec_bits + 5,( (const char *) (arq_mod->arq_tx_frame_indexer + 1) ),1); // Frame Index value being the delimiter is accounted for
	}
	else {
		memcpy(f->packed_codec_bits, mod_in, UART_BYTES_PER_FRAME);
	}

	/*------------------------------------------
	// Jak Edit: 20190118 - 0813
	//------------------------------------------
	// Here the bytes are put into individual bits
	// for easier maintaining.
	//----------------------------------------*/

	/* unpack bits, MSB first */

	bit = 7; byte = 0;
	for (i = 0; i < bits_per_codec_frame; i++) {
		f->codec_bits[i] = (f->packed_codec_bits[byte] >> bit) & 0x1;
		bit--;
		if (bit < 0) {
			bit = 7;
			byte++;
		}
	}

	// spare bit in frame that codec defines.  Use this 1
	// bit/frame to send txt messages

	data_flag_index = codec2_get_spare_bit_index(f->codec2);

	if (f->nvaricode_bits) {
		f->codec_bits[data_flag_index] = f->tx_varicode_bits[f->varicode_bit_index++];
		f->nvaricode_bits--;
	}

	if (f->nvaricode_bits == 0) {
		/* get new char and encode */
		char s[2];
		if (f->freedv_get_next_tx_char != NULL) {
			s[0] = (*f->freedv_get_next_tx_char)(f->callback_state);
			f->nvaricode_bits = varicode_encode(f->tx_varicode_bits, s, VARICODE_MAX_BITS, 1, 1);
			f->varicode_bit_index = 0;
		}
	}

	uint8_t num_of_data_bits = 0;

	if (*op_mode == DATA_ARQ) {
		num_of_data_bits = 8 * ARQ_BYTES_PER_FRAME;
	}
	else {
		num_of_data_bits = 8 * UART_BYTES_PER_FRAME;
	}

#ifdef __CRC_16__


	arq_mod->data_modem->crc_code = crcFast(mod_in, 6, arq_mod->data_modem);

	for (i = 0; i < num_of_data_bits; i++)
		f->tx_bits[i] = f->codec_bits[i];

	for (bit = 15; i < num_of_data_bits + 16; i++) {
		f->tx_bits[i] = (arq_mod->data_modem->crc_code >> bit) & 0x1;
		bit--;
	}

#else

	/******************************************************************
	Jak Edit: 190118 - 0716:
	------------------------
	So here is where the Golay Code is calculated for the DV protocol.

	For now have been using this FEC to detect any errors in the frames
	Unfortunately the bit errors detected were only for those bits
	protected by the FEC.
	*******************************************************************/

	/**************************************************************************
	Jak Edit: 20180930 - 0832
	FEC Coder: Here redundant bits are added to the data to provide ability to
						 detect and correct errors in the data.
	***************************************************************************/
	//------------------------------------------
	//  FEC Coder
	//------------------------------------------

	/* Protect first 12 out of first 16 excitation bits with (23,12) Golay Code:

	0,1,2,3: v[0]..v[1]
	4,5,6,7: MSB of pitch
	11,12,13,14: MSB of energy

	*/

	data = 0;
	for (i = 0; i < 8; i++) {
		data <<= 1;
		data |= f->codec_bits[i];
	}
	for (i = 11; i < 15; i++) {
		data <<= 1;
		data |= f->codec_bits[i];
	}
	codeword1 = golay23_encode(data);

	/* now pack output frame with parity bits at end to make them
	as far apart as possible from the data they protect.  Parity
	bits are LSB of the Golay codeword */

	for (i = 0; i < bits_per_codec_frame; i++)
		f->tx_bits[i] = f->codec_bits[i];

	for (j = 0; i < bits_per_codec_frame + 11; i++, j++) {
		f->tx_bits[i] = (codeword1 >> (10 - j)) & 0x1;
	}
	f->tx_bits[i] = 0; /* spare bit */

#endif //CRC16

	//------------------------------------------
	// Jak Edit
	// End of FEC Coder
	//------------------------------------------

	/* optionally overwrite with test frames */

	if (f->test_frames) {
		fdmdv_get_test_bits(f->fdmdv, f->tx_bits);
		fdmdv_get_test_bits(f->fdmdv, &f->tx_bits[bits_per_modem_frame]);
		//fprintf(stderr, "test frames on tx\n");
	}

	/* modulate even and odd frames */

	fdmdv_mod(f->fdmdv, tx_fdm, f->tx_bits, &f->tx_sync_bit);
	assert(f->tx_sync_bit == 1);

	fdmdv_mod(f->fdmdv, &tx_fdm[FDMDV_NOM_SAMPLES_PER_FRAME], &f->tx_bits[bits_per_modem_frame], &f->tx_sync_bit);
	assert(f->tx_sync_bit == 0);

	assert(2 * FDMDV_NOM_SAMPLES_PER_FRAME == f->n_nom_modem_samples);

	for (i = 0; i < f->n_nom_modem_samples; i++)
		mod_out1[i] = fcmult(FDMDV_SCALE, tx_fdm[i]);
}


#endif // EMBEDDED


//------------------------------------------------------------------


//------------------------------------------------------------------
// Original Function : freedv_comprx_fdmdv_1600
//
// This function is the fundamental receive for FREEDV I think.
//
//------------------------------------------------------------------
int jak_data_fdmdv_demod_1600(struct freedv *f, unsigned char* speech_out, short demod_in[], struct arq_machine * arq_mod,
							struct data_tx_rx * data_tx_rx_mod)
{
	int                 bits_per_codec_frame, bytes_per_codec_frame, bits_per_fdmdv_frame;
	int                 i, j, bit, byte, nin_prev, nout;
	int                 recd_codeword, codeword1, data_flag_index, n_ascii;
	short               abit[1];
	char                ascii_out;
	int                 reliable_sync_bit;

	
	//--------------------------------------
	// Previous Code for mode 1600
	//--------------------------------------
	int nin = freedv_nin(f);

	COMP rx_fdm[f->n_max_modem_samples];

	for (i = 0; i<nin; i++) {
		rx_fdm[i].real = (float)demod_in[i];
		rx_fdm[i].imag = 0.0;
	}

	//------------------------------------------
	bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
	bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
	nout = f->n_speech_samples;
	//------------------------------------------
	bits_per_fdmdv_frame = fdmdv_bits_per_frame(f->fdmdv);

	nin_prev = f->nin;
	data_fdmdv_demod(f->fdmdv, f->fdmdv_bits, &reliable_sync_bit, rx_fdm, &f->nin);
	//fdmdv_demod(f->fdmdv, f->fdmdv_bits, &reliable_sync_bit, rx_fdm, &f->nin);
	fdmdv_get_demod_stats(f->fdmdv, &f->stats);
	f->sync = f->fdmdv->sync;
	
	//--------------------------------------
	// Jak Demod Sync
	//--------------------------------------

	data_tx_rx_mod->rx_demod_sync = f->fdmdv->sync;

	f->snr_est = f->stats.snr_est;
	
	if (f->evenframe == 0) {
		memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
		nout = 0;
	}

	if (reliable_sync_bit == 1) {
		f->evenframe = 1;
	}

	if (f->stats.sync) {
		if (f->evenframe == 0) {
			memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
			nout = 0;
			//*valid = 0;
		}
		else {
				memcpy(&f->rx_bits[bits_per_fdmdv_frame], f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));

				bit = 7;
				byte = 0;
				memset(speech_out, 0, 8);
				for (i = 0; i < 8 * 8; i++) {

					speech_out[byte] |= (f->rx_bits[i] << bit);
					bit--;
					if (bit < 0) {
						bit = 7;
						byte++;
					}
				}

#ifdef __CRC_16__
				
				//memcpy(&arq_mod->data_modem->crc_var_ext, speech_out + 6, 2);

				arq_mod->data_modem->crc_var_ext = (((unsigned short)speech_out[6]) << 8) | (0x00ff & speech_out[7]);
				
				arq_mod->data_modem->crc_var_calc = crcFast(speech_out, 6, arq_mod->data_modem);

				f->total_bits += 8 * 8;

				//Capturing CRC codes extracted and calculated

				arq_mod->arq_rx_frame_indexer = speech_out[5];

				//------------------------------------------------------------------------------
				// These values will be updated accordingly depending on if the frame array
				// position was filled or not
				//------------------------------------------------------------------------------
				if ((arq_mod->arq_rx_frame_indexer < ARQ_MAX_FRAMES) && (arq_mod->arq_rx_frame_indexer >= 0)) {
					if ((((const char *)arq_mod->data_buffers->data_text_rx[arq_mod->arq_rx_frame_indexer][0]) == 0)) {
						arq_mod->crc_code_ext_calc[arq_mod->arq_rx_frame_indexer][0] = data_tx_rx_mod->crc_var_ext;
						arq_mod->crc_code_ext_calc[arq_mod->arq_rx_frame_indexer][1] = data_tx_rx_mod->crc_var_calc;
					}
				}
				if (arq_mod->data_modem->crc_var_ext == arq_mod->data_modem->crc_var_calc) {

					arq_mod->data_modem->bit_err_det = 0;
					arq_mod->debug_counter_1++;
				}
				else {

					arq_mod->data_modem->bit_err_det = 8*8;
					f->total_bit_errors += 8 * 8;
					arq_mod->debug_counter_2++;
				}

#else
				f->total_bits += 23;
			    recd_codeword = 0;
					for(i=0; i<8; i++) {
							recd_codeword <<= 1;
							recd_codeword |= (f->rx_bits[i] & 0x1);
					}
					for(i=11; i<15; i++) {
							recd_codeword <<= 1;
							recd_codeword |= (f->rx_bits[i] & 0x1);
					}
					for(i=bits_per_codec_frame; i<bits_per_codec_frame+11; i++) {
							recd_codeword <<= 1;
							recd_codeword |= (f->rx_bits[i] & 0x1);
					}
					codeword1 = golay23_decode(recd_codeword);

					// JAK EDIT: 1249 - 25/11/18
					// CHANGING THE CODE SO AS TO USE BIT ERROR DETECTION FOR FRAME CONTROL
					// NEW CODE:
					//if (bit_errors != 0){
					//	data_tx_rx_mod->bit_err_det = 1;
					//}

					data_tx_rx_mod->bit_err_det = golay23_count_errors(recd_codeword, codeword1);

					f->total_bit_errors += data_tx_rx_mod->bit_err_det;

#endif // __CRC_16__


			memset(f->packed_codec_bits,0,6);
	
			// Squelch if beneath SNR threshold or test frames enabled 

			if ((f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) || f->test_frames) {
				//fprintf(stderr,"squelch %f %f !\n", f->stats.snr_est, f->snr_squelch_thresh);
				//*valid = 0;
			}

			nout = f->n_speech_samples;

		}

		// note this freewheels if reliable sync dissapears on bad channels 

		if (f->evenframe)
			f->evenframe = 0;
		else
			f->evenframe = 1;

	}

	return nout;
}



//**********************************************************************************



/**********************************************************************************

                                End of Jak Functions

***********************************************************************************/




/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_open
  AUTHOR......: David Rowe
  DATE CREATED: 3 August 2014

  Call this first to initialise.  Returns NULL if initialisation fails
  (e.g. out of memory or mode not supported).

\*---------------------------------------------------------------------------*/

struct freedv *freedv_open(int mode) {
    return freedv_open_advanced(mode, NULL);
}

struct freedv *freedv_open_advanced(int mode, struct freedv_advanced *adv) {
    struct freedv *f;
    int            Nc, codec2_mode, nbit, nbyte;

    if ((mode != FREEDV_MODE_1600) && (mode != FREEDV_MODE_700) && 
        (mode != FREEDV_MODE_700B) && (mode != FREEDV_MODE_2400A) &&
        (mode != FREEDV_MODE_2400B) && (mode != FREEDV_MODE_800XA) &&
        (mode != FREEDV_MODE_700C) && (mode != FREEDV_MODE_700D) )
        return NULL;
    f = (struct freedv*)malloc(sizeof(struct freedv));
    if (f == NULL)
        return NULL;

    f->mode = mode;
    f->verbose = 0;
		// Jak Comment ----------------------------------------------------------------------------------------------
    f->test_frames = f->smooth_symbols = 0;  // I guess here he's just initialising some parameters of the modem
    f->freedv_put_error_pattern = NULL;      // -----------------------------------------------------------------
    f->error_pattern_callback_state = NULL;
    f->n_protocol_bits = 0;
    f->frames = 0;
    
    /* Init states for this mode, and set up samples in/out -----------------------------------------*/
    
    if (mode == FREEDV_MODE_1600) {
        f->snr_squelch_thresh = 2.0;                                  
        f->squelch_en = 1;
        Nc = 16;
        f->tx_sync_bit = 0;
        codec2_mode = CODEC2_MODE_1300;
        f->fdmdv = fdmdv_create(Nc);
        if (f->fdmdv == NULL)
            return NULL;
        golay23_init();
        f->nin = FDMDV_NOM_SAMPLES_PER_FRAME;
        f->n_nom_modem_samples = 2*FDMDV_NOM_SAMPLES_PER_FRAME;
        f->n_nat_modem_samples = f->n_nom_modem_samples;
        f->n_max_modem_samples = FDMDV_NOM_SAMPLES_PER_FRAME+FDMDV_MAX_SAMPLES_PER_FRAME;
        f->modem_sample_rate = FS;
        nbit = fdmdv_bits_per_frame(f->fdmdv);
        f->fdmdv_bits = (int*)malloc(nbit*sizeof(int));
        if (f->fdmdv_bits == NULL)
            return NULL;
        nbit = 2*fdmdv_bits_per_frame(f->fdmdv);
        f->tx_bits = (int*)malloc(nbit*sizeof(int));
        f->rx_bits = (int*)malloc(nbit*sizeof(int));
        if ((f->tx_bits == NULL) || (f->rx_bits == NULL))
            return NULL;
        f->evenframe = 0;
        f->sz_error_pattern = fdmdv_error_pattern_size(f->fdmdv);
    }

#if defined(MODES_700)
    if ((mode == FREEDV_MODE_700) || (mode == FREEDV_MODE_700B) || (mode == FREEDV_MODE_700C)) {
        f->snr_squelch_thresh = 0.0;
        f->squelch_en = 0;
        switch(mode) {
        case FREEDV_MODE_700:
            codec2_mode = CODEC2_MODE_700;
            break;
        case FREEDV_MODE_700B:
            codec2_mode = CODEC2_MODE_700B;
            break;
        case FREEDV_MODE_700C:
            codec2_mode = CODEC2_MODE_700C;
            break;
        default:
            assert(0);
        }

        f->cohpsk = cohpsk_create();
        f->nin = COHPSK_NOM_SAMPLES_PER_FRAME;
        f->n_nat_modem_samples = COHPSK_NOM_SAMPLES_PER_FRAME;             // native modem samples as used by the modem
        f->n_nom_modem_samples = f->n_nat_modem_samples * FS / COHPSK_FS;  // number of samples after native samples are interpolated to 8000 sps
        f->n_max_modem_samples = COHPSK_MAX_SAMPLES_PER_FRAME * FS / COHPSK_FS + 1;
        f->modem_sample_rate = FS;                                         /* note wierd sample rate tamed by interpolator */
        f->clip = 1;
        nbit = COHPSK_BITS_PER_FRAME;
        f->tx_bits = (int*)malloc(nbit*sizeof(int));
        if (f->tx_bits == NULL)
            return NULL;
        f->sz_error_pattern = cohpsk_error_pattern_size();
    }
   
    if (mode == FREEDV_MODE_700D) {
        /*
          TODO:
            [ ] how to set up interleaver, prob init time option best, as many arrays depend on it
            [ ] clip option?  Haven't tried clipping OFDM waveform yet
            [ ] support for uncoded and coded error patterns
        */
        
        f->snr_squelch_thresh = 0.0;
        f->squelch_en = 0;
        codec2_mode = CODEC2_MODE_700C;
        f->ofdm = ofdm_create(OFDM_CONFIG_700D);
        f->ldpc = (struct LDPC*)malloc(sizeof(struct LDPC));
        if (f->ldpc == NULL) { return NULL; }
        set_up_hra_112_112(f->ldpc);
        int coded_syms_per_frame = f->ldpc->coded_syms_per_frame;
        
        if (adv == NULL) {
            f->interleave_frames = 1;
        } else {
            assert((adv->interleave_frames >= 0) && (adv->interleave_frames <= 16));
            f->interleave_frames = adv->interleave_frames;
        }
        f->modem_frame_count_tx = f->modem_frame_count_rx = 0;
        
        f->codeword_symbols = (COMP*)malloc(sizeof(COMP)*f->interleave_frames*coded_syms_per_frame);
        if (f->codeword_symbols == NULL) {return NULL;}
        f->codeword_amps = (float*)malloc(sizeof(float)*f->interleave_frames*coded_syms_per_frame);
        if (f->codeword_amps == NULL) {return NULL;}
        for (int i=0; i<f->interleave_frames*coded_syms_per_frame; i++) {
            f->codeword_symbols[i].real = 0.0;
            f->codeword_symbols[i].imag = 0.0;
            f->codeword_amps[i] = 0.0;
        }

        f->nin = ofdm_get_samples_per_frame();
        f->n_nat_modem_samples = ofdm_get_samples_per_frame();
        f->n_nom_modem_samples = ofdm_get_samples_per_frame();
        f->n_max_modem_samples = ofdm_get_max_samples_per_frame();
        f->modem_sample_rate = OFDM_FS;
        f->clip = 0;
        nbit = OFDM_BITSPERFRAME;
        f->tx_bits = NULL; /* not used for 700D */
        f->sz_error_pattern = OFDM_BITSPERFRAME; /* uncoded errors */

        f->mod_out = (COMP*)malloc(sizeof(COMP)*f->interleave_frames*f->n_nat_modem_samples);
        if (f->mod_out == NULL) { return NULL; }
        for (int i=0; i<f->interleave_frames*f->n_nat_modem_samples; i++) {
            f->mod_out[i].real = 0.0;
            f->mod_out[i].imag = 0.0;
        }

        /* tx BPF on by default, can't see any reason we'd want this off */
        
        ofdm_set_tx_bpf(f->ofdm, 1);
    }
#endif  

    if ((mode == FREEDV_MODE_2400A) || (mode == FREEDV_MODE_2400B)) {
      
        /* Set up the C2 mode */
        codec2_mode = CODEC2_MODE_1300;
        /* Set the number of protocol bits */
        f->n_protocol_bits = 20;
        f->sz_error_pattern = 0;
    }
    
    if (mode == FREEDV_MODE_2400A) {
        /* Create the framer|deframer */
        f->deframer = fvhff_create_deframer(FREEDV_VHF_FRAME_A,0);
        if(f->deframer == NULL)
            return NULL;
  
        f->fsk = fsk_create_hbr(48000,1200,10,4,1200,1200);
        
        /* Note: fsk expects tx/rx bits as an array of uint8_ts, not ints */
        f->tx_bits = (int*)malloc(f->fsk->Nbits*sizeof(uint8_t));
        
        if(f->fsk == NULL){
            fvhff_destroy_deframer(f->deframer);
            return NULL;
        }
        
        f->n_nom_modem_samples = f->fsk->N;
        f->n_max_modem_samples = f->fsk->N + (f->fsk->Ts);
        f->n_nat_modem_samples = f->fsk->N;
        f->nin = fsk_nin(f->fsk);
        f->modem_sample_rate = 48000;
        /* Malloc something to appease freedv_init and freedv_destroy */
        f->codec_bits = malloc(1);
    }
    
    if (mode == FREEDV_MODE_2400B) {
        /* Create the framer|deframer */
        f->deframer = fvhff_create_deframer(FREEDV_VHF_FRAME_A,1);
        if(f->deframer == NULL)
            return NULL;
        
        f->fmfsk = fmfsk_create(48000,2400);
         
        if(f->fmfsk == NULL){
            fvhff_destroy_deframer(f->deframer);
            return NULL;
        }
        /* Note: fsk expects tx/rx bits as an array of uint8_ts, not ints */
        f->tx_bits = (int*)malloc(f->fmfsk->nbit*sizeof(uint8_t));
        
        f->n_nom_modem_samples = f->fmfsk->N;
        f->n_max_modem_samples = f->fmfsk->N + (f->fmfsk->Ts);
        f->n_nat_modem_samples = f->fmfsk->N;
        f->nin = fmfsk_nin(f->fmfsk);
        f->modem_sample_rate = 48000;
        /* Malloc something to appease freedv_init and freedv_destroy */
        f->codec_bits = malloc(1);
    }
    
    if (mode == FREEDV_MODE_800XA) {
        /* Create the framer|deframer */
        f->deframer = fvhff_create_deframer(FREEDV_HF_FRAME_B,0);
        if(f->deframer == NULL)
            return NULL;
  
        f->fsk = fsk_create_hbr(8000,400,10,4,800,400);
        fsk_set_nsym(f->fsk,32);
        
        /* Note: fsk expects tx/rx bits as an array of uint8_ts, not ints */
        f->tx_bits = (int*)malloc(f->fsk->Nbits*sizeof(uint8_t));
        
        if(f->fsk == NULL){
            fvhff_destroy_deframer(f->deframer);
            return NULL;
        }
        
        f->n_nom_modem_samples = f->fsk->N;
        f->n_max_modem_samples = f->fsk->N + (f->fsk->Ts);
        f->n_nat_modem_samples = f->fsk->N;
        f->nin = fsk_nin(f->fsk);
        f->modem_sample_rate = 8000;
        /* Malloc something to appease freedv_init and freedv_destroy */
        f->codec_bits = malloc(1);
        
        f->n_protocol_bits = 0;
        codec2_mode = CODEC2_MODE_700C;
        fsk_stats_normalise_eye(f->fsk, 0);
        f->sz_error_pattern = 0;
    }

    /* Init test frame states */
    
    f->test_frames_diversity = 1;
    f->test_frame_sync_state = 0;
    f->test_frame_sync_state_upper = 0;
    f->total_bits = 0;
    f->total_bit_errors = 0;
    f->total_bits_coded = 0;
    f->total_bit_errors_coded = 0;

    /* Init Codec 2 for this FreeDV mode ----------------------------------------------------*/
    
    f->codec2 = codec2_create(codec2_mode);
    if (f->codec2 == NULL)
        return NULL;

    /* work out how many codec 2 frames per mode frame, and number of
       bytes of storage for packed and unpacket bits.  TODO: do we really
       need to work in packed bits at all?  It's messy, chars would probably
       be OK.... */
    
    if ((mode == FREEDV_MODE_1600) || (mode == FREEDV_MODE_2400A) || (mode == FREEDV_MODE_2400B)) {
        f->n_speech_samples = codec2_samples_per_frame(f->codec2);
        f->n_codec_bits = codec2_bits_per_frame(f->codec2);
        nbit = f->n_codec_bits;
        nbyte = (nbit + 7) / 8;
    } else if (mode == FREEDV_MODE_800XA) {
        f->n_speech_samples = 2*codec2_samples_per_frame(f->codec2);
        f->n_codec_bits = codec2_bits_per_frame(f->codec2);
        nbit = f->n_codec_bits;
        nbyte = (nbit + 7) / 8;
        nbyte = nbyte*2;
        nbit = 8*nbyte;
        f->n_codec_bits = nbit;
    } else if ((mode == FREEDV_MODE_700) || (mode == FREEDV_MODE_700B) || (mode == FREEDV_MODE_700C)) {
        f->n_speech_samples = 2*codec2_samples_per_frame(f->codec2);
        f->n_codec_bits = 2*codec2_bits_per_frame(f->codec2);
        nbit = f->n_codec_bits;
        nbyte = 2*((codec2_bits_per_frame(f->codec2) + 7) / 8);
    } else /* mode == FREEDV_MODE_700D */ {

        /* should be exactly an integer number ofCodec 2 frames in a OFDM modem frame */

        assert((f->ldpc->data_bits_per_frame % codec2_bits_per_frame(f->codec2)) == 0);

        int Ncodec2frames = f->ldpc->data_bits_per_frame/codec2_bits_per_frame(f->codec2);
        f->n_speech_samples = Ncodec2frames*codec2_samples_per_frame(f->codec2);
        f->n_codec_bits = f->interleave_frames*Ncodec2frames*codec2_bits_per_frame(f->codec2);
        nbit = codec2_bits_per_frame(f->codec2);
        nbyte = (nbit + 7) / 8;
        nbyte = nbyte*Ncodec2frames*f->interleave_frames;
        f->nbyte_packed_codec_bits = nbyte;
        fprintf(stderr, "Ncodec2frames: %d n_speech_samples: %d n_codec_bits: %d nbit: %d  nbyte: %d\n",
                Ncodec2frames, f->n_speech_samples, f->n_codec_bits, nbit, nbyte);
        f->packed_codec_bits_tx = (unsigned char*)malloc(nbyte*sizeof(char));
        f->codec_bits = NULL;
    }
    f->packed_codec_bits = (unsigned char*)malloc(nbyte*sizeof(char));
    if (mode == FREEDV_MODE_1600)
        f->codec_bits = (int*)malloc(nbit*sizeof(int));
    if ((mode == FREEDV_MODE_700) || (mode == FREEDV_MODE_700B) || (mode == FREEDV_MODE_700C))
        f->codec_bits = (int*)malloc(COHPSK_BITS_PER_FRAME*sizeof(int));
    
    /* Note: VHF Framer/deframer goes directly from packed codec/vc/proto bits to filled frame */

    if (f->packed_codec_bits == NULL)
        return NULL;

    /* Sample rate conversion for modes using COHPSK */
    
    if ((mode == FREEDV_MODE_700) || (mode == FREEDV_MODE_700B) || (mode == FREEDV_MODE_700C) ) { 
        f->ptFilter7500to8000 = (struct quisk_cfFilter *)malloc(sizeof(struct quisk_cfFilter));
        f->ptFilter8000to7500 = (struct quisk_cfFilter *)malloc(sizeof(struct quisk_cfFilter));
        quisk_filt_cfInit(f->ptFilter8000to7500, quiskFilt120t480, sizeof(quiskFilt120t480)/sizeof(float));
        quisk_filt_cfInit(f->ptFilter7500to8000, quiskFilt120t480, sizeof(quiskFilt120t480)/sizeof(float));
    }
    else {
        f->ptFilter7500to8000 = NULL;
        f->ptFilter8000to7500 = NULL;
    }

    /* Varicode low bit rate text states */
    
    varicode_decode_init(&f->varicode_dec_states, 1);
    f->nvaricode_bits = 0;
    f->varicode_bit_index = 0;
    f->freedv_get_next_tx_char = NULL;
    f->freedv_put_next_rx_char = NULL;
		f->freedv_put_next_proto = NULL;
		f->freedv_get_next_proto = NULL;
    f->total_bit_errors = 0;

    return f;
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_close
  AUTHOR......: David Rowe
  DATE CREATED: 3 August 2014

  Frees up memory.

\*---------------------------------------------------------------------------*/

void freedv_close(struct freedv *freedv) {
    assert(freedv != NULL);

    free(freedv->packed_codec_bits);
    free(freedv->codec_bits);
    free(freedv->tx_bits);
    if (freedv->mode == FREEDV_MODE_1600)
        fdmdv_destroy(freedv->fdmdv);
#ifndef CORTEX_M4
    if ((freedv->mode == FREEDV_MODE_700) || (freedv->mode == FREEDV_MODE_700B) || (freedv->mode == FREEDV_MODE_700C))
        cohpsk_destroy(freedv->cohpsk);
    if (freedv->mode == FREEDV_MODE_700D) {
        free(freedv->packed_codec_bits_tx);
        free(freedv->mod_out);
        free(freedv->codeword_symbols);
        free(freedv->codeword_amps);
        free(freedv->ldpc);
        ofdm_destroy(freedv->ofdm);
    }
#endif
    if ((freedv->mode == FREEDV_MODE_2400A) || (freedv->mode == FREEDV_MODE_800XA)){
        fsk_destroy(freedv->fsk);
        fvhff_destroy_deframer(freedv->deframer);
	}
    
    if (freedv->mode == FREEDV_MODE_2400B){
        fmfsk_destroy(freedv->fmfsk);
		fvhff_destroy_deframer(freedv->deframer);
    }
    
    codec2_destroy(freedv->codec2);
    if (freedv->ptFilter8000to7500) {
        quisk_filt_destroy(freedv->ptFilter8000to7500);
        free(freedv->ptFilter8000to7500);
        freedv->ptFilter8000to7500 = NULL;
    }
    if (freedv->ptFilter7500to8000) {
        quisk_filt_destroy(freedv->ptFilter7500to8000);
        free(freedv->ptFilter7500to8000);
        freedv->ptFilter7500to8000 = NULL;
    }
    free(freedv);
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_tx
  AUTHOR......: David Rowe
  DATE CREATED: 3 August 2014

  Takes a frame of input speech samples, encodes and modulates them to
  produce a frame of modem samples that can be sent to the
  transmitter.  See freedv_tx.c for an example.

  speech_in[] is sampled at 8000 Hz, and the user must supply a block
  of exactly freedv_get_n_speech_samples(). The speech_in[] level
  should be such that the peak speech level is between +/- 16384 and
  +/- 32767.

  The FDM modem signal mod_out[] is sampled at 8000 Hz and is
  freedv_get_n_nom_modem_samples() long.  mod_out[] will be scaled
  such that the peak level is just less than +/-32767.

  The complex-valued output can directly drive an I/Q modulator to
  produce a single sideband signal.  To generate the other sideband,
  take the complex conjugate of mod_out[].

  The FreeDV 1600 modem has a high crest factor (around 12dB), however
  the energy and duration of the peaks is small.  FreeDV 1600 is
  usually operated at a "backoff" of 8dB.  Adjust the power amplifier
  drive so that the average power is 8dB less than the peak power of
  the PA.  For example, on a radio rated at 100W PEP for SSB, the
  average FreeDV power is typically 20W.

  The FreeDV 700 modem has a crest factor of about 8dB (with
  f->clip=1, the default), so if your PA can handle it, it can be
  driven harder than FreeDV 1600.  Caution - some PAs cannot handle a
  high continuous power.  A conservative level is 20W average for a
  100W PEP rated PA.

\*---------------------------------------------------------------------------*/

/* real-valued short sample output, useful for going straight to DAC */

/* TX routines for 2400 FSK modes, after codec2 encoding */
static void freedv_tx_fsk_voice(struct freedv *f, short mod_out[]) {
    int  i;
    float *tx_float; /* To hold on to modulated samps from fsk/fmfsk */
    uint8_t vc_bits[2]; /* Varicode bits for 2400 framing */
    uint8_t proto_bits[3]; /* Prococol bits for 2400 framing */

    /* Frame for 2400A/B */
    if(f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_2400B){
        /* Get varicode bits for TX and possibly ask for a new char */
        /* 2 bits per 2400A/B frame, so this has to be done twice */
        for(i=0;i<2;i++){
            if (f->nvaricode_bits) {
                vc_bits[i] = f->tx_varicode_bits[f->varicode_bit_index++];
                f->nvaricode_bits--;
            }

            if (f->nvaricode_bits == 0) {
                /* get new char and encode */
                char s[2];
                if (f->freedv_get_next_tx_char != NULL) {
                    s[0] = (*f->freedv_get_next_tx_char)(f->callback_state);
                    f->nvaricode_bits = varicode_encode(f->tx_varicode_bits, s, VARICODE_MAX_BITS, 1, 1);
                    f->varicode_bit_index = 0;
                }
            }
        }

        /* If the API user hasn't set up message callbacks, don't bother with varicode bits */
        if(f->freedv_get_next_proto != NULL){
            (*f->freedv_get_next_proto)(f->proto_callback_state,(char*)proto_bits);
            fvhff_frame_bits(FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),proto_bits,vc_bits);
        }else if(f->freedv_get_next_tx_char != NULL){
            fvhff_frame_bits(FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),NULL,vc_bits);
        }else {
            fvhff_frame_bits(FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),NULL,NULL);
        }
    /* Frame for 800XA */
    }else if(f->mode == FREEDV_MODE_800XA){
        fvhff_frame_bits(FREEDV_HF_FRAME_B,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),NULL,NULL);
    }

    /* Allocate floating point buffer for FSK mod */
    tx_float = alloca(sizeof(float)*f->n_nom_modem_samples);

    /* do 4fsk mod */
    if(f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_800XA){
        fsk_mod(f->fsk,tx_float,(uint8_t*)(f->tx_bits));
        /* Convert float samps to short */
        for(i=0; i<f->n_nom_modem_samples; i++){
            mod_out[i] = (short)(tx_float[i]*FSK_SCALE*NORM_PWR_FSK);
        }
    /* do me-fsk mod */
    }else if(f->mode == FREEDV_MODE_2400B){
        fmfsk_mod(f->fmfsk,tx_float,(uint8_t*)(f->tx_bits));
        /* Convert float samps to short */
        for(i=0; i<f->n_nom_modem_samples; i++){
            mod_out[i] = (short)(tx_float[i]*FMFSK_SCALE);
        }
    }
}

/* TX routines for 2400 FSK modes, after codec2 encoding */
static void freedv_comptx_fsk_voice(struct freedv *f, COMP mod_out[]) {
    int  i;
    float *tx_float; /* To hold on to modulated samps from fsk/fmfsk */
    uint8_t vc_bits[2]; /* Varicode bits for 2400 framing */
    uint8_t proto_bits[3]; /* Prococol bits for 2400 framing */

    /* Frame for 2400A/B */
    if(f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_2400B){
        /* Get varicode bits for TX and possibly ask for a new char */
        /* 2 bits per 2400A/B frame, so this has to be done twice */
        for(i=0;i<2;i++){
            if (f->nvaricode_bits) {
                vc_bits[i] = f->tx_varicode_bits[f->varicode_bit_index++];
                f->nvaricode_bits--;
            }

            if (f->nvaricode_bits == 0) {
                /* get new char and encode */
                char s[2];
                if (f->freedv_get_next_tx_char != NULL) {
                    s[0] = (*f->freedv_get_next_tx_char)(f->callback_state);
                    f->nvaricode_bits = varicode_encode(f->tx_varicode_bits, s, VARICODE_MAX_BITS, 1, 1);
                    f->varicode_bit_index = 0;
                }
            }
        }

        /* If the API user hasn't set up message callbacks, don't bother with varicode bits */
        if(f->freedv_get_next_proto != NULL){
            (*f->freedv_get_next_proto)(f->proto_callback_state,(char*)proto_bits);
            fvhff_frame_bits(FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),proto_bits,vc_bits);
        }else if(f->freedv_get_next_tx_char != NULL){
            fvhff_frame_bits(FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),NULL,vc_bits);
        }else {
            fvhff_frame_bits(FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),NULL,NULL);
        }
    /* Frame for 800XA */
    }else if(f->mode == FREEDV_MODE_800XA){
        fvhff_frame_bits(FREEDV_HF_FRAME_B,(uint8_t*)(f->tx_bits),(uint8_t*)(f->packed_codec_bits),NULL,NULL);
    }

    /* Allocate floating point buffer for FSK mod */
    tx_float = alloca(sizeof(float)*f->n_nom_modem_samples);

    /* do 4fsk mod */
    if(f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_800XA){
        fsk_mod_c(f->fsk,mod_out,(uint8_t*)(f->tx_bits));
        /* Convert float samps to short */
        for(i=0; i<f->n_nom_modem_samples; i++){
        	mod_out[i] = fcmult(NORM_PWR_FSK,mod_out[i]);
        }
    /* do me-fsk mod */
    }else if(f->mode == FREEDV_MODE_2400B){
        fmfsk_mod(f->fmfsk,tx_float,(uint8_t*)(f->tx_bits));
        /* Convert float samps to short */
        for(i=0; i<f->n_nom_modem_samples; i++){
            mod_out[i].real = (tx_float[i]);
        }
    }
}


/* TX routines for 2400 FSK modes, data channel */
static void freedv_tx_fsk_data(struct freedv *f, short mod_out[]) {
    int  i;
    float *tx_float; /* To hold on to modulated samps from fsk/fmfsk */
    
    if (f->mode != FREEDV_MODE_800XA)
    	fvhff_frame_data_bits(f->deframer, FREEDV_VHF_FRAME_A,(uint8_t*)(f->tx_bits));
    else
        fvhff_frame_data_bits(f->deframer, FREEDV_HF_FRAME_B,(uint8_t*)(f->tx_bits));
        
    /* Allocate floating point buffer for FSK mod */
    tx_float = alloca(sizeof(float)*f->n_nom_modem_samples);
        
    /* do 4fsk mod */
    if(f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_800XA){
        fsk_mod(f->fsk,tx_float,(uint8_t*)(f->tx_bits));
        /* Convert float samps to short */
        for(i=0; i<f->n_nom_modem_samples; i++){
            mod_out[i] = (short)(tx_float[i]*FSK_SCALE);
        }
    /* do me-fsk mod */
    }else if(f->mode == FREEDV_MODE_2400B){
        fmfsk_mod(f->fmfsk,tx_float,(uint8_t*)(f->tx_bits));
        /* Convert float samps to short */
        for(i=0; i<f->n_nom_modem_samples; i++){
            mod_out[i] = (short)(tx_float[i]*FMFSK_SCALE);
        }
    }
}

void freedv_tx(struct freedv *f, short mod_out[], short speech_in[]) {
    assert(f != NULL);
    COMP tx_fdm[f->n_nom_modem_samples];
    int  i;
    assert((f->mode == FREEDV_MODE_1600)  || (f->mode == FREEDV_MODE_700)   || 
           (f->mode == FREEDV_MODE_700B)  || (f->mode == FREEDV_MODE_700C)  ||
           (f->mode == FREEDV_MODE_700D)  || 
           (f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || 
           (f->mode == FREEDV_MODE_800XA));
    
    /* FSK and MEFSK/FMFSK modems work only on real samples. It's simpler to just 
     * stick them in the real sample tx/rx functions than to add a comp->real converter
     * to comptx */
     
    if ((f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || (f->mode == FREEDV_MODE_800XA)){
        /* 800XA has two codec frames per modem frame */
        if(f->mode == FREEDV_MODE_800XA){
            codec2_encode(f->codec2, &f->packed_codec_bits[0], &speech_in[  0]);
            codec2_encode(f->codec2, &f->packed_codec_bits[4], &speech_in[320]);
        }else{
            codec2_encode(f->codec2, f->packed_codec_bits, speech_in);
        }
        freedv_tx_fsk_voice(f, mod_out);
    } else{ /* 1600 mode is here */
        freedv_comptx(f, tx_fdm, speech_in);
        for(i=0; i<f->n_nom_modem_samples; i++)
            mod_out[i] = tx_fdm[i].real;
    }
}
/* complex valued output, useful for suitable for single sided freq shifting */

static void freedv_comptx_fdmdv_1600(struct freedv *f, COMP mod_out[]) {
    int    bit, byte, i, j;
    int    bits_per_codec_frame, bits_per_modem_frame;
    int    data, codeword1, data_flag_index;
    COMP   tx_fdm[f->n_nat_modem_samples];

    bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
    bits_per_modem_frame = fdmdv_bits_per_frame(f->fdmdv);

    /* unpack bits, MSB first */

    bit = 7; byte = 0;
    for(i=0; i<bits_per_codec_frame; i++) {
        f->codec_bits[i] = (f->packed_codec_bits[byte] >> bit) & 0x1;
        bit--;
        if (bit < 0) {
            bit = 7;
            byte++;
        }
    }

    // spare bit in frame that codec defines.  Use this 1
    // bit/frame to send txt messages

    data_flag_index = codec2_get_spare_bit_index(f->codec2);

    if (f->nvaricode_bits) {
        f->codec_bits[data_flag_index] = f->tx_varicode_bits[f->varicode_bit_index++];
        f->nvaricode_bits--;
    }

    if (f->nvaricode_bits == 0) {
        /* get new char and encode */
        char s[2];
        if (f->freedv_get_next_tx_char != NULL) {
            s[0] = (*f->freedv_get_next_tx_char)(f->callback_state);
            f->nvaricode_bits = varicode_encode(f->tx_varicode_bits, s, VARICODE_MAX_BITS, 1, 1);
            f->varicode_bit_index = 0;
        }
    }

    /* Protect first 12 out of first 16 excitation bits with (23,12) Golay Code:

       0,1,2,3: v[0]..v[1]
       4,5,6,7: MSB of pitch
       11,12,13,14: MSB of energy

    */

    data = 0;
    for(i=0; i<8; i++) {
        data <<= 1;
        data |= f->codec_bits[i];
    }
    for(i=11; i<15; i++) {
        data <<= 1;
        data |= f->codec_bits[i];
    }
    codeword1 = golay23_encode(data);

    /* now pack output frame with parity bits at end to make them
       as far apart as possible from the data they protect.  Parity
       bits are LSB of the Golay codeword */

    for(i=0; i<bits_per_codec_frame; i++)
        f->tx_bits[i] = f->codec_bits[i];
    for(j=0; i<bits_per_codec_frame+11; i++,j++) {
        f->tx_bits[i] = (codeword1 >> (10-j)) & 0x1;
    }
    f->tx_bits[i] = 0; /* spare bit */

    /* optionally overwrite with test frames */

    if (f->test_frames) {
        fdmdv_get_test_bits(f->fdmdv, f->tx_bits);
        fdmdv_get_test_bits(f->fdmdv, &f->tx_bits[bits_per_modem_frame]);
        //fprintf(stderr, "test frames on tx\n");
    }

    /* modulate even and odd frames */

    fdmdv_mod(f->fdmdv, tx_fdm, f->tx_bits, &f->tx_sync_bit);
    assert(f->tx_sync_bit == 1);

    fdmdv_mod(f->fdmdv, &tx_fdm[FDMDV_NOM_SAMPLES_PER_FRAME], &f->tx_bits[bits_per_modem_frame], &f->tx_sync_bit);
    assert(f->tx_sync_bit == 0);

    assert(2*FDMDV_NOM_SAMPLES_PER_FRAME == f->n_nom_modem_samples);

    for(i=0; i<f->n_nom_modem_samples; i++)
        mod_out[i] = fcmult(FDMDV_SCALE, tx_fdm[i]);
}

#ifndef CORTEX_M4
static void freedv_comptx_700(struct freedv *f, COMP mod_out[]) {
    int    bit, byte, i, j, k;
    int    bits_per_codec_frame, bits_per_modem_frame;
    int    data_flag_index, nspare;
    COMP   tx_fdm[f->n_nat_modem_samples];

    bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
    bits_per_modem_frame = COHPSK_BITS_PER_FRAME;

    byte = 0;
    for (j=0; j<bits_per_modem_frame; j+=bits_per_codec_frame) {

        /* unpack bits, MSB first */

        bit = 7;
        for(i=0; i<bits_per_codec_frame; i++) {
            f->codec_bits[j+i] = (f->packed_codec_bits[byte] >> bit) & 0x1;
            bit--;
            if (bit < 0) {
                bit = 7;
                byte++;
            }
        }
	if (bit != 7)
	    byte++;

        // spare bits in frame that codec defines.  Use these spare
        // bits/frame to send txt messages

        switch(f->mode) {
        case FREEDV_MODE_700:
            nspare = 2;
            break;
        case FREEDV_MODE_700B:
            nspare = 1; // Just one spare bit for FREEDV_MODE_700B
            break;
        case FREEDV_MODE_700C:
            nspare = 0; // and no spare bits for 700C atm
            break;
        default:
            assert(0);
        }

        data_flag_index = codec2_get_spare_bit_index(f->codec2);

        for(k=0; k<nspare; k++) {
            if (f->nvaricode_bits) {
                f->codec_bits[j+data_flag_index+k] = f->tx_varicode_bits[f->varicode_bit_index++];
                //fprintf(stderr, "%d %d\n", j+data_flag_index+k, f->codec_bits[j+data_flag_index+k]);
                f->nvaricode_bits--;
            }
            if (f->nvaricode_bits == 0) {
                /* get new char and encode */
                char s[2];
                if (f->freedv_get_next_tx_char != NULL) {
                    s[0] = (*f->freedv_get_next_tx_char)(f->callback_state);
                    f->nvaricode_bits = varicode_encode(f->tx_varicode_bits, s, VARICODE_MAX_BITS, 1, 1);
                    f->varicode_bit_index = 0;
                }
            }
        }
    }
    /* optionally ovwerwrite the codec bits with test frames */

    if (f->test_frames) {
        cohpsk_get_test_bits(f->cohpsk, f->codec_bits);
    }

    /* cohpsk modulator */

    cohpsk_mod(f->cohpsk, tx_fdm, f->codec_bits, COHPSK_BITS_PER_FRAME);
    if (f->clip) 
        cohpsk_clip(tx_fdm, COHPSK_CLIP, COHPSK_NOM_SAMPLES_PER_FRAME);
    for(i=0; i<f->n_nat_modem_samples; i++)
        mod_out[i] = fcmult(FDMDV_SCALE*NORM_PWR_COHPSK, tx_fdm[i]);
    i = quisk_cfInterpDecim(mod_out, f->n_nat_modem_samples, f->ptFilter7500to8000, 16, 15);
    //assert(i == f->n_nom_modem_samples);
    // Caution: assert fails if f->n_nat_modem_samples * 16.0 / 15.0 is not an integer

}

/*
  Ok so when interleaved, we take the interleaver length of input samples,
  and output that many modem samples, e.g. for interleaver of length 4:

  record input speech 1234
  freedv tx              |
  play modem sig         1234
  record modem sig       1234
  freedv_rx                   |
  play output speech          1234        
  time axis --------->123456789012---->

  So a sample of input speech at time 1 is ouput at time 9.  We assume
  the freedv_tx and freedv_rx and propogation time over channel (from
  when a modem signal is played at the HF tx to when it is recorded at
  the HF rx) is zero.

  The freedv tx interface ouputs n_nom_modem_samples, which a single
  OFDM modem frame, 112 payload bits or 4 speech codec frames.  So
  this function must always have 1280 speech samples as input, and
  1280 modem samples as output, regradless of interleaver_frames.  For
  interleaver_frames > 1, we need to buffer samples.
*/

static void freedv_comptx_700d(struct freedv *f, COMP mod_out[]) {
    int    bit, byte, i, j, k;
    int    nspare;
 
    int data_bits_per_frame = f->ldpc->data_bits_per_frame;
    int bits_per_interleaved_frame = f->interleave_frames*data_bits_per_frame;
    uint8_t tx_bits[bits_per_interleaved_frame];
    int bits_per_codec_frame = codec2_bits_per_frame(f->codec2);

    byte = 0;
    for (j=0; j<bits_per_interleaved_frame; j+=bits_per_codec_frame) {

        /* unpack bits, MSB first */

        bit = 7;
        for(i=0; i<bits_per_codec_frame; i++) {
            tx_bits[j+i] = (f->packed_codec_bits_tx[byte] >> bit) & 0x1;
            bit--;
            if (bit < 0) {
                bit = 7;
                byte++;
            }
        }
	if (bit != 7)
	    byte++;
    }

    assert(byte <= f->nbyte_packed_codec_bits);
    
    // Generate Varicode txt bits. Txt bits in OFDM frame come just
    // after Unique Word (UW).  Txt bits aren't protected by FEC, and need to be
    // added to each frame after interleaver as done it's thing

    nspare = OFDM_NTXTBITS*f->interleave_frames;
    uint8_t txt_bits[nspare];
    
    for(k=0; k<nspare; k++) {
        if (f->nvaricode_bits) {
            txt_bits[k] = f->tx_varicode_bits[f->varicode_bit_index++];
            f->nvaricode_bits--;
        }
        if (f->nvaricode_bits == 0) {
            /* get new char and encode */
            char s[2];
            if (f->freedv_get_next_tx_char != NULL) {
                s[0] = (*f->freedv_get_next_tx_char)(f->callback_state);
                f->nvaricode_bits = varicode_encode(f->tx_varicode_bits, s, VARICODE_MAX_BITS, 1, 1);
                f->varicode_bit_index = 0;
            }
        }
    }

    /* optionally replace codec payload bits with test frames known to rx */

    if (f->test_frames) {
        for (j=0; j<f->interleave_frames; j++) {
            for(i=0; i<data_bits_per_frame; i++) {
                tx_bits[j*data_bits_per_frame + i] = payload_data_bits[i];
            }
        }
    }

    /* OK now ready to LDPC encode, interleave, and OFDM modulate */
    
    complex float tx_sams[f->interleave_frames*f->n_nat_modem_samples];
    COMP asam;
    
    ofdm_ldpc_interleave_tx(f->ofdm, f->ldpc, tx_sams, tx_bits, txt_bits, f->interleave_frames);

    for(i=0; i<f->interleave_frames*f->n_nat_modem_samples; i++) {
        asam.real = crealf(tx_sams[i]);
        asam.imag = cimagf(tx_sams[i]);
        mod_out[i] = fcmult(OFDM_AMP_SCALE*NORM_PWR_OFDM, asam);
    }

    if (f->clip) {
        //fprintf(stderr, "clip ");
        cohpsk_clip(mod_out, OFDM_CLIP, f->interleave_frames*f->n_nat_modem_samples);
    }
}

#endif


void freedv_comptx(struct freedv *f, COMP mod_out[], short speech_in[]) {
    assert(f != NULL);

    assert((f->mode == FREEDV_MODE_1600) || (f->mode == FREEDV_MODE_700) || 
           (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C) || 
           (f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) ||
           (f->mode == FREEDV_MODE_700D));

    if (f->mode == FREEDV_MODE_1600) {
        codec2_encode(f->codec2, f->packed_codec_bits, speech_in);
				
        freedv_comptx_fdmdv_1600(f, mod_out);
				
    }

#ifndef CORTEX_M4

    int bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
    int bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
    int i,j;
    
    /* all these modes need to pack a bunch of codec frames into one modem frame */
    
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C)) {
	int codec_frames = f->n_codec_bits / bits_per_codec_frame;

        for (j=0; j<codec_frames; j++) {
            codec2_encode(f->codec2, f->packed_codec_bits + j * bytes_per_codec_frame, speech_in);
            speech_in += codec2_samples_per_frame(f->codec2);
        }
        freedv_comptx_700(f, mod_out);
    }

    /* special treatment due to interleaver */
    
    if (f->mode == FREEDV_MODE_700D) {
        int data_bits_per_frame = f->ldpc->data_bits_per_frame;
	int codec_frames = data_bits_per_frame / bits_per_codec_frame;

        //fprintf(stderr, "modem_frame_count_tx: %d dec_frames: %d bytes offset: %d\n",
        //        f->modem_frame_count_tx, codec_frames, (f->modem_frame_count_tx*codec_frames)*bytes_per_codec_frame);
       
        /* buffer up bits until we get enough encoded bits for interleaver */
        
        for (j=0; j<codec_frames; j++) {
            codec2_encode(f->codec2, f->packed_codec_bits_tx + (f->modem_frame_count_tx*codec_frames+j)*bytes_per_codec_frame, speech_in);
            speech_in += codec2_samples_per_frame(f->codec2);
        }

        /* call modulate function when we have enough frames to run interleaver */

        assert((f->modem_frame_count_tx >= 0) && (f->modem_frame_count_tx < f->interleave_frames));
        f->modem_frame_count_tx++;
        if (f->modem_frame_count_tx == f->interleave_frames) {
            freedv_comptx_700d(f, f->mod_out);
            //fprintf(stderr, "  calling freedv_comptx_700d()\n");
            f->modem_frame_count_tx = 0;
        }

        /* output n_nom_modem_samples at a time from modulated buffer */
        for(i=0; i<f->n_nat_modem_samples; i++) {
            mod_out[i] = f->mod_out[f->modem_frame_count_tx*f->n_nat_modem_samples+i];
        }
    }
    
#endif
    /* 2400 A and B are handled by the real-mode TX */
    if((f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B)){
    	codec2_encode(f->codec2, f->packed_codec_bits, speech_in);
        freedv_comptx_fsk_voice(f,mod_out);
    }
}

void freedv_codectx(struct freedv *f, short mod_out[], unsigned char *packed_codec_bits) {
    assert(f != NULL);
    COMP tx_fdm[f->n_nom_modem_samples];
    int bits_per_codec_frame;
    int bytes_per_codec_frame;
    int codec_frames;
    int  i;
    bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
    bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
    codec_frames = f->n_codec_bits / bits_per_codec_frame;

    memcpy(f->packed_codec_bits, packed_codec_bits, bytes_per_codec_frame * codec_frames);
    
    switch(f->mode) {
        case FREEDV_MODE_1600:
            freedv_comptx_fdmdv_1600(f, tx_fdm);
            break;
    #ifndef CORTEX_M4
        case FREEDV_MODE_700:
        case FREEDV_MODE_700B:
        case FREEDV_MODE_700C:
            freedv_comptx_700(f, tx_fdm);
            break;
        case FREEDV_MODE_2400A:
        case FREEDV_MODE_2400B:
        case FREEDV_MODE_800XA:
            freedv_tx_fsk_voice(f, mod_out);
            return; /* output is already real */
    #endif
    }
    /* convert complex to real */
    for(i=0; i<f->n_nom_modem_samples; i++)
        mod_out[i] = tx_fdm[i].real;
}

void freedv_datatx  (struct freedv *f, short mod_out[]){
    assert(f != NULL);
    if (f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_2400B || f->mode == FREEDV_MODE_800XA) {
            freedv_tx_fsk_data(f, mod_out);
    }
}

int  freedv_data_ntxframes (struct freedv *f){
    assert(f != NULL);
    if (f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_2400B) {
        if (f->deframer->fdc)
            return freedv_data_get_n_tx_frames(f->deframer->fdc, 8);
    } else if (f->mode == FREEDV_MODE_800XA) {
        if (f->deframer->fdc)
            return freedv_data_get_n_tx_frames(f->deframer->fdc, 6);
    }
    return 0;
}

int freedv_nin(struct freedv *f) {
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C))
        // For mode 700, the input rate is 8000 sps, but the modem rate is 7500 sps
        // For mode 700, we request a larger number of Rx samples that will be decimated to f->nin samples
        return (16 * f->nin + f->ptFilter8000to7500->decim_index) / 15;
    else
        return f->nin;
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_rx
  AUTHOR......: David Rowe
  DATE CREATED: 3 August 2014

  Takes a frame of samples from the radio receiver, demodulates and
  decodes them, producing a frame of decoded speech samples.  See
  freedv_rx.c for an example.

  demod_in[] is a block of received samples sampled at 8000 Hz.
  To account for difference in the transmit and receive sample clock
  frequencies, the number of demod_in[] samples is time varying. You
  MUST call freedv_nin() BEFORE each call to freedv_rx() and pass
  exactly that many samples to this function.

  To help set your buffer sizes, The maximum value of freedv_nin() is
  freedv_get_n_max_modem_samples().

  freedv_rx() returns the number of output speech samples available in
  speech_out[], which is sampled at 8000 Hz.  You should ALWAYS check
  the return value of freedv_rx(), and read EXACTLY that number of
  speech samples from speech_out[].

  1600 and 700D mode: When out of sync, the number of output speech
  samples returned will be freedv_nin(). When in sync to a valid
  FreeDV 1600 signal, the number of output speech samples will
  alternate between freedv_get_n_speech_samples() and 0.

  700 .. 700C modes: The number of output speech samples returned will
  always be freedv_get_n_speech_samples(), regardless of sync.

  The peak level of demod_in[] is not critical, as the demod works
  well over a wide range of amplitude scaling.  However avoid clipping
  (overload, or samples pinned to +/- 32767).  speech_out[] will peak
  at just less than +/-32767.

  When out of sync, this function echoes the demod_in[] samples to
  speech_out[].  This allows the user to listen to the channel, which
  is useful for tuning FreeDV signals or reception of non-FreeDV
  signals.  Setting the squelch with freedv_set_squelch_en(1) will
  return zero-valued samples instead.

\*---------------------------------------------------------------------------*/


// short version

int freedv_rx(struct freedv *f, short speech_out[], short demod_in[]) {
    assert(f != NULL);
    int i;
    int nin = freedv_nin(f);

    assert(nin <= f->n_max_modem_samples);
       
    /* FSK RX happens in real floats, so convert to those and call their demod here */
    if( (f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || (f->mode == FREEDV_MODE_800XA) ){
        float rx_float[f->n_max_modem_samples];
        for(i=0; i<nin; i++) {
            rx_float[i] = ((float)demod_in[i]);
        }
        return freedv_floatrx(f,speech_out,rx_float);
    }
    
    if ( (f->mode == FREEDV_MODE_1600) || (f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) ||
        (f->mode == FREEDV_MODE_700C) || (f->mode == FREEDV_MODE_700D)) {

        float gain = 1.0;
        if (f->mode == FREEDV_MODE_700D) {
            gain = 2.0; /* keep levels the same as Octave simulations and C unit tests for real signals */
        }
        
        /* FDM RX happens with complex samps, so do that */
        COMP rx_fdm[f->n_max_modem_samples];
        for(i=0; i<nin; i++) {
            rx_fdm[i].real = gain*(float)demod_in[i];
            rx_fdm[i].imag = 0.0;
        }
        return freedv_comprx(f, speech_out, rx_fdm);
    }
    
    return 0; /* should never get here */
}


// float input samples version
int freedv_comprx_fsk(struct freedv *f, COMP demod_in[], int *valid) {
    /* Varicode and protocol bits */
    uint8_t vc_bits[2];
    uint8_t proto_bits[3];
    short vc_bit;
    int i;
    int n_ascii;
    char ascii_out;

    if(f->mode == FREEDV_MODE_2400A || f->mode == FREEDV_MODE_800XA){        
	fsk_demod(f->fsk,(uint8_t*)f->tx_bits,demod_in);
        f->nin = fsk_nin(f->fsk);
        float EbNodB = f->fsk->stats->snr_est;           /* fsk demod actually estimates Eb/No     */
        f->snr_est = EbNodB + 10.0*log10f(800.0/3000.0); /* so convert to SNR Rb=800, noise B=3000 */
        //fprintf(stderr," %f %f\n", EbNodB, f->snr_est);
    } else{      
        /* 2400B needs real input samples */
        int n = fmfsk_nin(f->fmfsk);
        float demod_in_float[n];
        for(i=0; i<n; i++) {
            demod_in_float[i] = demod_in[i].real;
        }
        fmfsk_demod(f->fmfsk,(uint8_t*)f->tx_bits,demod_in_float);
        f->nin = fmfsk_nin(f->fmfsk);
    }
    
    if(fvhff_deframe_bits(f->deframer,f->packed_codec_bits,proto_bits,vc_bits,(uint8_t*)f->tx_bits)){
        /* Decode varicode text */
        for(i=0; i<2; i++){
            /* Note: deframe_bits spits out bits in uint8_ts while varicode_decode expects shorts */
            vc_bit = vc_bits[i];
            n_ascii = varicode_decode(&f->varicode_dec_states, &ascii_out, &vc_bit, 1, 1);
            if (n_ascii && (f->freedv_put_next_rx_char != NULL)) {
                (*f->freedv_put_next_rx_char)(f->callback_state, ascii_out);
            }
        }
        /* Pass proto bits on down if callback is present */
        if( f->freedv_put_next_proto != NULL){
            (*f->freedv_put_next_proto)(f->proto_callback_state,(char*)proto_bits);
        }
        *valid = 1;

        /* squelch if if sync but SNR too low */
        if (f->squelch_en && (f->snr_est < f->snr_squelch_thresh)) {
            *valid = 0;
        }
    } else {
        /* squelch if out of sync, or echo input of squelch off */
        if (f->squelch_en) 
            *valid = 0;
        else
            *valid = -1;
    }
    f->sync = f->deframer->state;
    f->stats.sync = f->deframer->state;

    return f->n_speech_samples;
}

int freedv_floatrx(struct freedv *f, short speech_out[], float demod_in[]) {
    assert(f != NULL);
    int  i;
    int nin = freedv_nin(f);    
    
    assert(nin <= f->n_max_modem_samples);
    
    COMP rx_fdm[f->n_max_modem_samples];
    for(i=0; i<nin; i++) {
        rx_fdm[i].real = demod_in[i];
        rx_fdm[i].imag = 0;
    }

    return freedv_comprx(f, speech_out, rx_fdm);
}

// complex input samples version


static int freedv_comprx_fdmdv_1600(struct freedv *f, COMP demod_in[], int *valid) {
    int                 bits_per_codec_frame, bytes_per_codec_frame, bits_per_fdmdv_frame;
    int                 i, j, bit, byte, nin_prev, nout;
    int                 recd_codeword, codeword1, data_flag_index, n_ascii;
    short               abit[1];
    char                ascii_out;
    int                 reliable_sync_bit;

    bits_per_codec_frame  = codec2_bits_per_frame(f->codec2);
    bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
    nout = f->n_speech_samples;

    COMP ademod_in[f->nin];
    for(i=0; i<f->nin; i++)
        ademod_in[i] = fcmult(1.0/FDMDV_SCALE, demod_in[i]);

    bits_per_fdmdv_frame  = fdmdv_bits_per_frame(f->fdmdv);

    nin_prev = f->nin;
    fdmdv_demod(f->fdmdv, f->fdmdv_bits, &reliable_sync_bit, ademod_in, &f->nin);
    fdmdv_get_demod_stats(f->fdmdv, &f->stats);
    f->sync = f->fdmdv->sync;
    f->snr_est = f->stats.snr_est;

	if (f->evenframe == 0) {
		memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
		nout = 0;
	}
	
    if (reliable_sync_bit == 1) {
        f->evenframe = 1;
    }

    if (f->stats.sync) {
        if (f->evenframe == 0) {
            memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
            nout = 0;
	    *valid = 0;
        }
        else {
            memcpy(&f->rx_bits[bits_per_fdmdv_frame], f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));

            if (f->test_frames == 0) {
                recd_codeword = 0;
                for(i=0; i<8; i++) {
                    recd_codeword <<= 1;
                    recd_codeword |= (f->rx_bits[i] & 0x1);
                }
                for(i=11; i<15; i++) {
                    recd_codeword <<= 1;
                    recd_codeword |= (f->rx_bits[i] & 0x1);
                }
                for(i=bits_per_codec_frame; i<bits_per_codec_frame+11; i++) {
                    recd_codeword <<= 1;
                    recd_codeword |= (f->rx_bits[i] & 0x1);
                }
                codeword1 = golay23_decode(recd_codeword);
                f->total_bit_errors += golay23_count_errors(recd_codeword, codeword1);
                f->total_bits       += 23;

                //codeword1 = recd_codeword;
                //fprintf(stderr, "received codeword1: 0x%x  decoded codeword1: 0x%x\n", recd_codeword, codeword1);

                for(i=0; i<bits_per_codec_frame; i++)
                    f->codec_bits[i] = f->rx_bits[i];

                for(i=0; i<8; i++) {
                    f->codec_bits[i] = (codeword1 >> (22-i)) & 0x1;
                }
                for(i=8,j=11; i<12; i++,j++) {
                    f->codec_bits[j] = (codeword1 >> (22-i)) & 0x1;
                }

                // extract txt msg data bit ------------------------------------------------------------

                data_flag_index = codec2_get_spare_bit_index(f->codec2);
                abit[0] = f->codec_bits[data_flag_index];

                n_ascii = varicode_decode(&f->varicode_dec_states, &ascii_out, abit, 1, 1);
                if (n_ascii && (f->freedv_put_next_rx_char != NULL)) {
                    (*f->freedv_put_next_rx_char)(f->callback_state, ascii_out);
                }

                // reconstruct missing bit we steal for data bit and decode speech

                codec2_rebuild_spare_bit(f->codec2, f->codec_bits);

                // pack bits, MSB received first

                bit  = 7;
                byte = 0;
                memset(f->packed_codec_bits, 0,  bytes_per_codec_frame);
                for(i=0; i<bits_per_codec_frame; i++) {
                    f->packed_codec_bits[byte] |= (f->codec_bits[i] << bit);
                    bit--;
                    if(bit < 0) {
                        bit = 7;
                        byte++;
                    }
                }
                *valid = 1;
            }
            else {
                int   test_frame_sync, bit_errors, ntest_bits, k;
                short error_pattern[fdmdv_error_pattern_size(f->fdmdv)];

                for(k=0; k<2; k++) {
                    /* test frames, so lets sync up to the test frames and count any errors */

                    fdmdv_put_test_bits(f->fdmdv, &test_frame_sync, error_pattern, &bit_errors, &ntest_bits, &f->rx_bits[k*bits_per_fdmdv_frame]);

                    if (test_frame_sync == 1) {
                        f->test_frame_sync_state = 1;
                        f->test_frame_count = 0;
                    }

                    if (f->test_frame_sync_state) {
                        if (f->test_frame_count == 0) {
                            f->total_bit_errors += bit_errors;
                            f->total_bits += ntest_bits;
                            if (f->freedv_put_error_pattern != NULL) {
                                (*f->freedv_put_error_pattern)(f->error_pattern_callback_state, error_pattern, fdmdv_error_pattern_size(f->fdmdv));
                            }
                        }
                        f->test_frame_count++;
                        if (f->test_frame_count == 4)
                            f->test_frame_count = 0;
                    }

                    //fprintf(stderr, "test_frame_sync: %d test_frame_sync_state: %d bit_errors: %d ntest_bits: %d\n",
                    //        test_frame_sync, f->test_frame_sync_state, bit_errors, ntest_bits);
                }
            }


            /* squelch if beneath SNR threshold or test frames enabled */

            if ((f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) || f->test_frames) {
                //fprintf(stderr,"squelch %f %f !\n", f->stats.snr_est, f->snr_squelch_thresh);
                *valid = 0;
            }

            nout = f->n_speech_samples;

        }

        /* note this freewheels if reliable sync dissapears on bad channels */

        if (f->evenframe)
            f->evenframe = 0;
        else
            f->evenframe = 1;
        //fprintf(stderr,"%d\n",  f->evenframe);

    } /* if (sync) .... */
    else {
        /* if not in sync pass through analog samples */
        /* this lets us "hear" whats going on, e.g. during tuning */

        //fprintf(stderr, "out of sync\n");

        if (f->squelch_en == 0) {
	    *valid = -1;
        }
        else {
	    *valid = 0;
        }
        //fprintf(stderr, "%d %d %d\n", nin_prev, speech_out[0], speech_out[nin_prev-1]);
        nout = nin_prev;
    }
    return nout;
}

#ifndef CORTEX_M4
static int freedv_comprx_700(struct freedv *f, COMP demod_in_8kHz[], int *valid) {
    int                 bits_per_codec_frame, bytes_per_codec_frame;
    int                 i, j, bit, byte, nout, k;
    int                 data_flag_index, n_ascii, nspare;
    short               abit[1];
    char                ascii_out;
    float rx_bits[COHPSK_BITS_PER_FRAME]; /* soft decn rx bits */
    int   sync;
    int   frames;

    bits_per_codec_frame  = codec2_bits_per_frame(f->codec2);
    bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
    frames = f->n_codec_bits / bits_per_codec_frame;
    nout = f->n_speech_samples;

    // echo samples back out as default (say if sync not found)
    *valid = -1;

    // quisk_cfInterpDecim() modifies input data so lets make a copy just in case there
    // is no sync and we need to echo inout to output

    COMP demod_in[freedv_nin(f)];
    for(i=0; i<freedv_nin(f); i++)
        demod_in[i] = demod_in_8kHz[i];

    i = quisk_cfInterpDecim(demod_in, freedv_nin(f), f->ptFilter8000to7500, 15, 16);
    //if (i != f->nin)
    //    printf("freedv_comprx decimation: input %d output %d\n", freedv_nin(f), i);

    for(i=0; i<f->nin; i++)
        demod_in[i] = fcmult(1.0/FDMDV_SCALE, demod_in[i]);
    
    cohpsk_demod(f->cohpsk, rx_bits, &sync, demod_in, &f->nin);

    f->sync = sync;
    cohpsk_get_demod_stats(f->cohpsk, &f->stats);
    f->snr_est = f->stats.snr_est;

    memset(f->packed_codec_bits, 0, bytes_per_codec_frame * frames);

    if (sync) {

        if (f->test_frames == 0) {
            data_flag_index = codec2_get_spare_bit_index(f->codec2);

            /* optional smoothing of codec symbols */

            if (f->smooth_symbols) {

                for(i=0; i<bits_per_codec_frame; i++) {
                    rx_bits[i] += rx_bits[i+bits_per_codec_frame];
                    rx_bits[i+bits_per_codec_frame] = rx_bits[i];
                }
            }

            byte = 0;
            for (j=0; j<COHPSK_BITS_PER_FRAME; j+=bits_per_codec_frame) {

                /* extract txt msg data bit(s) */

                switch(f->mode) {
                case FREEDV_MODE_700:
                    nspare = 2;
                    break;
                case FREEDV_MODE_700B:
                    nspare = 1; // Just one spare bit for FREEDV_MODE_700B
                    break;
                case FREEDV_MODE_700C:
                    nspare = 0; // and no spare bits for 700C atm
                    break;
                default:
                    assert(0);
                }

                for(k=0; k<nspare; k++)  {
                    abit[0] = rx_bits[data_flag_index+j+k] < 0.0;

                    n_ascii = varicode_decode(&f->varicode_dec_states, &ascii_out, abit, 1, 1);
                    if (n_ascii && (f->freedv_put_next_rx_char != NULL)) {
                        (*f->freedv_put_next_rx_char)(f->callback_state, ascii_out);
                    }
                }

                /* pack bits, MSB received first */

                bit = 7;
                for(i=0; i<bits_per_codec_frame; i++) {
                    f->packed_codec_bits[byte] |= ((rx_bits[j+i] < 0.0) << bit);
                    bit--;
                    if (bit < 0) {
                        bit = 7;
                        byte++;
                    }
                }
		if (bit != 7)
		    byte++;

                if (f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) {
		   *valid = 0;
                }
		*valid = 1;
            }
            nout = f->n_speech_samples;
        }
        else {
            //fprintf(stderr, " freedv_api:  f->test_frames_diversity: %d\n", f->test_frames_diversity);

            if (f->test_frames_diversity) {
                /* normal operation - error pattern on frame after diveristy combination */
                short error_pattern[COHPSK_BITS_PER_FRAME];
                int   bit_errors;

                /* test data, lets see if we can sync to the test data sequence */

                char rx_bits_char[COHPSK_BITS_PER_FRAME];
                for(i=0; i<COHPSK_BITS_PER_FRAME; i++)
                    rx_bits_char[i] = rx_bits[i] < 0.0;
                cohpsk_put_test_bits(f->cohpsk, &f->test_frame_sync_state, error_pattern, &bit_errors, rx_bits_char, 0);
                if (f->test_frame_sync_state) {
                    f->total_bit_errors += bit_errors;
                    f->total_bits       += COHPSK_BITS_PER_FRAME;
                    if (f->freedv_put_error_pattern != NULL) {
                        (*f->freedv_put_error_pattern)(f->error_pattern_callback_state, error_pattern, COHPSK_BITS_PER_FRAME);
                    }
                }
            } 
            else {
                /* calculate error pattern on uncombined carriers - test mode to spot any carrier specific issues like
                   tx passband filtering */

                short error_pattern[2*COHPSK_BITS_PER_FRAME];
                char  rx_bits_char[COHPSK_BITS_PER_FRAME];
                int   bit_errors_lower, bit_errors_upper;

                /* lower group of carriers */

                float *rx_bits_lower = cohpsk_get_rx_bits_lower(f->cohpsk);
                for(i=0; i<COHPSK_BITS_PER_FRAME; i++) {
                    rx_bits_char[i] = rx_bits_lower[i] < 0.0;
                }
                cohpsk_put_test_bits(f->cohpsk, &f->test_frame_sync_state, error_pattern, &bit_errors_lower, rx_bits_char, 0);

                /* upper group of carriers */

                float *rx_bits_upper = cohpsk_get_rx_bits_upper(f->cohpsk);
                for(i=0; i<COHPSK_BITS_PER_FRAME; i++) {
                    rx_bits_char[i] = rx_bits_upper[i] < 0.0;
                }
                cohpsk_put_test_bits(f->cohpsk, &f->test_frame_sync_state_upper, &error_pattern[COHPSK_BITS_PER_FRAME], &bit_errors_upper, rx_bits_char, 1);
                //                fprintf(stderr, " freedv_api:  f->test_frame_sync_state: %d f->test_frame_sync_state_upper: %d\n", 
                //        f->test_frame_sync_state, f->test_frame_sync_state_upper);

                /* combine total errors and call callback */

                if (f->test_frame_sync_state && f->test_frame_sync_state_upper) {
                    f->total_bit_errors += bit_errors_lower + bit_errors_upper;
                    f->total_bits       += 2*COHPSK_BITS_PER_FRAME;
                    if (f->freedv_put_error_pattern != NULL) {
                        (*f->freedv_put_error_pattern)(f->error_pattern_callback_state, error_pattern, 2*COHPSK_BITS_PER_FRAME);
                    }
                }

            }
            
	    *valid = 0;
            nout = f->n_speech_samples;
        }

    }

    /* no valid FreeDV signal - squelch output */

    if (sync == 0) {
        nout = freedv_nin(f);
        if (f->squelch_en) {
	    *valid = 0;
        }
    }
    return nout;
}
#endif

/*
  TODO: 
    [X] in testframe mode count coded and uncoded errors
    [X] freedv getter for modem and interleaver sync
    [X] rms level the same as fdmdv
    [X] way to stay in sync and not resync automatically 
    [X] SNR est, maybe from pilots, cohpsk have an example?
    [X] work out how to handle return of multiple interleaved frames over time
    [ ] error pattern support?
    [ ] deal with out of sync returning nin samples, listening to analog audio when out of sync
*/

static int freedv_comprx_700d(struct freedv *f, COMP demod_in_8kHz[], int *valid) {
    int   bits_per_codec_frame, bytes_per_codec_frame;
    int   i, j, bit, byte, nout, k;
    int   n_ascii;
    char  ascii_out;
    int   frames;
    struct OFDM *ofdm = f->ofdm;
    struct LDPC *ldpc = f->ldpc;
    
    int    data_bits_per_frame = ldpc->data_bits_per_frame;
    int    coded_bits_per_frame = ldpc->coded_bits_per_frame;
    int    coded_syms_per_frame = ldpc->coded_syms_per_frame;
    int    interleave_frames = f->interleave_frames;
    COMP  *codeword_symbols = f->codeword_symbols;
    float *codeword_amps = f->codeword_amps;
    int    Nbitsperframe = ofdm_get_bits_per_frame(ofdm);
    int    rx_bits[Nbitsperframe];
    short txt_bits[OFDM_NTXTBITS];
    COMP  payload_syms[coded_syms_per_frame];
    float payload_amps[coded_syms_per_frame];
   
    bits_per_codec_frame  = codec2_bits_per_frame(f->codec2);
    bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
    frames = f->n_codec_bits / bits_per_codec_frame;

    // pass through is too noisey ....
    //nout = f->n_speech_samples;
    nout = 0;
    
    int Nerrs_raw = 0;
    int Nerrs_coded = 0;
    int iter = 0;
    int parityCheckCount = 0;
    int rx_uw[OFDM_NUWBITS];
    COMP rxbuf_in[f->nin];

    for(i=0; i<f->nin; i++) {
        rxbuf_in[i].real = demod_in_8kHz[i].real/OFDM_AMP_SCALE;
        rxbuf_in[i].imag = demod_in_8kHz[i].imag/OFDM_AMP_SCALE;
    }
    
    /* echo samples back out as default (say if sync not found) */
    
    *valid = 1;
    f->sync = f->stats.sync = 0;
    
    /* TODO estimate this properly from signal */
    
    float EsNo = 3.0;
    
    /* looking for modem sync */
    
    if (strcmp(ofdm->sync_state,"search") == 0) {
        ofdm_sync_search(f->ofdm, rxbuf_in);
    }

     /* OK modem is in sync */
    
    if ((strcmp(ofdm->sync_state,"synced") == 0) || (strcmp(ofdm->sync_state,"trial") == 0) ) {
        ofdm_demod(ofdm, rx_bits, rxbuf_in);
        ofdm_disassemble_modem_frame(ofdm, rx_uw, payload_syms, payload_amps, txt_bits);

        f->sync = 1;
        ofdm_get_demod_stats(f->ofdm, &f->stats);
        f->snr_est = f->stats.snr_est;

        assert((OFDM_NUWBITS+OFDM_NTXTBITS+coded_bits_per_frame) == OFDM_BITSPERFRAME);

        /* now we need to buffer for de-interleaving -------------------------------------*/
                
        /* shift interleaved symbol buffers to make room for new symbols */
                
        for(i=0, j=coded_syms_per_frame; j<interleave_frames*coded_syms_per_frame; i++,j++) {
            codeword_symbols[i] = codeword_symbols[j];
            codeword_amps[i] = codeword_amps[j];
        }

        /* newest symbols at end of buffer (uses final i from last loop), note we 
           change COMP formats from what modem uses internally */
                
        for(i=(interleave_frames-1)*coded_syms_per_frame,j=0; i<interleave_frames*coded_syms_per_frame; i++,j++) {
            codeword_symbols[i] = payload_syms[j];
            codeword_amps[i]    = payload_amps[j];
         }
               
        /* run de-interleaver */
                
        COMP  codeword_symbols_de[interleave_frames*coded_syms_per_frame];
        float codeword_amps_de[interleave_frames*coded_syms_per_frame];
        gp_deinterleave_comp (codeword_symbols_de, codeword_symbols, interleave_frames*coded_syms_per_frame);
        gp_deinterleave_float(codeword_amps_de   , codeword_amps   , interleave_frames*coded_syms_per_frame);

        double llr[coded_bits_per_frame];
        char out_char[coded_bits_per_frame];

        interleaver_sync_state_machine(ofdm, ldpc, codeword_symbols_de, codeword_amps_de, EsNo,
                                       interleave_frames, &iter, &parityCheckCount, &Nerrs_coded);
                                         
        if (!strcmp(ofdm->sync_state_interleaver,"synced") && (ofdm->frame_count_interleaver == interleave_frames)) {
            ofdm->frame_count_interleaver = 0;

            if (f->test_frames) {
                int tmp[interleave_frames];
                Nerrs_raw = count_uncoded_errors(ldpc, tmp, interleave_frames, codeword_symbols_de);
                f->total_bit_errors += Nerrs_raw;
                f->total_bits       += Nbitsperframe*interleave_frames;
            }

            memset(f->packed_codec_bits, 0, bytes_per_codec_frame * frames);
            byte = 0; f->modem_frame_count_rx = 0;
            
            for (j=0; j<interleave_frames; j++) {
                symbols_to_llrs(llr, &codeword_symbols_de[j*coded_syms_per_frame],
                                &codeword_amps_de[j*coded_syms_per_frame],
                                EsNo, ofdm->mean_amp, coded_syms_per_frame);               
                iter = run_ldpc_decoder(ldpc, out_char, llr, &parityCheckCount);

                if (f->test_frames) {
                    Nerrs_coded = count_errors(payload_data_bits, out_char, data_bits_per_frame);
                    f->total_bit_errors_coded += Nerrs_coded;
                    f->total_bits_coded       += data_bits_per_frame;
                } else {

                    /* a frame of valid Codec 2 bits, pack into Codec 2 frame  */

                    for (i=0; i<data_bits_per_frame; i+=bits_per_codec_frame) {

                        /* pack bits, MSB received first */

                        bit = 7;
                        for(k=0; k<bits_per_codec_frame; k++) {
                            f->packed_codec_bits[byte] |= (out_char[i+k] << bit);
                            bit--;
                            if (bit < 0) {
                                bit = 7;
                                byte++;
                            }
                        }
                        if (bit != 7)
                            byte++;
                    }
                    
                }
            } /* for interleave frames ... */

            /* make sure we don't overrun packed byte array */

            assert(byte <= f->nbyte_packed_codec_bits);
                   
            nout = f->n_speech_samples;                  

            if (f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) {
                *valid = 0;
            }

        } /* if interleaver synced ..... */

        /* If modem is synced we can decode txt bits */
        
        for(k=0; k<OFDM_NTXTBITS; k++)  { 
            //fprintf(stderr, "txt_bits[%d] = %d\n", k, rx_bits[i]);
            n_ascii = varicode_decode(&f->varicode_dec_states, &ascii_out, &txt_bits[k], 1, 1);
            if (n_ascii && (f->freedv_put_next_rx_char != NULL)) {
                (*f->freedv_put_next_rx_char)(f->callback_state, ascii_out);
            }
        }

        /* estimate uncoded BER from UW.  Coded bit errors could
           probably be estimated as half of all failed LDPC parity
           checks */

        for(i=0; i<OFDM_NUWBITS; i++) {         
            if (rx_uw[i] != ofdm->tx_uw[i]) {
                f->total_bit_errors++;
            }
        }
        f->total_bits += OFDM_NUWBITS;          

    } /* if modem synced .... */ else {
        *valid = -1;
    }

    /* iterate state machine and update nin for next call */
    
    f->nin = ofdm_get_nin(ofdm);
    //fprintf(stderr, "nin: %d\n", ofdm_get_nin(ofdm));
    ofdm_sync_state_machine(ofdm, rx_uw);

    if (f->verbose  && strcmp(ofdm->last_sync_state, "search")) {
        fprintf(stderr, "%3d st: %-6s euw: %2d %1d f: %5.1f ist: %-6s %2d eraw: %3d ecdd: %3d iter: %3d pcc: %3d vld: %d, nout: %4d\n",
                f->frames++, ofdm->last_sync_state, ofdm->uw_errors, ofdm->sync_counter, ofdm->foff_est_hz,
                ofdm->last_sync_state_interleaver, ofdm->frame_count_interleaver,
                Nerrs_raw, Nerrs_coded, iter, parityCheckCount, *valid, nout);
    }
    
    /* no valid FreeDV signal - squelch output */
    
    int sync = !strcmp(ofdm->sync_state,"synced") || !strcmp(ofdm->sync_state,"trial");
    if (!sync) {
         if (f->squelch_en) {
 	    *valid = 0;
         }
         //f->snr_est = 0.0;
    }
    
    //fprintf(stderr, "sync: %d valid: %d snr: %3.2f\n", f->sync, *valid, f->snr_est);
    
    return nout;
}


int freedv_comprx(struct freedv *f, short speech_out[], COMP demod_in[]) {
    assert(f != NULL);
    int                 bits_per_codec_frame, bytes_per_codec_frame;
    int                 i, nout = 0;
    int valid;
    
    assert(f->nin <= f->n_max_modem_samples);

    bits_per_codec_frame  = codec2_bits_per_frame(f->codec2);
    bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;

    if (f->mode == FREEDV_MODE_1600) {
        nout = freedv_comprx_fdmdv_1600(f, demod_in, &valid);
    }
#ifndef CORTEX_M4
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C)) {
        nout = freedv_comprx_700(f, demod_in, &valid);
    }

    if (f->mode == FREEDV_MODE_700D) {
        nout = freedv_comprx_700d(f, demod_in, &valid);
    }
    
    if( (f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || (f->mode == FREEDV_MODE_800XA)){
        nout = freedv_comprx_fsk(f, demod_in, &valid);
    }
#endif

    if (valid == 0) {
        //fprintf(stderr, "squelch nout: %d\n", nout);
        
        /* squelch */
        
        for (i = 0; i < nout; i++)
            speech_out[i] = 0;
    }
    else if (valid < 0) {
        /* we havent got sync so play audio from radio.  This might
           not work for all modes due to nin bouncing about */
        for (i = 0; i < nout; i++)
            speech_out[i] = demod_in[i].real;
    }
    else {
        /* decoded audio to play */
        
        if (f->mode == FREEDV_MODE_700D) {
            int data_bits_per_frame = f->ldpc->data_bits_per_frame;
            int frames = data_bits_per_frame/bits_per_codec_frame;
            
            nout = 0;
            if (f->modem_frame_count_rx < f->interleave_frames) {
                nout = f->n_speech_samples;
                //fprintf(stderr, "modem_frame_count_rx: %d nout: %d\n", f->modem_frame_count_rx, nout);
                for (i = 0; i < frames; i++) {
                    codec2_decode(f->codec2, speech_out, f->packed_codec_bits + (i + frames*f->modem_frame_count_rx)* bytes_per_codec_frame);
                    speech_out += codec2_samples_per_frame(f->codec2);
                }
                f->modem_frame_count_rx++;
            }
           
        } else {
            int frames = f->n_codec_bits / bits_per_codec_frame;
            //fprintf(stderr, "frames: %d\n", frames);
            for (i = 0; i < frames; i++) {
                codec2_decode(f->codec2, speech_out, f->packed_codec_bits + i * bytes_per_codec_frame);
                speech_out += codec2_samples_per_frame(f->codec2);
            }
        }
    }

    //fprintf(stderr,"freedv_nin(f): %d nout: %d valid: %d\n", freedv_nin(f), nout, valid);
    return nout;
}

int freedv_codecrx(struct freedv *f, unsigned char *packed_codec_bits, short demod_in[])
{
    assert(f != NULL);
    COMP rx_fdm[f->n_max_modem_samples];
    int i;
    int nin = freedv_nin(f);
    int valid;
    int ret = 0;

    assert(nin <= f->n_max_modem_samples);
    
    for(i=0; i<nin; i++) {
        rx_fdm[i].real = (float)demod_in[i];
        rx_fdm[i].imag = 0.0;
    }

    if (f->mode == FREEDV_MODE_1600) {
        freedv_comprx_fdmdv_1600(f, rx_fdm, &valid);
    }

#ifndef CORTEX_M4
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C)) {
        freedv_comprx_700(f, rx_fdm, &valid);
    }
#endif
    
    if( (f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || (f->mode == FREEDV_MODE_800XA)){
        freedv_comprx_fsk(f, rx_fdm, &valid);
    }

    if (valid == 1) {
        int bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
        int bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
        int codec_frames = f->n_codec_bits / bits_per_codec_frame;

        memcpy(packed_codec_bits, f->packed_codec_bits, bytes_per_codec_frame * codec_frames);
	ret = bytes_per_codec_frame * codec_frames;
    }
    
    return ret;
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_get_version
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 28 July 2015

  Return the version of the FreeDV API.  This is meant to help API users determine when
  incompatible changes have occurred.

\*---------------------------------------------------------------------------*/

int freedv_get_version(void)
{
    return VERSION;
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_set_callback_txt
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 28 July 2015

  Set the callback functions and the callback state pointer that will be used
  for the aux txt channel.  The freedv_callback_rx is a function pointer that
  will be called to return received characters.  The freedv_callback_tx is a
  function pointer that will be called to send transmitted characters.  The callback
  state is a user-defined void pointer that will be passed to the callback functions.
  Any or all can be NULL, and the default is all NULL.
  The function signatures are:
    void receive_char(void *callback_state, char c);
    char transmit_char(void *callback_state);

\*---------------------------------------------------------------------------*/

void freedv_set_callback_txt(struct freedv *f, freedv_callback_rx rx, freedv_callback_tx tx, void *state)
{
    if (f->mode != FREEDV_MODE_800XA) {
        f->freedv_put_next_rx_char = rx;
        f->freedv_get_next_tx_char = tx;
        f->callback_state = state;
    }
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_set_callback_protocol
  AUTHOR......: Brady OBrien
  DATE CREATED: 21 February 2016

  Set the callback functions and callback pointer that will be used for the
  protocol data channel. freedv_callback_protorx will be called when a frame
  containing protocol data arrives. freedv_callback_prototx will be called
  when a frame containing protocol information is being generated. Protocol
  information is intended to be used to develop protocols and fancy features
  atop VHF freedv, much like those present in DMR.
   Protocol bits are to be passed in an msb-first char array
   The number of protocol bits are findable with freedv_get_protocol_bits
\*---------------------------------------------------------------------------*/

void freedv_set_callback_protocol(struct freedv *f, freedv_callback_protorx rx, freedv_callback_prototx tx, void *callback_state){
    if (f->mode != FREEDV_MODE_800XA) {
        f->freedv_put_next_proto = rx;
        f->freedv_get_next_proto = tx;
        f->proto_callback_state = callback_state;
    }
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_set_callback_datarx / freedv_set_callback_datatx
  AUTHOR......: Jeroen Vreeken
  DATE CREATED: 04 March 2016

  Set the callback functions and callback pointer that will be used for the
  data channel. freedv_callback_datarx will be called when a packet has been
  successfully received. freedv_callback_data_tx will be called when 
  transmission of a new packet can begin.
  If the returned size of the datatx callback is zero the data frame is still
  generated, but will contain only a header update.
\*---------------------------------------------------------------------------*/
void freedv_set_callback_data(struct freedv *f, freedv_callback_datarx datarx, freedv_callback_datatx datatx, void *callback_state) {
    if ((f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || (f->mode == FREEDV_MODE_800XA)){
        if (!f->deframer->fdc)
            f->deframer->fdc = freedv_data_channel_create();
        if (!f->deframer->fdc)
            return;
        
        freedv_data_set_cb_rx(f->deframer->fdc, datarx, callback_state);
        freedv_data_set_cb_tx(f->deframer->fdc, datatx, callback_state);
    }
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_set_data_header
  AUTHOR......: Jeroen Vreeken
  DATE CREATED: 04 March 2016

  Set the data header for the data channel.
  Header compression will be used whenever packets from this header are sent.
  The header will also be used for fill packets when a data frame is requested
  without a packet available.
\*---------------------------------------------------------------------------*/
void freedv_set_data_header(struct freedv *f, unsigned char *header)
{
    if ((f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_2400B) || (f->mode == FREEDV_MODE_800XA)){
        if (!f->deframer->fdc)
            f->deframer->fdc = freedv_data_channel_create();
        if (!f->deframer->fdc)
            return;
        
        freedv_data_set_header(f->deframer->fdc, header);
    }
}

/*---------------------------------------------------------------------------*\

  FUNCTION....: freedv_get_modem_stats
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 28 July 2015

  Return data from the modem.  The arguments are pointers to the data items.  The
  pointers can be NULL if the data item is not wanted.

\*---------------------------------------------------------------------------*/

void freedv_get_modem_stats(struct freedv *f, int *sync, float *snr_est)
{
    if (f->mode == FREEDV_MODE_1600)
        fdmdv_get_demod_stats(f->fdmdv, &f->stats);
#ifndef CORTEX_M4
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B)  || (f->mode == FREEDV_MODE_700C))
        cohpsk_get_demod_stats(f->cohpsk, &f->stats);
    if (f->mode == FREEDV_MODE_700D) {
        ofdm_get_demod_stats(f->ofdm, &f->stats);
    }
#endif
    if (sync) *sync = f->stats.sync;
    if (snr_est) *snr_est = f->stats.snr_est;
}

/*---------------------------------------------------------------------------*\

  FUNCTIONS...: freedv_set_*
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 28 July 2015

  Set some parameters used by FreeDV.  It is possible to write a macro using ## for
  this, but I wasn't sure it would be 100% portable.

\*---------------------------------------------------------------------------*/

// Set integers
void freedv_set_test_frames               (struct freedv *f, int val) {f->test_frames = val;}
void freedv_set_test_frames_diversity	  (struct freedv *f, int val) {f->test_frames_diversity = val;}
void freedv_set_squelch_en                (struct freedv *f, int val) {f->squelch_en = val;}
void freedv_set_total_bit_errors          (struct freedv *f, int val) {f->total_bit_errors = val;}
void freedv_set_total_bits                (struct freedv *f, int val) {f->total_bits = val;}
void freedv_set_total_bit_errors_coded    (struct freedv *f, int val) {f->total_bit_errors_coded = val;}
void freedv_set_total_bits_coded          (struct freedv *f, int val) {f->total_bits_coded = val;}
void freedv_set_clip                      (struct freedv *f, int val) {f->clip = val;}
void freedv_set_varicode_code_num         (struct freedv *f, int val) {varicode_set_code_num(&f->varicode_dec_states, val);}


/* Band Pass Filter to cleanup OFDM tx waveform, only supported by FreeDV 700D */

void freedv_set_tx_bpf(struct freedv *f, int val) {
    if (f->mode == FREEDV_MODE_700D) {
        ofdm_set_tx_bpf(f->ofdm, val);
    }
}


void freedv_set_verbose(struct freedv *f, int verbosity) {
    f->verbose = verbosity;
    if (f->mode == FREEDV_MODE_700D) {
        ofdm_set_verbose(f->ofdm, f->verbose);
    }
}

// Set floats
void freedv_set_snr_squelch_thresh        (struct freedv *f, float val) {f->snr_squelch_thresh = val;}

void freedv_set_callback_error_pattern    (struct freedv *f, freedv_calback_error_pattern cb, void *state)
{
    f->freedv_put_error_pattern = cb;
    f->error_pattern_callback_state = state;
}

#ifndef CORTEX_M4
void freedv_set_carrier_ampl(struct freedv *freedv, int c, float ampl) {
    assert(freedv->mode == FREEDV_MODE_700C);
    cohpsk_set_carrier_ampl(freedv->cohpsk, c, ampl);
}
#endif

/*---------------------------------------------------------------------------*\

  FUNCTIONS...: freedv_set_alt_modem_samp_rate
  AUTHOR......: Brady O'Brien
  DATE CREATED: 25 June 2016

  Attempt to set the alternative sample rate on the modem side of the api. Only
   a few alternative sample rates are supported. Please see below.
   
   2400A - 48000, 96000
   2400B - 48000, 96000
  
  TODO: Implement 2400B rate changing, allow other rate changing.
   

\*---------------------------------------------------------------------------*/

int freedv_set_alt_modem_samp_rate(struct freedv *f, int samp_rate){
	if(f->mode == FREEDV_MODE_2400A){ 
		if(samp_rate == 24000 || samp_rate == 48000 || samp_rate == 96000){
			fsk_destroy(f->fsk);
			f->fsk = fsk_create_hbr(samp_rate,1200,10,4,1200,1200);
        
			free(f->tx_bits);
			/* Note: fsk expects tx/rx bits as an array of uint8_ts, not ints */
			f->tx_bits = (int*)malloc(f->fsk->Nbits*sizeof(uint8_t));
        
			f->n_nom_modem_samples = f->fsk->N;
			f->n_max_modem_samples = f->fsk->N + (f->fsk->Ts);
			f->n_nat_modem_samples = f->fsk->N;
			f->nin = fsk_nin(f->fsk);
			f->modem_sample_rate = samp_rate;
			return 0;
		}else
			return -1;
	}else if(f->mode == FREEDV_MODE_2400B){
		if(samp_rate == 48000 || samp_rate == 96000){
			return -1;
		}else
			return -1;
	}
	return -1;
}


/*---------------------------------------------------------------------------* \

  FUNCTIONS...: freedv_set_sync
  AUTHOR......: David Rowe
  DATE CREATED: May 2018

  Extended control of sync state machines, especially for FreeDV 700D.
  This mode is required to acquire sync up at very low SNRS.  This is
  difficult to implement, for example we may get a false sync, or the
  state machine may fall out of sync by mistake during a long fade.

  So with this API call we allow some operator assistance.

  Ensure this is called inthe same thread as freedv_rx().

\*---------------------------------------------------------------------------*/

void freedv_set_sync(struct freedv *freedv, int sync_cmd) {
    assert (freedv != NULL);

    if (freedv->mode == FREEDV_MODE_700D) {
        ofdm_set_sync(freedv->ofdm, sync_cmd);        
    }
    
}

struct FSK * freedv_get_fsk(struct freedv *f){
	return f->fsk;
}

/*---------------------------------------------------------------------------*\

  FUNCTIONS...: freedv_get_*
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 28 July 2015

  Get some parameters from FreeDV.  It is possible to write a macro using ## for
  this, but I wasn't sure it would be 100% portable.

\*---------------------------------------------------------------------------*/

// Get integers
int freedv_get_protocol_bits              (struct freedv *f) {return f->n_protocol_bits;}
int freedv_get_mode                       (struct freedv *f) {return f->mode;}
int freedv_get_test_frames                (struct freedv *f) {return f->test_frames;}
int freedv_get_n_speech_samples           (struct freedv *f) {return f->n_speech_samples;}
int freedv_get_modem_sample_rate          (struct freedv *f) {return f->modem_sample_rate;}
int freedv_get_n_max_modem_samples        (struct freedv *f) {return f->n_max_modem_samples;}
int freedv_get_n_nom_modem_samples        (struct freedv *f) {return f->n_nom_modem_samples;}
int freedv_get_total_bits                 (struct freedv *f) {return f->total_bits;}
int freedv_get_total_bit_errors           (struct freedv *f) {return f->total_bit_errors;}
int freedv_get_total_bits_coded           (struct freedv *f) {return f->total_bits_coded;}
int freedv_get_total_bit_errors_coded     (struct freedv *f) {return f->total_bit_errors_coded;}
int freedv_get_sync                       (struct freedv *f) {return f->stats.sync;}

int freedv_get_sync_interleaver(struct freedv *f) {
    if (f->mode == FREEDV_MODE_700D) {
        return !strcmp(f->ofdm->sync_state_interleaver,"synced");
    }
    return 0;
}

int freedv_get_sz_error_pattern(struct freedv *f) 
{
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C)) {
        /* if diversity disabled callback sends error pattern for upper and lower carriers */
        return f->sz_error_pattern * (2 - f->test_frames_diversity);
    }
    else {
        return f->sz_error_pattern;
    }
}

// Get floats

struct CODEC2 *freedv_get_codec2	(struct freedv *f){return  f->codec2;}
int freedv_get_n_codec_bits             (struct freedv *f){return f->n_codec_bits;}

void freedv_get_modem_extended_stats(struct freedv *f, struct MODEM_STATS *stats)
{
    if (f->mode == FREEDV_MODE_1600)
        fdmdv_get_demod_stats(f->fdmdv, stats);

    if ((f->mode == FREEDV_MODE_2400A) || (f->mode == FREEDV_MODE_800XA)) {
        fsk_get_demod_stats(f->fsk, stats);
        float EbNodB = stats->snr_est;                       /* fsk demod actually estimates Eb/No     */
        stats->snr_est = EbNodB + 10.0*log10f(800.0/3000.0); /* so convert to SNR Rb=800, noise B=3000 */
    }

    if (f->mode == FREEDV_MODE_2400B) {
        fmfsk_get_demod_stats(f->fmfsk, stats);
    }
    
#ifndef CORTEX_M4
    if ((f->mode == FREEDV_MODE_700) || (f->mode == FREEDV_MODE_700B) || (f->mode == FREEDV_MODE_700C)) {
        cohpsk_get_demod_stats(f->cohpsk, stats);
    }
    
    if (f->mode == FREEDV_MODE_700D) {
        ofdm_get_demod_stats(f->ofdm, stats);
    }
    
#endif
}

/*--  Functions below this line are private, and not meant for public use  --*/
/*---------------------------------------------------------------------------*\

  FUNCTIONS...: quisk_filt_cfInit
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 27 August 2015

  Initialize a FIR filter that will be used to change sample rates.  These rate
  changing filters were copied from Quisk and modified for float samples.

\*---------------------------------------------------------------------------*/

static void quisk_filt_cfInit(struct quisk_cfFilter * filter, float * coefs, int taps)
{    // Prepare a new filter using coefs and taps.  Samples are complex.
    filter->dCoefs = coefs;
    filter->cSamples = (COMP *)malloc(taps * sizeof(COMP));
    memset(filter->cSamples, 0, taps * sizeof(COMP));
    filter->ptcSamp = filter->cSamples;
    filter->nTaps = taps;
    filter->cBuf = NULL;
    filter->nBuf = 0;
    filter->decim_index = 0;
}

/*---------------------------------------------------------------------------*\

  FUNCTIONS...: quisk_filt_destroy
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 27 August 2015

  Destroy the FIR filter and free all resources.

\*---------------------------------------------------------------------------*/

static void quisk_filt_destroy(struct quisk_cfFilter * filter)
{
    if (filter->cSamples) {
        free(filter->cSamples);
        filter->cSamples = NULL;
    }
    if (filter->cBuf) {
        free(filter->cBuf);
        filter->cBuf = NULL;
    }
}

/*---------------------------------------------------------------------------*\

  FUNCTIONS...: quisk_cfInterpDecim
  AUTHOR......: Jim Ahlstrom
  DATE CREATED: 27 August 2015

  Take an array of samples cSamples of length count, multiply the sample rate
  by interp, and then divide the sample rate by decim.  Return the new number
  of samples.  Each specific interp and decim will require its own custom
  FIR filter.

\*---------------------------------------------------------------------------*/

static int quisk_cfInterpDecim(COMP * cSamples, int count, struct quisk_cfFilter * filter, int interp, int decim)
{   // Interpolate by interp, and then decimate by decim.
    // This uses the float coefficients of filter (not the complex).  Samples are complex.
    int i, k, nOut;
    float * ptCoef;
    COMP * ptSample;
    COMP csample;

    if (count > filter->nBuf) {    // increase size of sample buffer
        filter->nBuf = count * 2;
        if (filter->cBuf)
            free(filter->cBuf);
        filter->cBuf = (COMP *)malloc(filter->nBuf * sizeof(COMP));
    }
    memcpy(filter->cBuf, cSamples, count * sizeof(COMP));
    nOut = 0;
    for (i = 0; i < count; i++) {
        // Put samples into buffer left to right.  Use samples right to left.
        *filter->ptcSamp = filter->cBuf[i];
        while (filter->decim_index < interp) {
            ptSample = filter->ptcSamp;
            ptCoef = filter->dCoefs + filter->decim_index;
            csample.real = 0;
            csample.imag = 0;
            for (k = 0; k < filter->nTaps / interp; k++, ptCoef += interp) {
                csample.real += (*ptSample).real * *ptCoef;
                csample.imag += (*ptSample).imag * *ptCoef;
                if (--ptSample < filter->cSamples)
                    ptSample = filter->cSamples + filter->nTaps - 1;
            }
            cSamples[nOut].real = csample.real * interp;
            cSamples[nOut].imag = csample.imag * interp;
            nOut++;
            filter->decim_index += decim;
        }
        if (++filter->ptcSamp >= filter->cSamples + filter->nTaps)
            filter->ptcSamp = filter->cSamples;
        filter->decim_index = filter->decim_index - interp;
    }
    return nOut;
}

//===================================================================================
//
//
//                           CRC-16 Checksum Functions
//
//
//===================================================================================
/***********************************************************************
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 **********************************************************************/

/*
 * Derive parameters from the standard-specific parameters in crc.h.
 */

#if (REFLECT_DATA == 1)
#undef  REFLECT_DATA
#define REFLECT_DATA(X)			((unsigned char) reflect((X), 8))
#else
#undef  REFLECT_DATA
#define REFLECT_DATA(X)			(X)
#endif

#if (REFLECT_REMAINDER == 1)
#undef  REFLECT_REMAINDER
#define REFLECT_REMAINDER(X)	((crc) reflect((X), (8 * sizeof(crc))))
#else
#undef  REFLECT_REMAINDER
#define REFLECT_REMAINDER(X)	(X)
#endif


 /*********************************************************************
  *
  * Function:    reflect()
  *
  * Description: Reorder the bits of a binary sequence, by reflecting
  *				them about the middle position.
  *
  * Notes:		No checking is done that nBits <= 32.
  *
  * Returns:		The reflection of the original data.
  *
  *********************************************************************/
static unsigned long reflect(unsigned long data, unsigned char nBits)
{
	unsigned long  reflection = 0x00000000;
	unsigned char  bit;

	/*
	 * Reflect the data about the center bit.
	 */
	for (bit = 0; bit < nBits; ++bit)
	{
		/*
		 * If the LSB bit is set, set the reflection of it.
		 */
		if (data & 0x01)
		{
			reflection |= (1 << ((nBits - 1) - bit));
		}

		data = (data >> 1);
	}

	return (reflection);

}	/* reflect() */


/*********************************************************************
 *
 * Function:    crcSlow()
 *
 * Description: Compute the CRC of a given message.
 *
 * Notes:
 *
 * Returns:		The CRC of the message.
 *
 *********************************************************************/
crc crcSlow(unsigned char const message[], int nBytes)
{
	//crc            remainder = INITIAL_REMAINDER;
	//
	//int            byte;
	//unsigned char  bit;


	///*
	// * Perform modulo-2 division, a byte at a time.
	// */
	//for (byte = 0; byte < nBytes; ++byte)
	//{
	//	/*
	//	 * Bring the next byte into the remainder.
	//	 */
	//	remainder ^= (REFLECT_DATA(message[byte]) << ((8 * sizeof(crc)) - 8));

	//	/*
	//	 * Perform modulo-2 division, a bit at a time.
	//	 */
	//	for (bit = 8; bit > 0; --bit)
	//	{
	//		/*
	//		 * Try to divide the current data bit.
	//		 */
	//		if (remainder & (1 << ((8 * sizeof(crc)) - 1)))
	//		{
	//			remainder = (remainder << 1) ^ POLYNOMIAL;
	//		}
	//		else
	//		{
	//			remainder = (remainder << 1);
	//		}
	//	}
	//}

	///*
	// * The final remainder is the CRC result.
	// */
	//return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);

	return 0;

}   /* crcSlow() */


/*********************************************************************
 *
 * Function:    crcInit()
 *
 * Description: Populate the partial CRC lookup table.
 *
 * Notes:		This function must be rerun any time the CRC standard
 *				is changed.  If desired, it can be run "offline" and
 *				the table results stored in an embedded system's ROM.
 *
 * Returns:		None defined.
 *
 *********************************************************************/
void crcInit(struct data_tx_rx * data_tx_rx_mod)
{
	crc			   remainder;
	int			   dividend;
	unsigned char  bit;

	/*
	 * Compute the remainder of each possible dividend.
	 */
	for (dividend = 0; dividend < 256; ++dividend)
	{
		/*
		 * Start with the dividend followed by zeros.
		 */
		remainder = dividend << ((8 * sizeof(crc)) - 8);

		/*
		 * Perform modulo-2 division, a bit at a time.
		 */
		for (bit = 8; bit > 0; --bit)
		{
			/*
			 * Try to divide the current data bit.
			 */
			if (remainder & (1 << ((8 * sizeof(crc)) - 1)))
			{
#ifndef JAK_CRC_DEFS
				remainder = (remainder << 1) ^ POLYNOMIAL;
#else
				remainder = (remainder << 1) ^ 0x1021;
#endif
			}
			else
			{
				remainder = (remainder << 1);
			}
		}

		/*
		 * Store the result into the table.
		 */
		data_tx_rx_mod->crcTable[dividend] = remainder;
	}

}   /* crcInit() */


/*********************************************************************
 *
 * Function:    crcFast()
 *
 * Description: Compute the CRC of a given message.
 *
 * Notes:		crcInit() must be called first.
 *
 * Returns:		The CRC of the message.
 *
 *********************************************************************/
crc crcFast(unsigned char const message[], int nBytes, struct data_tx_rx * data_tx_rx_mod)
{

#ifndef JAK_CRC_DEFS
	crc	           remainder = INITIAL_REMAINDER;
#else
	crc	           remainder = 0xFFFF;
#endif
	unsigned char  data;
	int            byte;


	/*
	 * Divide the message by the polynomial, a byte at a time.
	 */
	for (byte = 0; byte < nBytes; ++byte)
	{
#ifndef JAK_CRC_DEFS
		data = REFLECT_DATA(message[byte]) ^ (remainder >> ((8 * sizeof(crc)) - 8));
#else
		data = (message[byte]) ^ (remainder >> ((8 * sizeof(crc)) - 8));
#endif



		remainder = data_tx_rx_mod->crcTable[data] ^ (remainder << 8);
	}

	/*
	 * The final remainder is the CRC.
	 */

#ifndef JAK_CRC_DEFS
	return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);
#else
	return ((remainder) ^ 0x0000);
#endif

}   /* crcFast() */


/* END OF CRC FUNCTIONS PROVIDED BY MICHAEL BARR */



//===========================================================================
/*===========================================================================

					DATA FUNCTION DEFINITIONS

=============================================================================*/
//===========================================================================


#ifdef EMBEDDED

/**************************************************************


			State Control


***************************************************************/



/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  data_state_control()
AUTHOR:
DESC  : Control stages of Transmission of data (ARQ and normal UART)

Function here denotes actions for each transmission method:

- DATA_ARQ  - Data Protocol for sending error-free data in a series
of sync-data-eot phased transmission. See DATA_UART.

- DATA_UART - To send a 700 byte snippet via a sync-data-eot phased
transmission similar to that descibed in
US-MIL-STD-188-110C

- DATA_TEST - Sends a continuous stream of text. Used to determine if
any data is being received at receiver end.

- DV_TEST   - Enables DV voice transmission by simple UART PTT.

N.B:
There is a helper function that handles the actual sync and eot
transmission (sync_data_eot_transfer) for a single data transfer.
------------------------------------------------------------------------
----------------------------------------------------------------------*/


void data_state_control(struct timers * data_timers, struct data_struct * data_buffers,
	struct data_tx_rx * data_tx_rx_mod, struct arq_machine * arq_mod, struct freedv * f) {


	volatile unsigned int counter = 0;

	//--------------------------------
	// Update Each Timer Status
	//--------------------------------


	switch (*data_tx_rx_mod->op_mode) {
#ifdef ARQ_ENABLED					
	case DATA_ARQ:
		//----------------------------------------------------
		//
		//			ARQ SEQUENTIAL STATE MACHINE
		//
		//----------------------------------------------------
		switch (arq_mod->arq_state) {

			//=====================================================================================================
			// RX STATES ---------------------------------------------------------------------------------
			//=====================================================================================================

		case ARQ_RX_DATA:

			//------------------------------------------------------------------------------
			// 1. ARQ_RX_DATA: - 
			// THIS STATE IS THE DEFAULT.
			// 
			// A. IF DATA DETECTED FROM UART BUFFER, STATE SWITCHES TO: ARQ_TX_DATA
			//
			// B. IF AN INCOMING SIGNAL IS DETECTED, A DATA RX TRANSFER HAS BEGUN
			//
			// C. WHEN ALL FRAMES HAVE BEEN RECEIVED FOR ONE DATA SESSION THEN SWITCH 
			//    STATE TO ARQ_TX_RX_COMPLETE 
			//
			// D. WHEN ALL FRAMES RECEIVED FOR 1ST TRANSFER/PASS, PROCESSING CONTINUES TO:
			//    ARQ_RX_WAIT
			//
			// E. PROBLEM OCCURS WHILST EXPECTING INCOMING SIGNAL DURING A DATA TRANSFER 
			//------------------------------------------------------------------------------

			//=============================================================
			// NO CURRENT DATA SESSION - IDLING RECEIVE
			//=============================================================

			if (!arq_mod->data_session) {

				//=============================================================
				// 1.A : BEGINNING A NEW TRANSMISSION
				//=============================================================
#ifdef EMBEDDED
				data_tx_rx_mod->is_idle = (FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE); // Is UART idle ?
#else
				data_tx_rx_mod->is_idle = 1;
#endif
				if (data_tx_rx_mod->is_idle  && data_tx_rx_mod->tx_uart_data_index && !data_tx_rx_mod->data_transmission) { // Same beginning conditions as for normal UART communication

					// Begin transmission
					data_tx_rx_mod->begin_tx = 1;

					// Set Bffrs Ack Matrix
					init_bffrs_acks_matrix(data_buffers);

					// Set State to Transmit Stage
					arq_mod->prv_state = arq_mod->arq_state;
					arq_mod->arq_state = ARQ_TX_DATA;

					// Reset transmissions flag
					arq_mod->transmissions = 0;
					// Set Session flag
					arq_mod->data_session = 1;
				}

				//=============================================================
				// 1.B : A DATA RECEIVING SESSION BEGINS
				//=============================================================

				if (data_tx_rx_mod->rx_demod_sync) {   // VARIABLE RECEIVED FROM DEMOD FUNCTION

					// New Session - Reset Number of transmissions
					arq_mod->transmissions = 0;
					// Set Session flag to 1
					arq_mod->data_session = 1;
					// Set Ack_frame_index to NONE
					arq_mod->arq_ack_frame_indexer = 0xFF;

				}

			}

			//=============================================================
			//  ONGOING DATA SESSION - ACTIVE RECEIVE
			//=============================================================
			else {

				//==================================================================
				//  1.C : ACK COUNTER EQUALS 0 -> ALL DATA SUCCESSFULLY RXED
				//==================================================================
				if ((arq_mod->arq_ack_frame_indexer <= 0))
				{
					arq_mod->prv_state = arq_mod->arq_state;
					arq_mod->arq_state = ARQ_TX_RX_COMPLETE;

					// End this link and work on presenting/printing data to UART/screen/whatever...
				}

				//==========================================================
				// If sync signal detected...
				//==========================================================
				if (data_tx_rx_mod->rx_demod_sync) {
					//==========================================================
					//---------------------------------------------
					// JAK EDIT: 11/12/18
					//           We experienced strange behaviour 
					//           where rx_data_timeout should have 
					// been of 6s after transmission of ack frames
					// however this lasted to periods of no more
					// than 2s and sometimes less.
					//
					// Hence we implemented this check that as soon
					// as a sync signal was detected by the demod.
					// the timer would be reset
					//---------------------------------------------
					//==========================================================

					// Whenever a signal is detected this variable is set
					arq_mod->data_transfer = 1;

					// We reset the timer to zero whenever a sync signal is not detected
					if (data_timers->start_rx_data == 1) {
						data_timers->rx_data_time_elapsed = 0;
						data_timers->start_rx_data = 0;
					}

				}
				//==========================================================
				// If no signal detected...
				//==========================================================
				else {
					//=============================================================
					//  1.D : END OF DATA TRANSFER - SUCCESSFUL PASS
					//=============================================================
					if (arq_mod->data_transfer) {
						// Reset Data_transfer flag
						arq_mod->data_transfer = 0;

						// Increment no. of transmissions received
						arq_mod->transmissions++;

						// Determine the ACK number
						post_receive_data_transfer_processing(data_buffers, arq_mod);

						//Output contents of Error-free Matrix
						data_tx_rx_mod->debug_data_f_ptr(data_buffers, arq_mod);

#ifdef REPEAT_TRANSMIT

						if (arq_mod->arq_ack_frame_indexer == 0) {

							//---------------------------------------------
							// Transmit back - This confirms that all
							//				   data was received correctly
							//---------------------------------------------

							// Set receiver to transitory wait state
							arq_mod->prv_state = arq_mod->arq_state;
							arq_mod->arq_state = ARQ_WAIT;
						}
						else {
							arq_mod->prv_state = arq_mod->arq_state;
							arq_mod->arq_state = ARQ_RX_DATA;
							// Reset time elapsed - New listening period begun
							data_timers->rx_data_time_elapsed = 0;
						}

#else

						// Set receiver to transitory wait state
						arq_mod->prv_state = arq_mod->arq_state;
						arq_mod->arq_state = ARQ_WAIT;
#endif

					}
					else {
						//=============================================================
						//  1.E : RX TIMEOUT - ERROR OCCURRED !
						//=============================================================
						if (!data_timers->start_rx_data) {
							data_timers->start_rx_data = 1;		//	activate incoming data timeout clock
						}

						if (data_timers->rx_data_time_elapsed >= ARQ_RX_DATA_TIMEOUT_PERIOD) {
							// Reset Timer
							data_timers->rx_data_time_elapsed = 0;
							data_timers->start_rx_data = 0;

							// ERROR OCCURRED
							arq_mod->prv_state = arq_mod->arq_state;
							arq_mod->arq_state = ARQ_ERROR;
						}
					}

				}
			}

			break;

		case ARQ_RX_ACK:
			//-----------------------------------------------------------
			// ARQ_RX_ACK:
			// Transmitter is waiting for an acknowledgement
			//-----------------------------------------------------------



#ifdef REPEAT_TRANSMIT

			//----------------------------------------------------------------
			// If REPEAT_TRANSMIT is defined then there is no acknowledgement
			// per se via ack frames....
			//
			// We use a simple sync signal from the receiver to mean that the 
			// data has been received
			//-----------------------------------------------------------------

			if (data_tx_rx_mod->rx_demod_sync) {
				// Turn off the timer
				data_timers->start_rx_ack = 0;
			}
			else {
				data_timers->start_rx_ack = 1;
			}

			if (data_timers->rx_ack_time_elapsed >= ARQ_RX_ACK_TIMEOUT_PERIOD) { // Leave ARQ_RX_WAIT_period equal to like 20s first for experimental purposes

				// Since timer has reached value.. 
				// Stop timer and avoid risk of overflow - SM1000 risk not FreeDV really!
				data_timers->start_rx_ack = 0;

				// No action is performed if there is a signal detected
				if (!data_tx_rx_mod->rx_demod_sync) {
					// Reset Timer to Zero
					data_timers->rx_ack_time_elapsed = 0;

					//---------------------------------------------------------------------
					// Decision:
					// If ack successfully received perform partial/full retransmission
					//---------------------------------------------------------------------
					if (arq_mod->rx_ack_received) {
						// Set New State
						arq_mod->prv_state = arq_mod->arq_state;
						arq_mod->arq_state = ARQ_TX_RX_COMPLETE;

					}

					else {
						//---------------------------------------
						// No ack Received - Repeat transmission
						//---------------------------------------

						// Set New State
						arq_mod->prv_state = arq_mod->arq_state;
						arq_mod->arq_state = ARQ_WAIT;

						// Begin retransmission
						data_tx_rx_mod->begin_tx = 1;
					}
				}

			}

#else

			//-----------------------------------------------
			// WAIT to receive ACK from receiver
			//-----------------------------------------------
			// If we wait ARQ_RX_ACK_WAIT_PERIOD and no answer we transmit the data another time
			// If no ack we do this 5 times and then we terminate transmission.
			// ----------------------------------------------
			// LED blinks to indicate no transmission  
			//-----------------------------------------------

			//---------------------------------------
			// Debug Function
			// It prints some debug info...
			//---------------------------------------

			//----------------------------------------------------
			// This turns off the timer in the case that a signal
			// is being received.
			//----------------------------------------------------

			if (data_tx_rx_mod->rx_demod_sync) {
				// Turn off the timer
				data_timers->start_rx_ack = 0;
			}
			else {
				data_timers->start_rx_ack = 1;
			}


			if (data_timers->rx_ack_time_elapsed >= ARQ_RX_ACK_TIMEOUT_PERIOD) { // Leave ARQ_RX_WAIT_period equal to like 20s first for experimental purposes

				// Since timer has reached value.. 
				// Stop timer and avoid risk of overflow - SM1000 risk not FreeDV really!
				data_timers->start_rx_ack = 0;

				// No action is performed if there is a signal detected
				if (!data_tx_rx_mod->rx_demod_sync) {
					// Reset Timer to Zero
					data_timers->rx_ack_time_elapsed = 0;

					//---------------------------------------------------------------------
					// Decision:
					// If ack successfully received perform partial/full retransmission
					//---------------------------------------------------------------------

					if (arq_mod->rx_ack_received) {

						// Reset flag to zero
						arq_mod->rx_ack_received = 0;

						// Begin retransmission
						data_tx_rx_mod->begin_tx = 1;

						// Acquire rx frame number
						output_rx_ack_frame_number(data_buffers, arq_mod);

						//-------------------------------------------------------
						// 0xFF: It is a special case ACK number which indicates
						//       that the receiver is still not aware that the 
						//       of the number of frames it has to receive for this
						//       session.
						// 
						// This prompts a complete re-transmission since this 
						// is unaware of what was received and what was not.
						//-------------------------------------------------------
						if (arq_mod->arq_rx_frame_indexer == 0xff) {

							// The ack_frame_indexer is left as is, rx still expecting the
							// the first frame to be received (i.e. frame with highest frame number )
							//---------------------------
							// Full Retransmission ...
							//---------------------------
						}
						else {
							//-----------------------------------------------------------
							// Here a non-0xFF ack was received. Transmitter knows that
							// arq_rx_frame_indexer is the frame up to which the receiver 
							// has successfully and contiguously received.
							//-----------------------------------------------------------
							// So now, transmitter, sets the arq_ack_frame_indexer to 
							// arq_rx_frame_indexe. Now the transmitter will re-transmit
							// the data from this point downwards
							//------------------------------------------------------------
							arq_mod->arq_ack_frame_indexer = arq_mod->arq_rx_frame_indexer;
						}

						// Set New State
						arq_mod->prv_state = arq_mod->arq_state;
						arq_mod->arq_state = ARQ_WAIT;

					}
					//---------------------------------------
					// No ack received - Error Occurred!
					//---------------------------------------
					else {
						//---------------------------
						// ERROR OCCURRED
						//---------------------------
						arq_mod->prv_state = arq_mod->arq_state;
						arq_mod->arq_state = ARQ_ERROR;
					}
				}

			}

#endif //REPEAT_TRANSMIT


			break;

			//=====================================================================================================
			// TX STATES ------------------------------------------------------------------------------------------
			//=====================================================================================================

		case ARQ_TX_DATA:
			//-----------------------------------------------------------
			// ARQ_TX_DATA:
			// Transmitter is sending data 
			//-----------------------------------------------------------

			if (data_tx_rx_mod->begin_tx) {

				//-------------------------------------------
				// Offset Control for Transmitting from End
				//-------------------------------------------

				//-----------------------------------------------
				// Calculate Total Number of Frames
				//-----------------------------------------------
				// The first transmission of data .... 
				//------------------------------------------------
				if (arq_mod->transmissions == 0) {

					//-------------------------------------------
					// Calculate the initial offset
					//-------------------------------------------
					data_tx_rx_mod->init_rem = (data_tx_rx_mod->tx_uart_data_index + 1) % ARQ_BYTES_PER_FRAME;

					if (data_tx_rx_mod->init_rem != 0) {
						// ----------------------------------------------------------------------
						// The 1 in the calculation handles the 'starting from zero' problem
						// Take an example and check...
						// ----------------------------------------------------------------------
						data_tx_rx_mod->data_offset = data_tx_rx_mod->tx_uart_data_index + (-data_tx_rx_mod->init_rem + 1);

						//-------------------------------------------
						// Calculate number of frames required
						//-------------------------------------------
						arq_mod->arq_total_session_frames = (data_tx_rx_mod->tx_uart_data_index) / ARQ_BYTES_PER_FRAME;
						arq_mod->arq_total_session_frames += 1;

					}
					else {
						//-----------------------------------------------------------------------------------------------
						// Complete frame can be sent so... offset set for 1 complete frame - ARQ_BYTES_PER_FRAME bytes
						//-----------------------------------------------------------------------------------------------
						data_tx_rx_mod->data_offset = data_tx_rx_mod->tx_uart_data_index + (-ARQ_BYTES_PER_FRAME + 1);
						arq_mod->arq_total_session_frames = (data_tx_rx_mod->tx_uart_data_index) / ARQ_BYTES_PER_FRAME;
					}

					//----------------------------------------------------------------------------------------------------
					// ACK frame Indexer set to highest frame number, indicates machine is waiting for ack of this frame
					//-----------------------------------------------------------------------------------------------------
					arq_mod->arq_ack_frame_indexer = arq_mod->arq_total_session_frames - 1;

					//----------------------------------------------------------------------------------
					// Here we set the initial offset variable
					// This offset variable will be used to denote the starting location of every frame
					//----------------------------------------------------------------------------------
					data_tx_rx_mod->init_offset = data_tx_rx_mod->data_offset;
				}
				//--------------------------------------------------------
				// Retransmission - Set data_offset accordingly...
				//--------------------------------------------------------
				else {

#ifdef REPEAT_TRANSMIT
					//-----------------------------------------------
					// Resetting variables for full retransmission
					//------------------------------------------------
					data_tx_rx_mod->data_offset = data_tx_rx_mod->init_offset;
					arq_mod->arq_ack_frame_indexer = arq_mod->arq_total_session_frames - 1;
#else
					data_tx_rx_mod->data_offset = (arq_mod->arq_ack_frame_indexer - 1)*ARQ_BYTES_PER_FRAME;
#endif

				}
				//-------------------------------------------------
				// Beginning another transmission
				//-------------------------------------------------

				arq_mod->transmissions++;
				data_tx_rx_mod->data_transmission = 1;
				data_tx_rx_mod->begin_tx = 0;

				// Switch Data Tx Stage to SYNCING with Receiver
				data_tx_rx_mod->data_tx_stage = SYNCING;

			}
			else if (arq_mod->data_tx_completed) {

				arq_mod->data_tx_completed = 0;
				arq_mod->sent_first_data_packet = 0;

				arq_mod->prv_state = arq_mod->arq_state;
				arq_mod->arq_state = ARQ_RX_ACK;

				data_tx_rx_mod->no_bits_detected = 0;
			}

			if (arq_mod->arq_state == ARQ_TX_DATA) {
				sync_data_eot_transfer(data_buffers, data_timers, data_tx_rx_mod, arq_mod);
			}

			break;

		case ARQ_TX_ACK:
			//-----------------------------------------------------------
			// ARQ_TX_ACK:
			// Receiver is sending back an acknowledge.
			// It's a repeat transmit to make sure that at least one was received without errors
			//-----------------------------------------------------------	

			if (!data_timers->start_tx_ack) {
				// Start TX_ACK clock
				data_timers->start_tx_ack = 1;
				// Activate Transmission
				activate_ptt(1, data_tx_rx_mod);

				//---------------------------------------------------------
				// Send the ack frame number.
				// Ack frame number indicates the last frame successfully
				// and contiguously received. (last - lowest frame number
				//---------------------------------------------------------
				arq_mod->arq_tx_frame_indexer = arq_mod->arq_ack_frame_indexer;

				data_buffers->data_text_ack[0] = arq_mod->arq_ack_frame_indexer;

			}
			else if (data_timers->tx_ack_time_elapsed >= ARQ_TX_ACK_PERIOD) {
				// Turn off Timer
				data_timers->start_tx_ack = 0;
				// Reset Timer 
				data_timers->tx_ack_time_elapsed = 0;

				// Change State
				arq_mod->prv_state = arq_mod->arq_state;
				arq_mod->arq_state = ARQ_RX_DATA;

				//---------------------------------------------------
				// Set to receive and stop Transmission ------------
				//---------------------------------------------------
				activate_ptt(0, data_tx_rx_mod);

			}

			break;

			//=====================================================================================================
			// WAIT / ARQ TX/RX SESSION COMPLETE / ERROR ----------------------------------------------------------
			//=====================================================================================================

		case ARQ_WAIT:
			//--------------------------------------------------------------------
			// ARQ_WAIT:
			// This state is entered each and every time either Txer/Rxer are to
			// transmit data or an acknowledgement.
			//--------------------------------------------------------------------

			if (!data_timers->start_wait) {
				// BEGIN RX WAIT TIMER
				data_timers->start_wait = 1;
			}
			else if (data_timers->wait_time_elapsed > ARQ_WAIT_PERIOD) {

				// Turn off Timer
				data_timers->start_wait = 0;

				// Reset Timer
				data_timers->wait_time_elapsed = 0;

				// Change State
				if (arq_mod->prv_state == ARQ_RX_ACK) {
					// Update prv_state
					arq_mod->prv_state = arq_mod->arq_state;

					// Set arq_state
					arq_mod->arq_state = ARQ_TX_DATA;
				}
				else if (arq_mod->prv_state == ARQ_RX_DATA) {
					// Updated prv_state
					arq_mod->prv_state = arq_mod->arq_state;

					// Set arq_state
					arq_mod->arq_state = ARQ_TX_ACK;
				}

			}

			break;

		case ARQ_TX_RX_COMPLETE:
			//-----------------------------------------------------------
			// ARQ_TX_RX_COMPLETE:
			// Receiver has received file without errors.
			// Receiver now prints entire message to UART/screen
			//-----------------------------------------------------------

			if (arq_mod->prv_state == ARQ_RX_DATA) {
				data_tx_rx_mod->data_outputted = data_tx_rx_mod->output_data_f_ptr(data_buffers, arq_mod);

				if (data_tx_rx_mod->data_outputted) {

					arq_mod->prv_state = arq_mod->arq_state;
					arq_mod->arq_state = ARQ_RX_DATA;

					arq_mod->data_session = 0;
					data_tx_rx_mod->data_outputted = 0;
				}

				//----------------------------------
				// Reset the arq machine to zero
				//----------------------------------
				arq_machine_reset(arq_mod);
			}
			else if (arq_mod->prv_state == ARQ_RX_ACK) {
				arq_mod->prv_state = arq_mod->arq_state;
				arq_mod->arq_state = ARQ_RX_DATA;
			}

			break;


		case ARQ_ERROR:

		default:
			break;
		}
		//-----------------------------
		//
		//	END OF ARQ STATE MACHINE
		//
		//-----------------------------


		break;
		// END DATA_UART -----------------------------------
		// ------------------------------------------

#endif // ARQ_ENABLED
		//===================================
		//
		//	END OF ARQ STATE MACHINE
		//
		//===================================

		//=====================================================================================

		//===================================
		//
		//	EXTRA DATA TX/RX STATE/S 
		//
		//===================================

	case DATA_UART:

		//------------------------------
		// Transmitting Data  -------
		//------------------------------
#ifdef EMBEDDED
		data_tx_rx_mod->is_idle = (FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE); // Is UART idle ?
#else
		data_tx_rx_mod->is_idle = 1;
#endif

		if (data_tx_rx_mod->is_idle  && data_tx_rx_mod->tx_uart_data_index && !data_tx_rx_mod->data_transmission) { // uart idle + Uart_index != 0 + !EOT_Waiting_Period
			data_tx_rx_mod->begin_tx = 1;
		}

		//---------------------------------------
		// The very beginning of a Transmission
		//---------------------------------------
		if (data_tx_rx_mod->begin_tx && (data_tx_rx_mod->data_tx_stage == NO_TXING)) {
			data_tx_rx_mod->data_transmission = 1;
			data_tx_rx_mod->begin_tx = 0;
			// Switch Data Tx Stage to SYNCING with Receiver
			data_tx_rx_mod->data_tx_stage = SYNCING;
		}


		sync_data_eot_transfer(data_buffers, data_timers, data_tx_rx_mod, arq_mod);

		break;
		// END DATA_UART -----------------------------------
		// ------------------------------------------

	case DV_TEST:
#ifdef EMBEDDED
		activate_ptt(data_tx_rx_mod->uart_start_stop % 2, data_tx_rx_mod);
#endif			
		break;

	case DATA_TEST:
		//	activate_ptt(1);
		data_tx_rx_mod->data_tx_stage = TRANSMIT_DATA;
#ifdef EMBEDDED
		activate_ptt(data_tx_rx_mod->uart_start_stop % 2, data_tx_rx_mod);
#endif		
		break;

	default:
		break;
	}
}



/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  sync_data_eot_transfer()
AUTHOR:
DESC  : Staged transmission of data. Sending data in phases:

SYNC : Establishing connection with RECEIVER
DATA : Sending data to RECEIVER
EOT  : Closing connection with RECEIVER
------------------------------------------------------------------------
----------------------------------------------------------------------*/

void sync_data_eot_transfer(struct data_struct * data_buffers, struct timers * data_timers,
	struct data_tx_rx * data_tx_rx_mod, struct arq_machine * arq_mod) {

	//----------------------------------------------------------
	// SEQUENTIAL STATE MACHINE - TX STAGE
	//----------------------------------------------------------
	// This section starts each respective phase - SYNC or EOT
	// This ensures that the Systick timer will not start until
	// Sequential State machine has properly run.
	//-----------------------------------------------------------

	if (data_tx_rx_mod->data_transmission) {
		if ((data_tx_rx_mod->data_tx_stage == SYNCING)) {
			if ((!data_timers->start_sync)) {
				// Activate Transmission
				activate_ptt(1, data_tx_rx_mod);
				//Sync Clock
				data_timers->start_sync = 1;	 // Nothing.. just wait to tick to Transmit_DATA...
				data_tx_rx_mod->uchar_ptr = data_buffers->data_text_sync;

				// Set SYNC Frame Content

				if (*data_tx_rx_mod->op_mode == DATA_ARQ) {
					strncpy((char*)data_buffers->data_text_sync, "SYNC_", 5);
					//data_buffers->data_text_sync = "SYNC_";
					data_timers->sync_time = SYNC_TIME_ARQ;
				}
				else if (*data_tx_rx_mod->op_mode == DATA_UART) {
					//data_buffers->data_text_sync = "STATx_";
					strncpy((char*)data_buffers->data_text_sync, "STATx_", 6);
					data_timers->sync_time = SYNC_TIME_UART;
				}


			}
			else if ((data_timers->sync_time_elapsed >= data_timers->sync_time)) {
				data_timers->start_sync = 0;									// Stop timer
				data_timers->sync_time_elapsed = 0;                             // Reset the timer!!
				data_tx_rx_mod->data_tx_stage = TRANSMIT_DATA;					// Proceed to Transmission
			}
		}
		else if (data_tx_rx_mod->data_tx_stage == TRANSMIT_DATA) {

			if (arq_mod->transmissions > 1) {
				volatile int i = 0;
				i++;
			}

			if (data_tx_rx_mod->uart_buffer_sent) {
				data_tx_rx_mod->uart_buffer_sent = 0;                            // Reset the flag!
				data_tx_rx_mod->data_tx_stage = END_OF_TXING;
			}

		}
		else if (data_tx_rx_mod->data_tx_stage == END_OF_TXING) {
			if (!data_timers->start_eot) {
				//EOT clock
				data_timers->start_eot = 1;

				// Set EOT Frame Content + Set EOT Phase Time

				if (*data_tx_rx_mod->op_mode == DATA_ARQ) {
					//data_buffers->data_text_eot = "EOT__";
					strncpy((char*)data_buffers->data_text_eot, "EOT__", 5);
					data_timers->eot_time = EOT_TIME_ARQ;
				}
				else if (*data_tx_rx_mod->op_mode == DATA_UART) {
					strncpy((char*)data_buffers->data_text_eot, "ENDTx_", 6);
					//data_buffers->data_text_eot = "ENDTx_";
					data_timers->eot_time = EOT_TIME_UART;
				}

			}
			else if (data_timers->end_of_tx_time_elapsed >= data_timers->eot_time) {
				data_timers->end_of_tx_time_elapsed = 0;
				data_tx_rx_mod->data_tx_stage = NO_TXING;
				data_timers->start_eot = 0;
				// Set to receive and stop Transmission -------------
				activate_ptt(0, data_tx_rx_mod);
				data_tx_rx_mod->data_transmission = 0;  	// Data Transmission from UART has ceased

				if (*data_tx_rx_mod->op_mode != DATA_ARQ) {
					// Clear Uart_Buffer and reset uart_index
					clear_buffer((unsigned char *)data_buffers->data_from_uart);
					data_tx_rx_mod->tx_uart_data_index = 0;
				}
				//---------------------
				// UART/ARQ Variable 
				//---------------------
				arq_mod->data_tx_completed = 1;
			}
		}
	}
}


/**************************************************************


					Combinational Control


***************************************************************/


/*------------------------------------------------------------------------
--------------------------------------------------------------------------
NAME  :  data_combinational_control()
AUTHOR:
DESC  : Control stages of Transmission/Reception of data (ARQ and non-ARQ)
--------------------------------------------------------------------------
-------------------------------------------------------------------------*/

void data_combinational_control(struct data_struct * data_buffers, struct data_tx_rx * data_tx_rx_mod,
	struct arq_machine * arq_mod) {
	//----------------------------------------------------------
	// COMBINATIONAL MACHINE - TX STAGE
	//----------------------------------------------------------
	// Here the pointers are assigned the proper array addresses
	// which will be fed to the modulation function.
	// Function designed to be flexible for both SM1000 and FreeDV.
	//-----------------------------------------------------------

	switch (*data_tx_rx_mod->op_mode) {
#ifdef ARQ_ENABLED					
	case DATA_ARQ:
		//----------------------------------------------------
		//
		//			ARQ COMBINATIONAL LOGIC MACHINE
		//
		//----------------------------------------------------
		switch (arq_mod->arq_state) {

			//=====================================================================================================
			// RX STATES ------------------------------------------------------------------------------------------
			//=====================================================================================================

		case ARQ_RX_DATA:

			//			if (!data_tx_rx_mod->bit_err_det){
			//				jak_counter();
			//			}

			if ((data_tx_rx_mod->bit_err_det == 0) && arq_mod->data_session && data_tx_rx_mod->rx_demod_sync) {

				//----------------------------------------------------------------------------------------------------------------------------------
				// JAK EDIT: 0807 - 26/11/18
				// OLD CODE:
				//----------------------------------------------------------------------------------------------------------------------------------
				// strncpy((char*)arq_mod->arq_rx_frame_indexer, (const char*) buffers->data_tx_rx_frame[5], 1);
				// strncpy((char*)buffers->data_to_uart[arq_mod->arq_rx_frame_indexer], (const char*) buffers->data_tx_rx_frame, ARQ_BYTES_PER_FRAME);
				//-----------------------------------------------------------------------------------------------------------------------------------
				// NEW CODE: Changed it to use memcpy instead

				if ((strncmp((const char *)data_buffers->data_tx_rx_frame, (const char *) "SYNC_", 5) == 0) && \
					!arq_mod->start_frame_received) {
					// Copy the Frame Index Number
					// This one here indicates the total number of frames for the session
					//memcpy((void*)arq_mod->arq_rx_frame_indexer, (const void*)data_buffers->data_tx_rx_frame[5], 1);
					arq_mod->arq_rx_frame_indexer = data_buffers->data_tx_rx_frame[5];

					// Here the frame_number indicates the total number of frames for the session
					arq_mod->arq_total_session_frames = arq_mod->arq_rx_frame_indexer;

					// The ack indexer is set to this value;
					// It is waiting for this frame to be rxed error-free
					arq_mod->arq_ack_frame_indexer = arq_mod->arq_rx_frame_indexer;

					arq_mod->start_frame_received = 1;

				}
				else if ((strncmp((const char *)data_buffers->data_tx_rx_frame, (const char *) "SYNC_", 5) == 0)) {
					//-----------------------------------------------------------------------------------------------------------------------------------
					// JAK EDIT: 08:45 - 19/12/18
					//-----------------------------------------------------------------------------------------------------------------------------------
					//---------------------------------------------------------------
					// Just ignore frame...
					// Receiver already knows total number of session frames...
					// Future Work:
					//  - Could possibly use these bits for some channel equalization??
					//---------------------------------------------------------------
				}
				else if ((strncmp((const char *)data_buffers->data_tx_rx_frame, (const char *) "EOT__", 5) == 0)) {
					//-----------------------------------------------------------------------------------------------------------------------------------
					// JAK EDIT: 11:25 - 18/12/18
					//-----------------------------------------------------------------------------------------------------------------------------------
					// Here the frame is just ignored.
					// 
					// Since the frame indicator is set to zero, the contents of the frame would be included into the
					// data_rx matrix.
					//
					// This conditional statement rectifies that issue.
					//-----------------------------------------------------------------------------------------------------------------------------------
				}
				else {

					//=================================================================================================
					// JAK EDIT: 12/12/18 : Have noticed that program is running into segmentation faults due to memcpy
					//						when bytes param = 1.
					//						Have changed statements to assignments avoiding memcpy
					//===================================================================================================

					//memcpy((void*)arq_mod->arq_rx_frame_indexer, (const void*)data_buffers->data_tx_rx_frame[5], 1);
					arq_mod->arq_rx_frame_indexer = data_buffers->data_tx_rx_frame[5];

					//---------------------------------
					// Additional Safeguard
					// to stop any unwanted frames
					//---------------------------------
					/*********************************************************************
					NB: JAK EDIT - 1100 - 16012019
					-------------------------------
					For repeat transmit we are removing requirement:
					(arq_mod->arq_rx_frame_indexer < arq_mod->arq_total_session_frames)

					Replacing with:
					if (((const char *)data_buffers->data_text_rx[counter][0]) == 0)

					So only frames which are empty can be filled.

					***********************************************************************/
					if ((arq_mod->arq_rx_frame_indexer < ARQ_MAX_FRAMES) && (arq_mod->arq_rx_frame_indexer >= 0)) {
						if (((const char *)data_buffers->data_text_rx[arq_mod->arq_rx_frame_indexer][0]) == 0) {

							memcpy((void*)data_buffers->data_text_rx[arq_mod->arq_rx_frame_indexer], (const void*)data_buffers->data_tx_rx_frame, ARQ_BYTES_PER_FRAME);
							//memcpy((void*)data_buffers->data_text_rx[arq_mod->arq_rx_frame_indexer - 1][5], (const void*) '\0', 1);
							data_buffers->data_text_rx[arq_mod->arq_rx_frame_indexer][5] = '\0';

							arq_mod->crc_code_ext_calc[arq_mod->arq_rx_frame_indexer][0] = data_tx_rx_mod->crc_var_ext;
							arq_mod->crc_code_ext_calc[arq_mod->arq_rx_frame_indexer][1] = data_tx_rx_mod->crc_var_calc;

							//------------------------------------
							// This is repeat code of above to
							// properly examine what is actually
							// going on.
							//------------------------------------
							memcpy((void*)data_buffers->debug[arq_mod->arq_rx_frame_indexer], (const void*)data_buffers->data_tx_rx_frame, ARQ_BYTES_PER_FRAME);
							//memcpy((void*)data_buffers->data_text_rx[arq_mod->arq_rx_frame_indexer - 1][5], (const void*) '\0', 1);
							data_buffers->debug[arq_mod->arq_rx_frame_indexer][5] = '\0';

							jak_counter();
						}
					}
				}

			}



			break;

		case ARQ_RX_ACK:
			//-----------------------------------------------------------
			// ARQ_RX_ACK:
			// Transmitter is receiving an acknowledgement.
			//-----------------------------------------------------------

			//-----------------------------------------------
			// WAIT to receive ACK from receiver
			//-----------------------------------------------
			// If we wait ARQ_RX_ACK_WAIT_PERIOD and no answer we transmit the data another time
			// If no ack we do this 5 times and then we terminate transmission.
			// ----------------------------------------------
			// LED blinks to indicate no transmission  
			//-----------------------------------------------


#ifdef REPEAT_TRANSMIT
			if (data_tx_rx_mod->rx_demod_sync) {
				arq_mod->rx_ack_received = 1;
			}
#else

			if (data_tx_rx_mod->rx_demod_sync) {
				if (data_tx_rx_mod->bit_err_det == 0) {
					data_tx_rx_mod->no_bits_detected++;
				}

				arq_mod->rx_ack_received = 1;

				// Print Contents of the frames received
				data_tx_rx_mod->debug_data_f_ptr(data_buffers, arq_mod);

				//if (!data_tx_rx_mod->bit_err_det){
				//	//--------------------------------------
				//	// A successful ack frame was received
				//	//--------------------------------------
				//	arq_mod->arq_ack_frame_indexer = data_buffers->data_tx_rx_frame[5];
				//

				//	//============================================================================
				//	// DECISION TREE -------------------
				//	//============================================================================					
				//	//arq_mod->rx_ack_received = 1;
				//}

			}
#endif // REPEAT_TRANSMIT


			break;

			//=====================================================================================================
			// TX STATES ------------------------------------------------------------------------------------------
			//=====================================================================================================

		case ARQ_TX_DATA:
			//-----------------------------------------------------------
			// ARQ_TX_DATA:
			// Transmitter is sending data 
			//-----------------------------------------------------------

			//-----------------------------------------------
			// This just sets input pointers to buffers
			// for each data_tx_stage ....
			//-----------------------------------------------
			sync_data_eot_comb_logic(data_buffers, data_tx_rx_mod);

			//----------------------------------------------------
			// Here we handle the frame index sent to the receiver
			//----------------------------------------------------

			if ((data_tx_rx_mod->data_tx_stage == SYNCING)) {
				arq_mod->arq_tx_frame_indexer = arq_mod->arq_total_session_frames;
			}

			else if (data_tx_rx_mod->data_tx_stage == TRANSMIT_DATA) {
				//--------------------------------------------------------------------------------
				// Nothing to do here....
				//--------------------------------------------------------------------------------
				if (!arq_mod->sent_first_data_packet) {

					// This sets the tx_frame indexer for the first frame to be sent.
					// The value of 'arq_tx_frame_indexer' is inserted directly into the frame

					arq_mod->arq_tx_frame_indexer = arq_mod->arq_ack_frame_indexer;
					arq_mod->sent_first_data_packet = 1;
				}

			}

			else if ((data_tx_rx_mod->data_tx_stage == END_OF_TXING)) {
				arq_mod->arq_tx_frame_indexer = 0xFF;
			}

			break;

		case ARQ_TX_ACK:
			//-----------------------------------------------------------
			// ARQ_TX_ACK:
			// Receiver is sending back an acknowledge 
			//-----------------------------------------------------------	

			//----------------------
			// Transmit ACK packets
			//----------------------
			// It's a repeat transmit to make sure that at least one was received without errors
			// So after enough acknowledgements have been sent
			//-----------------------------------------------------------------------------------

			data_tx_rx_mod->modulator_input = data_buffers->data_text_ack;
			//data_tx_rx_mod->char_num = sizeof(data_buffers->data_text_ack);

			break;

			//=====================================================================================================
			// WAIT / ARQ TX/RX SESSION COMPLETE / ERROR ----------------------------------------------------------
			//=====================================================================================================

		case ARQ_WAIT:
			//----------------------------------
			// Nothing To do but wait
			//----------------------------------

			break;

		case ARQ_TX_RX_COMPLETE:
			//-----------------------------------------------------------
			// ARQ_TX_RX_COMPLETE:
			// Nothing To do
			//-----------------------------------------------------------


			break;

		case ARQ_ERROR:

			//-----------------------------------------------------------
			// ARQ_ERROR:
			// Nothing To do
			//-----------------------------------------------------------


		default:
			break;
		}
		//-----------------------------
		//
		//	END OF ARQ STATE MACHINE
		//
		//-----------------------------


		break;
		// END DATA_ARQ -----------------------------------
		// ------------------------------------------

#endif // ARQ_ENABLED
		//===================================
		//
		//	END OF ARQ STATE MACHINE
		//
		//===================================

		//=====================================================================================

		//===================================
		//
		//	EXTRA DATA TX/RX STATE/S 
		//
		//===================================

	case DATA_UART:

		//------------------------------
		// Transmitting Data  -------
		//------------------------------

		sync_data_eot_comb_logic(data_buffers, data_tx_rx_mod);

		break;
		// END DATA_UART -----------------------------------
		// ------------------------------------------

	case DV_TEST:


		break;

	case DATA_TEST:
		data_tx_rx_mod->uchar_ptr = data_buffers->data_text_0;
		data_tx_rx_mod->modulator_input = data_buffers->data_text_0 + data_tx_rx_mod->data_offset;
		//data_tx_rx_mod->tx_uart_data_index = sizeof(data_buffers->data_text_0);
		//data_tx_rx_mod->char_num = sizeof(data_buffers->data_text_0);
		data_tx_rx_mod->tx_uart_data_index = 100;
		data_tx_rx_mod->char_num = 100;
		break;

	default:
		break;
	}


}


/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  sync_data_eot_comb_logic
AUTHOR:
DESC  : Set up buffers and limits to transmit

SYNC : Establishing connection with RECEIVER
DATA : Sending data to RECEIVER
EOT  : Closing connection with RECEIVER
------------------------------------------------------------------------
----------------------------------------------------------------------*/

void sync_data_eot_comb_logic(struct data_struct * data_buffers, struct data_tx_rx * data_tx_rx_mod) {

	//---------------------------------
	// Txing Data --------------------
	//---------------------------------
	if (data_tx_rx_mod->not_rxing_txing) {   // Transmitting

#ifdef EMBEDDED
		led_tx_data_control(data_tx_rx_mod);
#endif //EMBEDDED	
		//--------------------------------
		// SYNC PHASE
		//--------------------------------
		if ((data_tx_rx_mod->data_tx_stage == SYNCING)) {
			data_tx_rx_mod->modulator_input = data_buffers->data_text_sync;
		}
		//--------------------------------
		// DATA PHASE
		//--------------------------------
		else if ((data_tx_rx_mod->data_tx_stage == TRANSMIT_DATA)) {

			data_tx_rx_mod->uchar_ptr = (unsigned char *)data_buffers->data_from_uart;
			data_tx_rx_mod->modulator_input = data_tx_rx_mod->uchar_ptr + data_tx_rx_mod->data_offset;
		}
		//--------------------------------
		// EOT PHASE
		//--------------------------------
		else if ((data_tx_rx_mod->data_tx_stage == END_OF_TXING)) {

			data_tx_rx_mod->modulator_input = data_buffers->data_text_eot;

		}
	}
	//---------------------------------
	// Rxing Data --------------------
	//---------------------------------
	else {  // Receiving

		if (((strcmp((const char*)data_buffers->data_tx_rx_frame, "\0")) != 0)) {

			if ((strcmp((const char*)data_buffers->data_tx_rx_frame, "STATx_")) == 0) {
				data_tx_rx_mod->rx_data_accept = 1;
			}

			if (data_tx_rx_mod->rx_data_accept && ((strcmp((const char*)data_buffers->data_tx_rx_frame, "STATx_")) != 0)) {
				// Jak Edit: 1001 25/11/18
				//memcpy((char*)data_buffers->data_to_uart[data_tx_rx_mod->rx_uart_data_index], (const char*)data_buffers->data_tx_rx_frame, UART_BYTES_PER_FRAME);
				data_tx_rx_mod->rx_uart_data_index += UART_BYTES_PER_FRAME;
			}

			if ((strcmp((const char*)data_buffers->data_tx_rx_frame, "ENDTx_")) == 0) {
				data_tx_rx_mod->rx_data_accept = 0;
			}
		}
		//-------------------------------------------------------------------
		// Clear Buffer
		//-------------------------------------------------------------------
		// Remember to clear buffer outside of function then!!!!
		// Ensure that value is copied whenever but that the buffer is cleared !!
		//--------------------------------------------------------------------
		// strcpy((char*)data_buffers->data_tx_rx_frame, "\0");
		//--------------------------------------------------------------------

	}
}


void clear_data_frame_buffer(struct data_struct * data_buffers) {

	strcpy((char*)data_buffers->data_tx_rx_frame, "\0");

}




/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  led_tx_data_control()
AUTHOR:
DESC  : Get leds to flicker differently during each stage of
transmission.
------------------------------------------------------------------------
----------------------------------------------------------------------*/

#ifdef EMBEDDED

void led_tx_data_control(struct data_tx_rx * data_tx_rx_mod) {

	//--------------------------------------
	// LED TOGGLING FOR EACH PHASE OF TXING
	//--------------------------------------
	if (data_tx_rx_mod->data_tx_stage == SYNCING) {
		led_rt(1);
		led_err(0);
	}
	else if (data_tx_rx_mod->data_tx_stage == TRANSMIT_DATA) {
		led_rt(1);
		led_err(1);
	}
	else if (data_tx_rx_mod->data_tx_stage == END_OF_TXING) {
		led_rt(0);
		led_err(1);
	}

}

#endif // EMBEDDED


/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  buffer_offset_control()
AUTHOR:
DESC  : This controls which part of the input data is sent
- The offset of input buffer
- Decrements/Increments the offset pointer of Input Buffer
- If ARQ mode, decrements the arq_tx_frame_indexer
------------------------------------------------------------------------
----------------------------------------------------------------------*/

void buffer_offset_control(struct data_struct * data_buffers, struct data_tx_rx * data_tx_rx_mod, struct arq_machine * arq_mod) {

	int buff_offset = 0;

	if (data_tx_rx_mod->data_tx_stage == TRANSMIT_DATA) {

		// Set offset value according to mode
		if (*data_tx_rx_mod->op_mode == DATA_ARQ) {
			if (arq_mod->arq_state == ARQ_TX_DATA)
			{
				//-------------------------------------------------------
				// Arq Mode sends the input buffer contents in reverse
				//--------------------------------------------------------

				buff_offset = ARQ_BYTES_PER_FRAME;

				//-------------------------------------------------
				// If resultant data_offset is greater or equal to zero
				// alter values.
				//-------------------------------------------------

				if ((data_tx_rx_mod->data_offset - buff_offset) >= 0)
				{
					//-------------------------------------------------
					// Decrement Input Buffer Offset
					//-------------------------------------------------
					data_tx_rx_mod->data_offset -= buff_offset;

					//-------------------------------------------------
					// Decrement tx_frame_indexer for Transmit Stage
					//-------------------------------------------------
					arq_mod->arq_tx_frame_indexer--;

					//----------------------------------------------------
					// Increment the number of frames sent - Value for UI
					//----------------------------------------------------
					data_tx_rx_mod->frames_sent++;
				}
				else if ((data_tx_rx_mod->data_offset - buff_offset) < 0) {
					data_tx_rx_mod->data_offset = 0;
					data_tx_rx_mod->uart_buffer_sent = 1;
				}

			}
		}

		else {
			//-------------------------------------------------------
			// Normal Mode sends data in FIFO mode - 1st to Last Byte
			//--------------------------------------------------------
			buff_offset = UART_BYTES_PER_FRAME;

			if ((data_tx_rx_mod->data_offset + buff_offset) <= data_tx_rx_mod->tx_uart_data_index)
			{
				data_tx_rx_mod->data_offset += buff_offset;
			}
			else {
				data_tx_rx_mod->data_offset = 0;
				if ((data_tx_rx_mod->uchar_ptr == data_buffers->data_from_uart))
					data_tx_rx_mod->uart_buffer_sent = 1;
			}

		}

	}
	//-------------------------------------------

}

//-------------------------------------------------
// To Determine the position of latest
// contiguous frame that was successfully received.
//-------------------------------------------------													

void post_receive_data_transfer_processing(struct data_struct * data_buffers, struct arq_machine * arq_mod) {

	short counter = 0;
	short loop_counter = 0;
	char empty_frame = 0;

	//-----------------------------------------------------------------------
	// If The 1st Expected Frame was successfully received
	// then expected number of frames for transmission is known
	//
	// Hence we know how many of the frames have been successfully received
	// Can send back acknowledge frame with frame number of latest contiguous
	// frame received
	//-----------------------------------------------------------------------
	if (arq_mod->start_frame_received) {
		arq_mod->rxed_frames_counter = 0;
		//---------------------------------
		// Loop 
		//--------------------------------
		loop_counter = arq_mod->arq_total_session_frames - 1;
		for (counter = loop_counter; loop_counter >= 0; loop_counter--) {

			//--------------------------------------------
			// At first data frame received that is empty
			// exit loop
			//--------------------------------------------

			if (!empty_frame) {
				counter--;
			}

			if ((((const char *)data_buffers->data_text_rx[loop_counter][0]) == 0)) {
				if (!empty_frame) {
					empty_frame = 1;
				}
			}
			else {
				arq_mod->rxed_frames_counter++;
			}
		}
		//----------------------------------------------------
		// Setting Up Ratio of completion
		//----------------------------------------------------
		arq_mod->prv_ratio_complete = arq_mod->ratio_complete;
		arq_mod->ratio_complete = (((float)arq_mod->rxed_frames_counter) / ((float)arq_mod->arq_total_session_frames)) * ((float)100);

		//----------------------------------------------------------------------------
		// Since counter represents first frame found empty in matrix... then the one
		// before is the last successfully received frame
		//----------------------------------------------------------------------------
		//----------------------------------------------
		// The first xpected frame was not well received...
		// Hence asking txer for a full retransmission
		//-----------------------------------------------
		if (counter == arq_mod->arq_total_session_frames - 1) {
			arq_mod->arq_ack_frame_indexer = 0xFF;
		}
		//-------------------------------------------------
		// Frame with errors was not first frame of file
		// So the frame before can be acknowledged
		//-------------------------------------------------
		else {

			arq_mod->arq_ack_frame_indexer = counter + 1;

			if (arq_mod->transmissions == 1) {
				arq_mod->prv_arq_ack_frame_indexer = arq_mod->arq_ack_frame_indexer;
			}
			else {
				if (arq_mod->prv_arq_ack_frame_indexer < arq_mod->arq_ack_frame_indexer) {
					while (1) {
						;  //ERROR OCCURRED !!
					}
				}
				else {
					arq_mod->prv_arq_ack_frame_indexer = arq_mod->arq_ack_frame_indexer;
				}

			}
		}
	}
	//------------------------------------------------------------------------
	// No error-free sync frames received...
	// Sending ACK number of 0xFF to request retransmission of whole file!
	//------------------------------------------------------------------------
	else {
		arq_mod->arq_ack_frame_indexer = 0xFF;
	}
	jak_counter();
}



//===================================================================
// Special Functions for ACK processing
//---------------------------------------
//===================================================================

/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  output_rx_ack_frame_number()
AUTHOR:
DESC  : Capture acks...
------------------------------------------------------------------------
----------------------------------------------------------------------*/


void output_rx_ack_frame_number(struct data_struct * bffrs, struct arq_machine * arq_mod) {



	//arq_mod->arq_ack_frame_indexer = arq_mod->rx_ack_frame_index_sum / arq_mod->ack_frames_rxed;



	//int index_no = 0;

	//for (int i = 0; i < ACK_MATRIX_SIZE; i++) {

	//	if (i == 0) {
	//		index_no = 0;
	//	}
	//	else {

	//		if (bffrs->ack_received[i][1] > bffrs->ack_received[index_no][1]) {

	//			index_no = i;

	//		}
	//	}
	//}
	////---------------------------------------------------------------------------------
	//// Here the value represents till where the receiver has received without errors
	////---------------------------------------------------------------------------------
	//arq_mod->arq_ack_frame_indexer = bffrs->ack_received[index_no][0];
	//volatile int i;
	//i++;
}

/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  init_bffrs_acks_matrix()
AUTHOR:
DESC  :
------------------------------------------------------------------------
----------------------------------------------------------------------*/

void init_bffrs_acks_matrix(struct data_struct * bffrs) {

	for (int i = 0; i < ACK_MATRIX_SIZE; i++) {
		bffrs->ack_received[i][0] = 0xFFFF;
		bffrs->ack_received[i][1] = 0xFFFF;
	}

}



/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  smart_ack_processing()
AUTHOR:
DESC  :
The purpose of this function is due to the unfortunate circumstance of
receiving acks from the SM1000.

When a data_ack was attempted it was found that whilst yes a sync signal
being observed, using the same code used by the FreeDV, the SM1000 was
not receiving any frame without errors in a period of a number of
seconds.
------------------------------------------------------------------------
----------------------------------------------------------------------*/


void smart_ack_processing(struct data_struct * bffrs, struct data_tx_rx * data_tx_rx_mod) {

	//--------------------------------------------
	// Check if frame matches any others...
	// If so add counter of matching frame number.
	//--------------------------------------------

	for (int i = 0; i < ACK_MATRIX_SIZE; i++) {

		//---------------------------------------------------
		// If empty then fill...
		//---------------------------------------------------

		if (bffrs->ack_received[i][0] == 0xFFFF) {
			// Ack number set to that received
			bffrs->ack_received[i][0] = bffrs->data_tx_rx_frame[0];

			// Counter set to 1
			bffrs->ack_received[i][1] = 1;
			// Ack handled - Increment occurred
			data_tx_rx_mod->ack_incr_occured = 1;
			// Exit Loop
			break;
		}

		//---------------------------------------------------
		// Ifmatches then increment...
		//---------------------------------------------------		
		if (bffrs->data_tx_rx_frame[0] == bffrs->ack_received[i][0]) {
			// Increment the counter
			bffrs->ack_received[i][1]++;
			// Increment occurred
			data_tx_rx_mod->ack_incr_occured = 1;
			// Exit Loop
			break;
		}
	}
	//--------------------------------------------------------------------------------
	// If no increment occurred then the ack number of this last ack frame 
	// needs to be swapped with the frame number in the matrix with the lowest count
	//--------------------------------------------------------------------------------
	if (!data_tx_rx_mod->ack_incr_occured)
	{
		int index_no = 0;

		// Find ack number withs smallest number of acks received

		for (int i = 0; i < ACK_MATRIX_SIZE; i++) {
			// If starting search, assume first frame has smallest number
			if (i == 0) {
				index_no = 0;
			}
			else {

				if (bffrs->ack_received[i][1] < bffrs->ack_received[index_no][1]) {

					index_no = i;
				}
			}
		}

		bffrs->ack_received[index_no][0] = bffrs->data_tx_rx_frame[0];
		bffrs->ack_received[index_no][1] = 1;

	}
	else {
		// Reset Flag and proceed as normal
		data_tx_rx_mod->ack_incr_occured = 0;
	}
}


/*----------------------------------------------------------------------
------------------------------------------------------------------------
NAME  :  activate_ptt(uint8_t flag)
AUTHOR:
DESC  : Turn on/off Rig Transmit
------------------------------------------------------------------------
----------------------------------------------------------------------*/


void activate_ptt(uint8_t flag, struct data_tx_rx * data_tx_rx_mod) {
	if (flag) {

		data_tx_rx_mod->not_rxing_txing = 1;
#ifdef EMBEDDED
		not_cptt(0);
		*data_tx_rx_mod->core_state = STATE_TX;
#else
		g_tx = 1;
		frame_ptr->m_btnTogPTT->SetValue(true);
		frame_ptr->togglePTT();
#endif

	}
	else {

		data_tx_rx_mod->not_rxing_txing = 0;
#ifdef EMBEDDED
		not_cptt(1);
		*data_tx_rx_mod->core_state = STATE_RX;
#else
		g_tx = 0;
		frame_ptr->m_btnTogPTT->SetValue(false);
		frame_ptr->togglePTT();
#endif
	}

}


/*------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
NAME  :  examine_data_received()
AUTHOR:
DESC  : Examine contents of the data_rx_buffer for Nemiver
--------------------------------------------------------------------------------------
------------------------------------------------------------------------------------*/

void examine_data_received(struct data_struct * data_buff, unsigned char u_limit) {

	for (int i = u_limit - DEBUG_NUM, j = 0; i < u_limit; j++, i++) {
		memcpy(data_buff->debug[j], data_buff->data_text_rx[i], 6);
	}

	jak_counter();

}


/*------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
NAME  :  arq_machine_reset()
AUTHOR:
DESC  : Reset the arq_machine
--------------------------------------------------------------------------------------
------------------------------------------------------------------------------------*/

void arq_machine_reset(struct arq_machine * arq_mod) {

	arq_mod->transmissions = 0;

	//------------------------------------
	// INDEXERS
	//------------------------------------

	arq_mod->arq_total_session_frames = 0;
	arq_mod->arq_tx_frame_indexer = 0;
	arq_mod->arq_rx_frame_indexer = 0;
	arq_mod->arq_ack_frame_indexer = 0;

	//---------------------------------------
	// ERROR VARs
	//---------------------------------------
	arq_mod->arq_error_code = 0;

	/*====================================================================

	FLAGS

	====================================================================*/

	//---------------------------------------
	// ARQ FLAGS
	//---------------------------------------
	arq_mod->data_tx_completed = 0;
	arq_mod->data_session = 0;
	arq_mod->data_transfer = 0;

	arq_mod->tx_err_sent = 0;
	arq_mod->tx_error = 0;
	arq_mod->sent_first_data_packet = 0;

	arq_mod->rx_ack_received = 0;

	arq_mod->error_occurred = 0;
	arq_mod->start_frame_received = 0;  // Indicates if the start frame was received


}

void clear_buffer(unsigned char * arr) {
	for (int i = 0; i < MAX_UART_BUFF_SIZE; i++) {
		arr[i] = '\0';
	}
}

#endif //DEBUG_DEFS_FREEDV




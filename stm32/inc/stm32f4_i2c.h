/* 1520 2018/09/22  */

/* Hey there!

This is a header supplemented to the codec2 project for enabling data transmission.

This header declares the functions to be used later on.

*/
#include "stm32f4xx.h"
#include "blu5_defines.h"
#ifndef __STM32F4_I2C__
#define __STM32F4_I2C__



//----------------------------
// I2C FUNCTIONS
//----------------------------

void i2c_configure();
void configure_digipots();
volatile uint8_t check_sending();

void rig_digi_write_protect(uint8_t state);

//--------------------------------------

#endif
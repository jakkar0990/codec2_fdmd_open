/* 1520 2018/09/22  */

/* Hey there!

This is a header supplemented to the codec2 project for enabling data transmission.

This header declares the functions to be used later on.

*/
#include "stm32f4xx.h"
#include "blu5_defines.h"
#include "blu5_struct_defines.h"

#ifndef __STM32F4_UART__
#define __STM32F4_UART__



//----------------------------
// UART FUNCTIONS
//----------------------------

extern int output_data_received_to_uart(struct data_struct * data_buffers, struct arq_machine * arq_mod);
extern void debug_to_uart(struct data_struct * data_buffers, struct arq_machine * arq_mod);

extern void uart_putnum(USART_TypeDef* USARTx,short num);
extern void test_uart_putnum(USART_TypeDef* USARTx);


extern void clear_buffer(unsigned char *);
extern uint8_t size_of_buffer(unsigned char *);
extern uint8_t uart_menu();
extern void baud_rate_corr();
extern void uart_setup();     // To setup UART3 for the sm1000
extern void UU_PutChar(USART_TypeDef* USARTx, uint8_t ch);
extern void UU_PutString(USART_TypeDef* USARTx, uint8_t * str);

extern volatile uint8_t uart_start_stop;


//-------------------------------
//-------------------------------

extern uint8_t usart_perr_counter;
extern uint8_t usart_err_counter;

//--------------------------------------

//-------------------------------------
//  SM1000 variables
//-------------------------------------

extern uint8_t data_transmission;          // Flag for signalling beging and end of data tx
//extern volatile unsigned char data_from_uart[MAX_UART_BUFF_SIZE];

extern struct data_struct buffers;
extern struct data_tx_rx data_tx_rx_1; 

extern volatile uint8_t data_tx_stage;
extern uint16_t tx_uart_data_index;

//--------------------------------------

#endif
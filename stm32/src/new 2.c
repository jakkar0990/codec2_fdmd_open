int jak_data_fdmdv_demod_1600(struct freedv *f, unsigned char* speech_out, short demod_in[],int* valid, int* off2)
{
	int                 bits_per_codec_frame, bytes_per_codec_frame, bits_per_fdmdv_frame;
	int                 i, j, bit, byte, nin_prev, nout;
	int                 recd_codeword, codeword1, data_flag_index, n_ascii;
	short               abit[1];
	char                ascii_out;
	int                 reliable_sync_bit;
	///////////////////////////////////
	// Previous Code for mode 1600
	///////////////////////////////////
	int nin = freedv_nin(f);

	COMP rx_fdm[f->n_max_modem_samples];

	for (i = 0; i<nin; i++) {
		rx_fdm[i].real = (float)demod_in[i];
		rx_fdm[i].imag = 0.0;
	}
	////////////////////////////////////////////
	bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
	bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
	nout = f->n_speech_samples;

//	COMP ademod_in[f->nin];
//	for (i = 0; i<f->nin; i++)
//		ademod_in[i] = fcmult(1.0, rx_fdm[i]);
	//ademod_in[i] = fcmult(1.0 / FDMDV_SCALE, rx_fdm[i]);

	bits_per_fdmdv_frame = fdmdv_bits_per_frame(f->fdmdv);

	nin_prev = f->nin;
	fdmdv_demod(f->fdmdv, f->fdmdv_bits, &reliable_sync_bit, rx_fdm, &f->nin);
	fdmdv_get_demod_stats(f->fdmdv, &f->stats);
	f->sync = f->fdmdv->sync;
	f->snr_est = f->stats.snr_est;

	if (f->evenframe == 0) {
		memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
		nout = 0;
	}

	if (reliable_sync_bit == 1) {
		f->evenframe = 1;
	}

	if (f->stats.sync) {
		if (f->evenframe == 0) {
			memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
			nout = 0;
			*valid = 0;
		}
		else {
			memcpy(&f->rx_bits[bits_per_fdmdv_frame], f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
			// pack bits, MSB received first
			bit = 7;
			byte = 0;
			memset(f->packed_codec_bits, 0, bytes_per_codec_frame);
			for (i = 0; i<bits_per_codec_frame; i++) {
				speech_out[byte] = 0;
				f->packed_codec_bits[byte] |= (f->rx_bits[i] << bit);
				speech_out[byte + *(off2)] |= (f->rx_bits[i] << bit);
				bit--;
				if (bit < 0) {
					bit = 7;
					byte++;
				}
			}
				(*off2) += bytes_per_codec_frame;
				*valid = 1;

			/* squelch if beneath SNR threshold or test frames enabled */

			if ((f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) || f->test_frames) {
				//fprintf(stderr,"squelch %f %f !\n", f->stats.snr_est, f->snr_squelch_thresh);
				*valid = 0;
			}

			nout = f->n_speech_samples;

		}

		/* note this freewheels if reliable sync dissapears on bad channels */

		if (f->evenframe)
			f->evenframe = 0;
		else
			f->evenframe = 1;
		//fprintf(stderr,"%d\n",  f->evenframe);

	} /* if (sync) .... */
	else {
		/* if not in sync pass through analog samples */
		/* this lets us "hear" whats going on, e.g. during tuning */

		//fprintf(stderr, "out of sync\n");

		if (f->squelch_en == 0) {
			*valid = -1;
		}
		else {
			*valid = 0;
		}
		//fprintf(stderr, "%d %d %d\n", nin_prev, speech_out[0], speech_out[nin_prev-1]);
		nout = nin_prev;
	}
	return nout;
}


//// complex input samples version

//
// Actual Demodulator Function?
// 
int jak_origmod_data_fdmdv_demod_1600(struct freedv *f, unsigned char* speech_out, short demod_in[],int* valid,int mod_out_lim , int* off2){
	int                 bits_per_codec_frame, bytes_per_codec_frame, bits_per_fdmdv_frame;
	int                 i, j, bit, byte, nin_prev, nout;
	int                 recd_codeword, codeword1, data_flag_index, n_ascii;
	short               abit[1];
	char                ascii_out;
	int                 reliable_sync_bit;
	///////////////////////////////////
	// Previous Code for mode 1600
	///////////////////////////////////
	int nin = freedv_nin(f);

	COMP rx_fdm[f->n_max_modem_samples];

	for (i = 0; i<nin; i++) {
		rx_fdm[i].real = (float)demod_in[i];
		rx_fdm[i].imag = 0.0;
	}
	////////////////////////////////////////////
	
	/////////////////////////////////////////
	// Altering address for f->packed_bits
	/////////////////////////////////////////
	int value_off2 = (*off2);
	unsigned char * u;
	u = &speech_out[*off2];
	f->packed_codec_bits = u;
	unsigned char * y = (f->packed_codec_bits);
	// Test Assignment to Mod_out
	//	strcpy(y,"HEY THERE");
	if(*off2 < mod_out_lim)
		(*off2) += 7;
	else
		(*off2) = 0;
	//////////////////////////////////////////
	//////////////////////////////////////////
	
	bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
	bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
	nout = f->n_speech_samples;

	COMP ademod_in[f->nin];
	for (i = 0; i<f->nin; i++){
	// Jak Edit: demod_in converted to rx_fdm
		ademod_in[i] = fcmult(1.0 / FDMDV_SCALE, rx_fdm[i]);
	// ---------------------------------------------------
	}
	bits_per_fdmdv_frame = fdmdv_bits_per_frame(f->fdmdv);

	nin_prev = f->nin;
	fdmdv_demod(f->fdmdv, f->fdmdv_bits, &reliable_sync_bit, ademod_in, &f->nin);
	fdmdv_get_demod_stats(f->fdmdv, &f->stats);
	f->sync = f->fdmdv->sync;
	f->snr_est = f->stats.snr_est;

	if (reliable_sync_bit == 1) {
		f->evenframe = 1;
	}

	if (f->stats.sync) {
		if (f->evenframe == 0) {
			memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
			nout = 0;
			*valid = 0;
		}
		else {
			memcpy(&f->rx_bits[bits_per_fdmdv_frame], f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));

			if (f->test_frames == 0) {
				// This is actual decoding for FreeDV data
				strcpy(f->packed_codec_bits,"->");
				recd_codeword = 0;
				for (i = 0; i<8; i++) {
					recd_codeword <<= 1;
					recd_codeword |= (f->rx_bits[i] & 0x1);
				}
				for (i = 11; i<15; i++) {
					recd_codeword <<= 1;
					recd_codeword |= (f->rx_bits[i] & 0x1);
				}
				for (i = bits_per_codec_frame; i<bits_per_codec_frame + 11; i++) {
					recd_codeword <<= 1;
					recd_codeword |= (f->rx_bits[i] & 0x1);
				}
				codeword1 = golay23_decode(recd_codeword);
				f->total_bit_errors += golay23_count_errors(recd_codeword, codeword1);
				f->total_bits += 23;

				//codeword1 = recd_codeword;
				//fprintf(stderr, "received codeword1: 0x%x  decoded codeword1: 0x%x\n", recd_codeword, codeword1);

				for (i = 0; i<bits_per_codec_frame; i++)
					f->codec_bits[i] = f->rx_bits[i];

				for (i = 0; i<8; i++) {
					f->codec_bits[i] = (codeword1 >> (22 - i)) & 0x1;
				}
				for (i = 8, j = 11; i<12; i++, j++) {
					f->codec_bits[j] = (codeword1 >> (22 - i)) & 0x1;
				}

				// extract txt msg data bit ------------------------------------------------------------

				data_flag_index = codec2_get_spare_bit_index(f->codec2);
				abit[0] = f->codec_bits[data_flag_index];

				n_ascii = varicode_decode(&f->varicode_dec_states, &ascii_out, abit, 1, 1);
				if (n_ascii && (f->freedv_put_next_rx_char != NULL)) {
					(*f->freedv_put_next_rx_char)(f->callback_state, ascii_out);
				}

				// reconstruct missing bit we steal for data bit and decode speech

				codec2_rebuild_spare_bit(f->codec2, f->codec_bits);

				// pack bits, MSB received first

				bit = 7;
				byte = 0;
				memset(f->packed_codec_bits, 0, bytes_per_codec_frame);
				for (i = 0; i<bits_per_codec_frame; i++) {
					f->packed_codec_bits[byte] |= (f->codec_bits[i] << bit);
					bit--;
					if (bit < 0) {
						bit = 7;
						byte++;
					}
				}
				*valid = 1;
			}
			else {
				strcpy(f->packed_codec_bits, "*");
				int   test_frame_sync, bit_errors, ntest_bits, k;
				short error_pattern[fdmdv_error_pattern_size(f->fdmdv)];

				for (k = 0; k<2; k++) {
					/* test frames, so lets sync up to the test frames and count any errors */

					fdmdv_put_test_bits(f->fdmdv, &test_frame_sync, error_pattern, &bit_errors, &ntest_bits, &f->rx_bits[k*bits_per_fdmdv_frame]);

					if (test_frame_sync == 1) {
						f->test_frame_sync_state = 1;
						f->test_frame_count = 0;
					}

					if (f->test_frame_sync_state) {
						if (f->test_frame_count == 0) {
							f->total_bit_errors += bit_errors;
							f->total_bits += ntest_bits;
							if (f->freedv_put_error_pattern != NULL) {
								(*f->freedv_put_error_pattern)(f->error_pattern_callback_state, error_pattern, fdmdv_error_pattern_size(f->fdmdv));
							}
						}
						f->test_frame_count++;
						if (f->test_frame_count == 4)
							f->test_frame_count = 0;
					}

					//fprintf(stderr, "test_frame_sync: %d test_frame_sync_state: %d bit_errors: %d ntest_bits: %d\n",
					//        test_frame_sync, f->test_frame_sync_state, bit_errors, ntest_bits);
				}
			}


			/* squelch if beneath SNR threshold or test frames enabled */

			if ((f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) || f->test_frames) {
				//fprintf(stderr,"squelch %f %f !\n", f->stats.snr_est, f->snr_squelch_thresh);
				*valid = 0;
			}

			nout = f->n_speech_samples;

		}

		/* note this freewheels if reliable sync dissapears on bad channels */

		if (f->evenframe)
			f->evenframe = 0;
		else
			f->evenframe = 1;
		//fprintf(stderr,"%d\n",  f->evenframe);
	} /* if (sync) .... */
	else {
		/* if not in sync pass through analog samples */
		/* this lets us "hear" whats going on, e.g. during tuning */

		//fprintf(stderr, "out of sync\n");

		if (f->squelch_en == 0) {
			*valid = -1;
		}
		else {
			*valid = 0;
		}
		//fprintf(stderr, "%d %d %d\n", nin_prev, speech_out[0], speech_out[nin_prev-1]);
		nout = nin_prev;
	}
	return nout;
}
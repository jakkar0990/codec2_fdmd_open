int jak_data_fdmdv_demod_1600(struct freedv *f, unsigned char* speech_out, short demod_in[],int* valid, int* off2)
{
	int                 bits_per_codec_frame, bytes_per_codec_frame, bits_per_fdmdv_frame;
	int                 i, j, bit, byte, nin_prev, nout;
	int                 recd_codeword, codeword1, data_flag_index, n_ascii;
	short               abit[1];
	char                ascii_out;
	int                 reliable_sync_bit;
	///////////////////////////////////
	// Previous Code for mode 1600
	///////////////////////////////////
	int nin = freedv_nin(f);

	COMP rx_fdm[f->n_max_modem_samples];

	for (i = 0; i<nin; i++) {
		rx_fdm[i].real = (float)demod_in[i];
		rx_fdm[i].imag = 0.0;
	}
	////////////////////////////////////////////
	bits_per_codec_frame = codec2_bits_per_frame(f->codec2);
	bytes_per_codec_frame = (bits_per_codec_frame + 7) / 8;
	nout = f->n_speech_samples;

//	COMP ademod_in[f->nin];
//	for (i = 0; i<f->nin; i++)
//		ademod_in[i] = fcmult(1.0, rx_fdm[i]);
	//ademod_in[i] = fcmult(1.0 / FDMDV_SCALE, rx_fdm[i]);

	bits_per_fdmdv_frame = fdmdv_bits_per_frame(f->fdmdv);

	nin_prev = f->nin;
	fdmdv_demod(f->fdmdv, f->fdmdv_bits, &reliable_sync_bit, rx_fdm, &f->nin);
	fdmdv_get_demod_stats(f->fdmdv, &f->stats);
	f->sync = f->fdmdv->sync;
	f->snr_est = f->stats.snr_est;

	if (f->evenframe == 0) {
		memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
		nout = 0;
	}

	if (reliable_sync_bit == 1) {
		f->evenframe = 1;
	}

	if (f->stats.sync) {
		if (f->evenframe == 0) {
			memcpy(f->rx_bits, f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
			nout = 0;
			*valid = 0;
		}
		else {
			memcpy(&f->rx_bits[bits_per_fdmdv_frame], f->fdmdv_bits, bits_per_fdmdv_frame*sizeof(int));
			// pack bits, MSB received first
			bit = 7;
			byte = 0;
			memset(f->packed_codec_bits, 0, bytes_per_codec_frame);
			for (i = 0; i<bits_per_codec_frame; i++) {
				speech_out[byte] = 0;
				f->packed_codec_bits[byte] |= (f->rx_bits[i] << bit);
				speech_out[byte + *(off2)] |= (f->rx_bits[i] << bit);
				bit--;
				if (bit < 0) {
					bit = 7;
					byte++;
				}
			}
				(*off2) += bytes_per_codec_frame;
				*valid = 1;

			/* squelch if beneath SNR threshold or test frames enabled */

			if ((f->squelch_en && (f->stats.snr_est < f->snr_squelch_thresh)) || f->test_frames) {
				//fprintf(stderr,"squelch %f %f !\n", f->stats.snr_est, f->snr_squelch_thresh);
				*valid = 0;
			}

			nout = f->n_speech_samples;

		}

		/* note this freewheels if reliable sync dissapears on bad channels */

		if (f->evenframe)
			f->evenframe = 0;
		else
			f->evenframe = 1;
		//fprintf(stderr,"%d\n",  f->evenframe);

	} /* if (sync) .... */
	else {
		/* if not in sync pass through analog samples */
		/* this lets us "hear" whats going on, e.g. during tuning */

		//fprintf(stderr, "out of sync\n");

		if (f->squelch_en == 0) {
			*valid = -1;
		}
		else {
			*valid = 0;
		}
		//fprintf(stderr, "%d %d %d\n", nin_prev, speech_out[0], speech_out[nin_prev-1]);
		nout = nin_prev;
	}
	return nout;
}


//// complex input samples version
/*---------------------------------------------------------------------------*\

  FILE........: stm32f4_i2c.c
  AUTHOR......: 
  DATE CREATED: 

  UART Driver for enabling data tx/rx between embedded and PC.

\*---------------------------------------------------------------------------*/

/*
  Copyright (C) 2013 David Rowe

  All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2.1, as
  published by the Free Software Foundation.  This program is
  distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f4xx.h"
#include "stm32f4_i2c.h"
#include <ctype.h>

#include "stm32f4xx_i2c.h"
#include "blu5_defines.h"
#include "freedv_api.h"

//------------------------------------------------
// Digipot addresses - 8bit format(just omit MSB)
//------------------------------------------------

//#define RIG_DIGIPOT 0b 0010 1101
//#define EXT_SPKR		0b 0010 1111
//#define EXT_MIC			0b 0010 1100

// SHIFT LEFT BY 1 FOR TXING

//#define RIG_DIGIPOT 0b 0101 1010 -> 0x5A
//#define EXT_SPKR    0b 0101 1110 -> 0x5E
//#define EXT_MIC     0b 0101 1000 -> 0x58



//=====================================
// I2C EVENT COUNTERs
//=====================================
#define ADDRESS_SENT 0
#define INSTR_SENT   1
#define DATA_SENT    2


volatile uint8_t digipot_config_phase = 0;
volatile uint8_t address_sent = 0;
volatile uint8_t instr_sent = 0;
volatile uint8_t data_sent = 0;
volatile uint8_t start_sent = 0;
volatile uint32_t delay_counter = 0;

//-------------------------------------
// Bit MASKs for Bits
//-------------------------------------

#define ADDR_BIT_SET 

//=====================================
// AD5241 - EXT_MIC Data Transfers
//=====================================

#define EXT_MIC_SLAVE_ADDRESS 0x58 // 

#define EXT_MIC_ADD 			 0x58
#define EXT_MIC_INST 			 0x00
#define EXT_MIC_DATA  		 0xFF

//=====================================
// AD5241 - EXT_SPKR Data Transfers
//=====================================

#define EXT_SPKR_SLAVE_ADDRESS 0x5E

#define EXT_SPKR_ADD 		0x5E
#define EXT_SPKR_INST 	0x00
#define EXT_SPKR_DATA  	0xFF

//=====================================
// AD5252 - RIG_DIGIPOT Data Transfers
//=====================================


//------------------------------------
// Rig MIC
//------------------------------------
#define RIG_DIGIPOT_SLAVE_ADDRESS 0x5A

#define RIG_SLAVE 			 0x5A
#define RIG_RDAC3 			 0x23
#define RIG_MIC_SETTING  0xFF
//#define RIG_MIC_SETTING  0x00

//------------------------------------
// Rig SPKR
//------------------------------------
#define RIG_SLAVE 			 0x5A
#define RIG_RDAC1 			 0x21
#define RIG_SPKR_SETTING 0x19
//------------------------------------


//=====================================
//=====================================

void reset_flags(){
	address_sent = 0;
	instr_sent = 0;
	data_sent = 0;
}


void I2C2_EV_IRQHandler(void)
{
	
	if( I2C_GetITStatus( I2C2, I2C_IT_SB ) ){
		start_sent = 1;
		//I2C_GenerateSTART(I2C2,DISABLE);
	}

	
	if( I2C_GetITStatus(I2C2, I2C_IT_ADDR) ){
		address_sent = 1;
	}
	
	if( I2C_GetITStatus(I2C2, I2C_IT_AF) ){
		jak_counter();
	}
	
		if( I2C_GetITStatus(I2C2, I2C_IT_BERR) ){
		jak_counter();
	}
	
	
	if( I2C_GetITStatus(I2C2, I2C_IT_BTF) ){
		if( instr_sent)
			instr_sent = 1;
		else
			data_sent = 1;
	}
	
	
	
	
//	if( I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT ) ){
//		start_sent = 1; 
//	}
//	
//	
//	
//	if( I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) ){
//		
//		instr_sent = 1;
//		
//	}
//	
//	if( I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) ){
//		
//		data_sent = 1;
//		
//	}
	
}







inline void delay_rx(){
		while( delay_counter++ < 5000);
		delay_counter = 0;
}


void i2c_start() {
    I2C2->CR1 |= I2C_CR1_START;
    while (!(I2C2->SR1 & I2C_SR1_SB));
}

int i2c_sendAddr(uint8_t addr) {
    I2C2->DR = addr;
    while (!(I2C2->SR1 & I2C_SR1_ADDR))
        if (I2C2->SR1 & I2C_SR1_AF)
            return -1;
    I2C2->SR2; //generates a read, clears ADDR
    return 0;
}

int i2c_sendByte(uint8_t data) {
    while (!(I2C2->SR1 & (I2C_SR1_TXE | I2C_SR1_AF)));
    I2C2->DR = data;
    return I2C2->SR1 & I2C_SR1_AF ? -1 : 0;
}

void i2c_stop() {
    I2C2->CR1 |= I2C_CR1_STOP;
	while ( !((I2C2->SR1 & I2C_SR1_TXE) || (I2C2->SR1 & I2C_SR1_SB) ) );
}



void configure_digipots(){
	
	uint8_t address_ack_received = 0;
	//======================================
	// AD 5241 - EXT_MIC DIGIPOT
	//======================================
	
	//------------------------------
	// Register Value Method
	//------------------------------
	//-----------------------
	//Generate Start Signal	
	//-------------------------

	
	
//		i2c_start();

//		i2c_sendAddr(EXT_MIC_ADD);
//		i2c_sendByte(EXT_MIC_INST);
//		i2c_sendByte(EXT_MIC_DATA);
		
//		i2c_sendAddr(RIG_DIGIPOT_SLAVE_ADDRESS);
//		i2c_sendByte(RIG_RDAC3);
//		i2c_sendByte(RIG_MIC_SETTING);

//		i2c_stop();
		//------------------------------
		// END OF Register Value Method
		//------------------------------

	//-----------------------------
	// SPL Method
	//-----------------------------
	rig_digi_write_protect(1);
	
	
	#ifdef I2C_REGISTER
	
	i2c_start();

//	i2c_sendAddr(EXT_MIC_ADD);
//	i2c_sendByte(EXT_MIC_INST);
//	i2c_sendByte(EXT_MIC_DATA);
	
	i2c_sendAddr(RIG_DIGIPOT_SLAVE_ADDRESS);
	i2c_sendByte(RIG_RDAC3);
	i2c_sendByte(RIG_MIC_SETTING);

	i2c_stop();
	
	#endif // I2C_Register
	
	#ifdef I2C_SPL
	
	
	I2C_GenerateSTART(I2C2,ENABLE);

//	while(I2C_GetFlagStatus(I2C2,I2C_FLAG_SB));
	while(!start_sent);
	
	//delay_rx();
	
	
//	I2C_Send7bitAddress(I2C2,EXT_MIC_SLAVE_ADDRESS,I2C_Direction_Transmitter);
	I2C_Send7bitAddress(I2C2,RIG_SLAVE,I2C_Direction_Transmitter);
//I2C_SendData(I2C2,EXT_MIC_ADD);
	
//	delay_rx();
//	while(I2C_GetFlagStatus(I2C2,I2C_FLAG_SB));
	while(!address_sent);
	// Select RDAC3 Register

//	I2C_SendData(I2C2,EXT_MIC_INST);
	I2C_SendData(I2C2,RIG_RDAC3);
//	while(I2C_GetFlagStatus(I2C2,I2C_FLAG_BTF));	
	while(!instr_sent);
	// delay_rx();
 
	
	// Writing actual Setting	
//	I2C_SendData(I2C2,EXT_MIC_DATA);
	I2C_SendData(I2C2,RIG_MIC_SETTING);
//	while(I2C_GetFlagStatus(I2C2,I2C_FLAG_BTF));		
	while(!data_sent);
	//delay_rx();
		
	// Generate Stop Signal
	I2C_GenerateSTOP(I2C2,ENABLE);	
	while(!I2C_GetFlagStatus(I2C2,I2C_FLAG_BTF));		


	reset_flags();
	#endif //I2C_SPL
	//-----------------------------
	// END OF SPL Method
	//-----------------------------

	
	
	
	rig_digi_write_protect(0);
	//-------------------------------
	// To be tried later....
	//-------------------------------
	
	
	
	
//	//======================================
//	// AD 5241 - EXT_SPKR DIGIPOT
//	//======================================
//	
//	// Generate Start Signal
//	I2C_GenerateSTART(I2C2,ENABLE);
//	
//	//Send slave address + Write Bit
//		
//	I2C_Send7bitAddress(I2C2,EXT_SPKR_SLAVE_ADDRESS,I2C_Direction_Transmitter); // Digipot Address : 010 1101 

//	do{
//		address_sent = I2C2->SR2 == 0x00000007 && I2C2->SR1 == 0x00000082;
//  }while( !address_sent );

//	// Select RDAC3 Register
//	I2C_SendData(I2C2,EXT_SPKR_INST);
//	do{
//		instr_sent = I2C2->SR2 == 0x00000007 && I2C2->SR1 == 0x00000084;
//  }while( !instr_sent );
//	
//	// Writing actual Setting	
//	I2C_SendData(I2C2,EXT_SPKR_DATA);

//	do{
//		data_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	} while (!data_sent);
//	
//	// Generate Stop Signal
//	I2C_GenerateSTOP(I2C2,ENABLE);

//	//-------------------------
//	// Reset Flags
//	//-------------------------

//	reset_flags();

//	//======================================
//	// AD5252:
//	// -------------------------------------
//	//======================================
//	
//	//--------------------------------------
//	// RIG MIC GAIN CONFIG
//	//--------------------------------------	
//	
//	//-----------------------
//	// Set WP Line high
//	//-----------------------
//	rig_digi_write_protect(1);
//	
//	//------------------------
//	// Generate Start Signal
//	//------------------------
//	I2C_GenerateSTART(I2C2,ENABLE);

//	I2C_Send7bitAddress(I2C2,RIG_DIGIPOT_SLAVE_ADDRESS,I2C_Direction_Transmitter);
//	
//		address_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000082));
//	do{
//		address_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000082));
//	} while (!address_sent);

//	// Select RDAC3 Register
//	I2C_SendData(I2C2,RIG_RDAC3);
//	instr_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	do{
//		instr_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	} while (!instr_sent);

//	// Writing actual Setting	
//	I2C_SendData(I2C2,RIG_MIC_SETTING);
//	
//		data_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	do{
//		data_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	} while (!data_sent);

//	// Generate Stop Signal
//	I2C_GenerateSTOP(I2C2,ENABLE);
//	
//	//------------------------
//	// Set WP Line Low
//	//-----------------------
//	rig_digi_write_protect(0);
//	
//	//---------------------------------------

//	reset_flags();

//	//-------------------------------------
//	// RIG SPKR GAIN CONFIG
//	//-------------------------------------

//	//------------------------
//	// Set WP Line HIGH
//	//-----------------------

//	rig_digi_write_protect(1);

//	// Generate Start Signal
//	I2C_GenerateSTART(I2C2,ENABLE);
//	
//	I2C_Send7bitAddress(I2C2,RIG_DIGIPOT_SLAVE_ADDRESS,I2C_Direction_Transmitter); // Digipot Address : 010 1101 
//	do{
//		address_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000082));
//	} while (!address_sent);
//	
//	// Select RDAC3 Register
//	I2C_SendData(I2C2,RIG_RDAC1);

//	do{
//		instr_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	} while (!instr_sent);
//	
//	// Writing actual Setting	
//	I2C_SendData(I2C2,RIG_SPKR_SETTING);

//	do{
//		data_sent = ((I2C2->SR2 == 0x00000007) && (I2C2->SR1 == 0x00000084));
//	} while (!data_sent);
//	
//	// Generate Stop Signal
//	I2C_GenerateSTOP(I2C2,ENABLE);
//	
//	//------------------------
//	// Set WP Line Low
//	//-----------------------	
//	rig_digi_write_protect(0);

}

//volatile uint8_t check_sending(){
//	
//		switch(digipot_config_phase){
//		
//		case ADDRESS_SENT:              
//		if(!address_sent && I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) ){
//			address_sent = 1;
//			digipot_config_phase = INSTR_SENT;
//			return 1;
//		}
//		break;
//		
//		case INSTR_SENT:
//		
//		if(!instr_sent && I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED )){
//			instr_sent = 1;
//			digipot_config_phase = DATA_SENT;
//			return 1;
//		}
//		
//		break;
//		
//		case DATA_SENT:
//		if(!data_sent && I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED )){
//			data_sent = 1;
//			digipot_config_phase = ADDRESS_SENT;
//			return 1;
//		}
//		break;
//	}

//	return 0;
//}


void i2c_configure(){

	//----------------------------
	// Clock Enables
	//----------------------------

	// Initialising I2C clock first so that perhaps we solve this Bus Busy error we're always getting
	// https://electronics.stackexchange.com/questions/272427/stm32-busy-flag-is-set-after-i2c-initialization

  /* Reset I2Cx IP */
	  RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, ENABLE);

  /* Release reset signal of I2Cx IP */
	  RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, DISABLE);

	//-----------------------------------------------------


	// Enable GPIO C Port Clock - For Write Protect of Rig Digipot
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	
	//--------------------------------------------
	// AD5252-WP GPIO Config - PC3
	//--------------------------------------------
	GPIO_InitTypeDef GPIO_InitStruct0;
	
	GPIO_InitStruct0.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStruct0.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct0.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStruct0.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct0.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStruct0);	

	//---------------------------------------------

	// Enable GPIO H Port Clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);

	//--------------------------------
	// SCL/SDA GPIO Config - PH4,PH5
	//--------------------------------
	GPIO_InitTypeDef GPIO_InitStruct_SDA;
	GPIO_InitTypeDef GPIO_InitStruct_SCL;
	
//	//------------------------------
//	// SCL Pin - PH4
//	//------------------------------
//	
//	GPIO_InitStruct_SCL.GPIO_Pin = GPIO_Pin_4;
//	GPIO_InitStruct_SCL.GPIO_Mode = GPIO_Mode_AF;
//	GPIO_InitStruct_SCL.GPIO_Speed = GPIO_Speed_50MHz;
//	//------------------------------------------------------------
//	// Recommended that the pins use pull-up resistances
//	// Open Drain recommended because multiple pins are connected
//	//------------------------------------------------------------	
//	GPIO_InitStruct_SCL.GPIO_OType = GPIO_OType_OD;
//	GPIO_InitStruct_SCL.GPIO_PuPd = GPIO_PuPd_UP;
//	GPIO_Init(GPIOH, &GPIO_InitStruct_SCL);
//	
//	
//	//------------------------------
//	// SDA Pin - PH5
//	//------------------------------
//	
//	GPIO_InitStruct_SDA.GPIO_Pin = GPIO_Pin_5;
//	GPIO_InitStruct_SDA.GPIO_Mode = GPIO_Mode_AF;
//	GPIO_InitStruct_SDA.GPIO_Speed = GPIO_Speed_50MHz;
//	//------------------------------------------------------------
//	// Recommended that the pins use pull-up resistances
//	// Open Drain recommended because multiple pins are connected
//	//------------------------------------------------------------	
//	GPIO_InitStruct_SDA.GPIO_OType = GPIO_OType_OD;
//	GPIO_InitStruct_SDA.GPIO_PuPd = GPIO_PuPd_UP;
//	GPIO_Init(GPIOH, &GPIO_InitStruct_SDA);


	//------------------------------
	// SDA Pin - PH5
	//------------------------------
	
	GPIO_InitStruct_SDA.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStruct_SDA.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct_SDA.GPIO_Speed = GPIO_Speed_50MHz;
	//------------------------------------------------------------
	// Recommended that the pins use pull-up resistances
	// Open Drain recommended because multiple pins are connected
	//------------------------------------------------------------	
	GPIO_InitStruct_SDA.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct_SDA.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOH, &GPIO_InitStruct_SDA);

	//-----------------------------------
	
	//--------------
	// I2C Config
	//--------------
	
	// Enable I2C clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2,ENABLE);
	
//	I2C_Cmd(I2C2, DISABLE);
//	I2C_DeInit(I2C2);
	I2C_InitTypeDef I2C_InitStructure;
	
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
//I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_16_9;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0x33;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = 1000; // Standard mode
	
	//-----------------------------------
	//-----------------------------------
	
	I2C_Init(I2C2, &I2C_InitStructure);
	/* Enable the I2C peripheral */
	I2C_Cmd(I2C2, ENABLE);
	
	jak_counter();
	//-----------------------------------
	// Setting Up interrupt requests for 
	// I2C.
	//----------------------------------

	//------------------------------------------------
	// This is to set up the interrupts from I2C2.
	// 
	// AKA Set up the ITBUFEN, ITEVTEN, ITERREN bits!
	//------------------------------------------------
	//------------------------------------------------
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Configure the I2C event priority */
	NVIC_InitStructure.NVIC_IRQChannel                   = I2C2_EV_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	//-----------------------------------
	// Set Up Interrupt Triggers
	//-----------------------------------
	#ifdef I2C_SPL	
		I2C_ITConfig(I2C2, I2C_IT_BUF , ENABLE);
		I2C_ITConfig(I2C2, I2C_IT_EVT , ENABLE);
		I2C_ITConfig(I2C2, I2C_IT_ERR , ENABLE);
	#endif // I2C_SPL
	//------------------------------------
}



void rig_digi_write_protect(uint8_t state){
	
	   if (state > 0)
        GPIOC->ODR |= (1 << 3);
    else if (state < 0)
        GPIOC->ODR ^= (1 << 3);
    else
        GPIOC->ODR &= ~(1 << 3);
	
}









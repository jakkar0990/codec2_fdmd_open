///*---------------------------------------------------------------------------*\

//  FILE........: stm32f4_rcc.c
//  AUTHOR......: 
//  DATE CREATED: 

//  This contains functions to analyze and check that clocks are running as expected.
// This is due to the fact that clock registers when read did not produce resulst as
// expected.

//\*---------------------------------------------------------------------------*/

///*
//  Copyright (C) 2013 David Rowe

//  All rights reserved.

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License version 2.1, as
//  published by the Free Software Foundation.  This program is
//  distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
//  License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with this program; if not, see <http://www.gnu.org/licenses/>.
//*/

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>



//#include "blu5_defines.h"




void initClocks(void) {
	/*
	HSE: 8 MHz
	PLL: HSE / 8 * 192 => 192 MHz
	SysCLK:  PLL / 2 => 96 MHz
	PrphCLK: PLL / 4 => 48 MHz
	*/
	RCC_HSEConfig(RCC_HSE_ON);
	
	RCC_PLLConfig(RCC_PLLSource_HSE, 8, 336, 2, 7);
	RCC_PLLCmd(ENABLE);

	// Wait
	while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) continue;

	// Use PLL
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

	while (RCC_GetSYSCLKSource() != 0x08) continue;

	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLK2Config(RCC_SYSCLK_Div1);
	RCC_PCLK1Config(RCC_SYSCLK_Div1);
}
















#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "blu5_defines.h"

#include "stm32f4xx.h"
#include "codec2_fifo.h"
#include "stm32f4_dac.h"
#include "debugblinky.h"

#include "stm32f4xx_spi.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_adc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#define DAC_BUF_SZ   320

#define SPI_DATA_ADDRESS 			 0x4001300C


static unsigned short dac1_buf[DAC_BUF_SZ];

void spi_configure(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		SPI_InitTypeDef SPI_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
		DMA_InitTypeDef  DMA_InitStructure;
		
		/* Enable the SPI clock */
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI5, ENABLE);
		/* Enable the GPIOA clock */
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
		/* Connect PF6 to SPI5_NSS */
		GPIO_PinAFConfig(GPIOF, GPIO_PinSource6, GPIO_AF_SPI5);
		/* Connect PF7 to SPI5_SCK */
		GPIO_PinAFConfig(GPIOF, GPIO_PinSource7, GPIO_AF_SPI5);
		/* Connect PF9 to SPI5_MOSI */
		GPIO_PinAFConfig(GPIOF, GPIO_PinSource9, GPIO_AF_SPI5);
	
		/*  Configure SPI1 pins as alternate function  */ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 |GPIO_Pin_9;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(GPIOF, &GPIO_InitStructure);
		
		/* SPI configuration *****************************************/

		SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
		SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
		SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
		SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
		SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
		SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
		SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
		SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
		SPI_InitStructure.SPI_CRCPolynomial = 7;
		SPI_Init(SPI5, &SPI_InitStructure);

		//-------------------------
		// DMA Configuration
		//------------------------------------------------------------
		
		DMA_DeInit(DMA1_Stream5);
		DMA_InitStructure.DMA_Channel = DMA_Channel_7;
		DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) SPI_DATA_ADDRESS; // SPI DR address
		DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)dac1_buf;   // This is good
		DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
		DMA_InitStructure.DMA_BufferSize = DAC_BUF_SZ;
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
		DMA_InitStructure.DMA_Priority = DMA_Priority_High;
		DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
		DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
		DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
		DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
		DMA_Init(DMA1_Stream5, &DMA_InitStructure);
		
	//-------------------------------------------------------------
		
	/* Enable DMA Half & Complete interrupts */

  DMA_ITConfig(DMA1_Stream5, DMA_IT_TC | DMA_IT_HT, ENABLE);

  /* Enable the DMA Stream IRQ Channel */

  NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable DMA1_Stream5 */

  DMA_Cmd(DMA1_Stream5, ENABLE);

  /* Enable DAC Channel 1 */

  SPI_Cmd(SPI5, ENABLE);

  /* Enable DMA for SPI5 */

	SPI_I2S_DMACmd(SPI5, SPI_I2S_DMAReq_Tx, ENABLE);

}


/*---------------------------------------------------------------------------*\

  FILE........: stm32f4_uart.c
  AUTHOR......: 
  DATE CREATED: 

  UART Driver for enabling data tx/rx between embedded and PC.

\*---------------------------------------------------------------------------*/

/*
  Copyright (C) 2013 David Rowe

  All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2.1, as
  published by the Free Software Foundation.  This program is
  distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f4xx.h"
#include "stm32f4_uart.h"
#include <ctype.h>

#include "blu5_defines.h"
#include "blu5_struct_defines.h"

#include "freedv_api.h"


uint8_t uart_menu_flag = 0;
volatile char uart_menu_sel = 0;
volatile int uart_sel_int = 0;
volatile uint8_t menu_valid = 0;
volatile int menu_selection = 0;			


void baud_rate_corr(){
	USART3->BRR = 0x1117;
}


void uart_setup(){
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStruct;

	#ifdef BLU5_BOARD
	/* Enable GPIOC clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	/* Connect PC10 to USART3_Tx */
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_USART3);
	/* Connect PC11 to USART3_Rx*/
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_USART3);
	/* Configure USART3_Tx and USART3_Rx as alternate function */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;						// Just to know : The boards have external pullups
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;							// Try changing the setting
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	#endif //BLU5_BOARD	
	

	#ifdef SM1000
	/* Enable GPIOC clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	/* Connect PC10 to USART3_Tx */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
	/* Connect PC11 to USART3_Rx*/
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);
	/* Configure USART3_Tx and USART3_Rx as alternate function */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	#endif //SM1000
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	USART_InitStructure.USART_BaudRate = 9600;

	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure);
	
	/* Enable USART3 */
	USART_Cmd(USART3, ENABLE);
		
	/**
	 * Set Channel to USART1
	 * Set Channel Cmd to enable. That will enable USART1 channel in NVIC
	 * Set Both priorities to 0. This means high priority
	 *
	 * Initialize NVIC
	 */
	NVIC_InitStruct.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
		
/**
 * Enable RX interrupt
 */
 
	USART_ITConfig(USART3, USART_IT_RXNE , ENABLE);
	//USART_ITConfig(USART3,  USART_IT_IDLE , ENABLE);
	USART_ITConfig(USART3, USART_IT_ERR , ENABLE);
	USART_ITConfig(USART3, USART_IT_PE , ENABLE);

}

//---------------------------------
// Test UART Driver Function
// Pinting all possible functions
//---------------------------------
void test_uart_putnum(USART_TypeDef* USARTx){
	
    short i = 0;

	while(i <= 255){

		uart_putnum(USARTx, i);

		i++;
	}
}



void uart_putnum(USART_TypeDef* USARTx,short num){
	
	//Storage Matrix
	// Assuming 10 digits is largest number
	char num_output_matrix_in_rev[10];
	
	// Loop Counter
	uint8_t arr_counter = 0;

	short quotient = 0xFF;
	uint8_t rem = 0xFF;


	// Temporary Variable used to store result of each seq.
	// division
	short res_store = num;

	while (quotient != 0){
	
		quotient = res_store / 10;
		rem = res_store % 10;

		res_store = quotient;

		num_output_matrix_in_rev[arr_counter++] = rem + 48; // +48 because 48 is the number of character '0' in ASCII
	}
	
	// arr_counter is 1 greater than no. of chars

	for (int i = arr_counter - 1; i >= 0; i--){
		while ((FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE));
		UU_PutChar(USARTx, num_output_matrix_in_rev[i]);
	}

	while ((FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE));
	UU_PutChar(USARTx, 44); //Insert a comma

	while ((FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE));
	UU_PutChar(USARTx, 32); //Insert a space

	while ((FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE));
	UU_PutChar(USARTx, 10); //Insert Newline

}


void UU_PutChar(USART_TypeDef* USARTx, uint8_t ch)
{
  while(!(USARTx->SR & USART_SR_TXE));
  USARTx->DR = ch;  
}


void UU_PutString(USART_TypeDef* USARTx, uint8_t * str)
{
  //while ((FlagStatus)USART_GetFlagStatus(USARTx, USART_FLAG_IDLE));
  while(*str != 0)
  {
    UU_PutChar(USARTx, *str);
    str++;
  }
}


int output_data_received_to_uart(struct data_struct * data_buffers, struct arq_machine * arq_mod){

	int is_idle = (FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE);

	if (is_idle){

		for (int i = 0; i < arq_mod->arq_total_session_frames; i++){

			UU_PutString(USART3, data_buffers->data_text_rx[i]);
		}
	}
	return 1;
}



void debug_to_uart(struct data_struct * data_buffers, struct arq_machine * arq_mod){

	if (arq_mod->debug_counter < DEBUG_NUM){
		if ( (strncmp((const char *)data_buffers->data_tx_rx_frame, (const char *) "11011", 5) != 0) ){

			memcpy(data_buffers->debug[arq_mod->debug_counter], data_buffers->data_tx_rx_frame, 5);

			data_buffers->debug[arq_mod->debug_counter][5] = data_buffers->data_tx_rx_frame[5];

			memcpy(data_buffers->data_tx_rx_frame, "11011", 5);
			data_buffers->data_tx_rx_frame[5] = '\0';
			arq_mod->debug_counter++;
		}
	}


		//if (arq_mod->arq_state == ARQ_RX_ACK){

		//	// Contents of frames!
		//	for (int i = 0; i < 5; i++){
		//		while ((FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE));
		//		UU_PutChar(USART3, data_buffers->data_tx_rx_frame[i]);
		//	}

		//	uart_putnum(USART3, data_buffers->data_tx_rx_frame[5]);
		//	while ((FlagStatus)USART_GetFlagStatus(USART3, USART_FLAG_IDLE));
		//	UU_PutString(USART3, ",\n");
		//}
}


void USART3_IRQHandler(void)
{
	if( USART_GetITStatus(USART3, USART_IT_RXNE))
  {
		if(!uart_menu_flag)
			buffers.data_from_uart[data_tx_rx_1.tx_uart_data_index++] = USART_ReceiveData(USART3);
		else
		uart_menu_sel = USART_ReceiveData(USART3);	
  }
	
	if(USART_GetITStatus(USART3, USART_IT_PE))
		data_tx_rx_1.usart_perr_counter++;
	
	if(USART_GetITStatus(USART3, USART_IT_ERR))
		data_tx_rx_1.usart_err_counter++;
	
	data_tx_rx_1.uart_start_stop++;
}

uint8_t size_of_buffer(unsigned char *arr){
	int counter = 0;
	while((arr[counter] != '\0') && (counter <= (MAX_UART_BUFF_SIZE - 1))){
	counter++;
	}
	return counter;
}

uint8_t uart_menu(){
	
	uint8_t mode_selected = 0;
	UU_PutString(USART3,"Which data mode? \n\n");

	UU_PutString(USART3,"1) ANALOG \n");
	UU_PutString(USART3,"2) DV \n");
	UU_PutString(USART3,"3) TONE \n");
//	UU_PutString(USART3,"4) DATA_PTT \n");
	UU_PutString(USART3,"5) DATA_UART \n");
	UU_PutString(USART3,"6) DV_TEST \n");
	UU_PutString(USART3,"7) DATA_TEST \n");
	UU_PutString(USART3,"8) DATA_ARQ \n");
	
	
	// We are in menu now!!
	uart_menu_flag = 1;
	
	clear_buffer(buffers.data_from_uart);

	while( !uart_menu_sel ){   // Loop here till numeric is inserted from terminal
		jak_counter();
		jak_counter();
	}
	
	if(uart_menu_sel == '8'){  // DATA_ARQ
		mode_selected = DATA_ARQ;
		UU_PutString(USART3,"\nDATA_ARQ mode selected. \n");
		UU_PutString(USART3, "Please insert text to transmit: \n");
	}
	else if(uart_menu_sel == '7'){  // DATA_TEST
		mode_selected = DATA_TEST;
		UU_PutString(USART3,"\nDATA_TEST mode selected. \n");
	}
	else if(uart_menu_sel == '6'){ // DV_TEST
		UU_PutString(USART3,"\nDV_TEST mode selected. \n");
		mode_selected = DV_TEST;
	}
	else if(uart_menu_sel == '5'){ // DATA_UART
		mode_selected = DATA_UART;
		UU_PutString(USART3,"\nDATA_UART mode selected. \n");
		UU_PutString(USART3,"Please insert text to transmit: \n");
	}
//	else if(uart_menu_sel == '4'){ // DATA_PTT
//		op_mode = DATA_PTT;
//		UU_PutString(USART3,"\nDATA_PTT mode selected. \n");		
//	}
	else if(uart_menu_sel == '3'){  //  TONE
		mode_selected = TONE;
		UU_PutString(USART3,"\nTONE mode selected. \n");
	}
	else if(uart_menu_sel == '2'){  // DV
		mode_selected = DV;
		UU_PutString(USART3,"\nDV mode selected. \n");
	}
	else if(uart_menu_sel == '1'){  // ANALOG
		mode_selected = ANALOG;
		UU_PutString(USART3,"\nANALOG mode selected. \n");
	}
	
 	uart_menu_flag = 0;
	clear_buffer(buffers.data_from_uart);
	data_tx_rx_1.tx_uart_data_index = 0;
	uart_menu_sel = 0;

	return mode_selected;
}







